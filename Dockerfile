FROM openjdk:8-jdk-slim

WORKDIR /app

ADD target/caepi-0.0.1-SNAPSHOT.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "app.jar"]