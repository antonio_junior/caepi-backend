package br.com.caepi.service;

import br.com.caepi.entities.EspecificidadeOpcao;
import br.com.caepi.entities.NormaTecnica;
import br.com.caepi.repository.EspecificidadeOpcaoRegraExibicaoRepository;
import br.com.caepi.repository.EspecificidadeOpcaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class EspecificidadeOpcaoService {

    @Autowired
    EspecificidadeOpcaoRepository especificidadeOpcaoRepository;

    @Autowired
    EspecificidadeOpcaoRegraExibicaoRepository especificidadeOpcaoRegraExibicaoRepository;

    public EspecificidadeOpcao salvar(EspecificidadeOpcao especificidadeOpcao) {
        Integer ultimaOrdem = this.pegarUltimaOrdem(especificidadeOpcao.getEspecificidade().getId());
        especificidadeOpcao.setOrdem(ultimaOrdem == null ? 1 : ultimaOrdem + 1);
        return this.especificidadeOpcaoRepository.save(especificidadeOpcao);
    }

    public EspecificidadeOpcao atualizarExibicao(Long id, EspecificidadeOpcao especificidadeOpcaoAtualizada) {
        EspecificidadeOpcao especificidadeOpcaoExistente = especificidadeOpcaoRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("EspecificidadeOpcao não encontrada com o ID: " + id));

        especificidadeOpcaoAtualizada.setEspecificidade(especificidadeOpcaoExistente.getEspecificidade());

        // Verifica se o usuário escolheu a opção "Não depende"
        if (especificidadeOpcaoAtualizada.getRegraExibicao() == null && especificidadeOpcaoExistente.getRegraExibicao() != null) {
            Long especificidadeOpcaoRegraExibicaoId = especificidadeOpcaoExistente.getRegraExibicao().getId();
            especificidadeOpcaoRegraExibicaoRepository.deleteRegrasEspecificidadesOpcoesByRegraId(especificidadeOpcaoRegraExibicaoId);
            especificidadeOpcaoRegraExibicaoRepository.deleteEspecificidadeOpcaoRegraExibicaoById(especificidadeOpcaoRegraExibicaoId);
        }

        return this.especificidadeOpcaoRepository.save(especificidadeOpcaoAtualizada);
    }

    public EspecificidadeOpcao atualizar(Long id, EspecificidadeOpcao especificidadeOpcaoAtualizada) {
        EspecificidadeOpcao especificidadeOpcaoExistente = especificidadeOpcaoRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("EspecificidadeOpcao não encontrada com o ID: " + id));

        especificidadeOpcaoExistente.setDescricao(especificidadeOpcaoAtualizada.getDescricao());
        especificidadeOpcaoExistente.setIndicacaoDeUso(especificidadeOpcaoAtualizada.getIndicacaoDeUso());
        especificidadeOpcaoExistente.setOrdem(especificidadeOpcaoAtualizada.getOrdem());

        return especificidadeOpcaoRepository.save(especificidadeOpcaoExistente);
    }

    public Optional<EspecificidadeOpcao> buscarPorId(Long id) {
        return especificidadeOpcaoRepository.findById(id);
    }

    public void excluir(Long id) {
        this.especificidadeOpcaoRepository.deleteById(id);
    }

    public EspecificidadeOpcao vincularNormaTecnica(Long especificidadeOpcaoId, Set<NormaTecnica> normasTecnicas) {
        EspecificidadeOpcao especificidadeOpcao = this.especificidadeOpcaoRepository.findById(especificidadeOpcaoId)
                .orElseThrow(() -> new RuntimeException("EspecificidadeOpcao não encontrada com o ID: " + especificidadeOpcaoId));

        especificidadeOpcao.setNormasTecnicas(normasTecnicas);

        return this.especificidadeOpcaoRepository.save(especificidadeOpcao);
    }

    private Integer pegarUltimaOrdem(Long idEspecificidade) {
        return this.especificidadeOpcaoRepository.pegarUltimaOrdem(idEspecificidade);
    }
}
