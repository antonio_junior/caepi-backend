package br.com.caepi.service;

import br.com.caepi.entities.ProtecaoBase;
import br.com.caepi.entities.TipoEpi;
import br.com.caepi.entities.TipoEpiProtecaoBase;
import br.com.caepi.repository.TipoEpiRepository;
import br.com.caepi.repository.TipoEpiProtecaoBaseRepository;
import br.com.caepi.repository.SubTipoProtecaoRepository;
import br.com.caepi.repository.TipoProtecaoRepository;
import br.com.caepi.specifications.TipoEpiSubTipoProtecaoSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class TipoEpiProtecaoBaseService {

    private final TipoEpiProtecaoBaseRepository tipoEpiProtecaoBaseRepository;
    private final TipoEpiRepository tipoEpiRepository;
    private final TipoProtecaoRepository tipoProtecaoRepository;
    private final SubTipoProtecaoRepository subTipoProtecaoRepository;

    @Autowired
    public TipoEpiProtecaoBaseService(TipoEpiProtecaoBaseRepository tipoEpiProtecaoBaseRepository,
                                      TipoEpiRepository tipoEpiRepository,
                                      TipoProtecaoRepository tipoProtecaoRepository,
                                      SubTipoProtecaoRepository subTipoProtecaoRepository) {
        this.tipoEpiProtecaoBaseRepository = tipoEpiProtecaoBaseRepository;
        this.tipoEpiRepository = tipoEpiRepository;
        this.subTipoProtecaoRepository = subTipoProtecaoRepository;
        this.tipoProtecaoRepository = tipoProtecaoRepository;
    }

    public Page<TipoEpiProtecaoBase> findAll(Map<String, String> queryParameters
            , Pageable pageable) {
        Specification<TipoEpiProtecaoBase> spec = new TipoEpiSubTipoProtecaoSpecification(queryParameters);
        return tipoEpiProtecaoBaseRepository.findAll(spec, pageable);
    }

    public Optional<TipoEpiProtecaoBase> buscarPorId(Long id) {
        return tipoEpiProtecaoBaseRepository.findById(id);
    }

    public TipoEpiProtecaoBase criarTipoEpiProtecaoBase(Long tipoEpiId, Long entityId, String tipo) {
        TipoEpi tipoEpi = tipoEpiRepository.findById(tipoEpiId)
                .orElseThrow(() -> new RuntimeException("TipoEpi não encontrado com o ID: " + tipoEpiId));

        ProtecaoBase protecaoBase = null;

        if(tipo.equals("SubTipoProtecao")) {
            protecaoBase = subTipoProtecaoRepository.findById(entityId)
                    .orElseThrow(() -> new RuntimeException("SubTipoProtecao não encontrado com o ID: " + entityId));
        }

        if(tipo.equals("TipoProtecao")) {
            protecaoBase = tipoProtecaoRepository.findById(entityId)
                    .orElseThrow(() -> new RuntimeException("TipoProtecao não encontrado com o ID: " + entityId));
        }


        TipoEpiProtecaoBase tipoEpiProtecaoBase = new TipoEpiProtecaoBase();
        tipoEpiProtecaoBase.setTipoEpi(tipoEpi);
        tipoEpiProtecaoBase.setProtecaoBase(protecaoBase);

        return tipoEpiProtecaoBaseRepository.save(tipoEpiProtecaoBase);
    }

    public void excluirTipoEpiProtecaoBase(Long id) {
        tipoEpiProtecaoBaseRepository.deleteByIdNative(id);
    }
}

