package br.com.caepi.service;

import br.com.caepi.DTOS.LaboratorioOCPDTO;
import br.com.caepi.entities.LaboratorioOCP;
import br.com.caepi.entities.TipoEpi;
import br.com.caepi.repository.LaboratorioOCPRepository;
import br.com.caepi.repository.TipoEpiRepository;
import br.com.caepi.specifications.LaboratorioOCPSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class LaboratorioOCPService {

    private final LaboratorioOCPRepository laboratorioOCPRepository;
    private final TipoEpiRepository tipoEpiRepository;

    @Autowired
    public LaboratorioOCPService(LaboratorioOCPRepository laboratorioOCPRepository, TipoEpiRepository tipoEpiRepository) {
        this.laboratorioOCPRepository = laboratorioOCPRepository;
        this.tipoEpiRepository = tipoEpiRepository;
    }

    public Set<LaboratorioOCP> listarTodosLaboratoriosOCPs() {
        List<LaboratorioOCP> laboratorioOCPList = laboratorioOCPRepository.findAll();
        Comparator<LaboratorioOCP> idComparator = Comparator.comparing(LaboratorioOCP::getId);
        return laboratorioOCPList.stream()
                .sorted(idComparator)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }
    public Page<LaboratorioOCP> findAll(Map<String, String> queryParameters
            , Pageable pageable) {
        Specification<LaboratorioOCP> spec = new LaboratorioOCPSpecification(queryParameters);
        return laboratorioOCPRepository.findAll(spec, pageable);
    }

    public Optional<LaboratorioOCP> buscarPorId(Long id) {
        return laboratorioOCPRepository.findById(id);
    }

    public LaboratorioOCP salvar(LaboratorioOCP laboratorioOCP) {
        return laboratorioOCPRepository.save(laboratorioOCP);
    }

    public LaboratorioOCP atualizar(LaboratorioOCP laboratorioOCPAtualizado) {
        if (laboratorioOCPAtualizado.getId() == null) {
            throw new EntityNotFoundException("O ID do LaboratorioOCP é necessário para atualização.");
        }

        Optional<LaboratorioOCP> laboratorioOCPOptional = laboratorioOCPRepository.findById(laboratorioOCPAtualizado.getId());

        if (laboratorioOCPOptional.isPresent()) {
            LaboratorioOCP laboratorioOCPExistente = laboratorioOCPOptional.get();

            laboratorioOCPExistente.setRazaoSocial(laboratorioOCPAtualizado.getRazaoSocial());
            laboratorioOCPExistente.setNomeFantasia(laboratorioOCPAtualizado.getNomeFantasia());
            laboratorioOCPExistente.setCnpj(laboratorioOCPAtualizado.getCnpj());
            laboratorioOCPExistente.setDataCadastro(laboratorioOCPAtualizado.getDataCadastro());
            laboratorioOCPExistente.setEndereco(laboratorioOCPAtualizado.getEndereco());
            laboratorioOCPExistente.setCep(laboratorioOCPAtualizado.getCep());
            laboratorioOCPExistente.setEmail(laboratorioOCPAtualizado.getEmail());
            laboratorioOCPExistente.setTelefone(laboratorioOCPAtualizado.getTelefone());
            laboratorioOCPExistente.setTipoEpiProcessos(laboratorioOCPAtualizado.getTipoEpiProcessos());
            laboratorioOCPExistente.setDataRevogacao(laboratorioOCPAtualizado.getDataRevogacao());
            laboratorioOCPExistente.setEstado(laboratorioOCPAtualizado.getEstado());
            laboratorioOCPExistente.setMunicipio(laboratorioOCPAtualizado.getMunicipio());
            laboratorioOCPExistente.setTipo(laboratorioOCPAtualizado.getTipo());

            return laboratorioOCPRepository.save(laboratorioOCPExistente);
        } else {
            throw new EntityNotFoundException("LaboratorioOCP não encontrado com o ID: " + laboratorioOCPAtualizado.getId());
        }
    }

    @Transactional
    public LaboratorioOCP atualizarLaboratorioOCPTipoEPI(LaboratorioOCPDTO laboratorioOCPAtualizado) {
        LaboratorioOCP existingLaboratorioOCP = laboratorioOCPRepository.findById(laboratorioOCPAtualizado.getId())
                .orElseThrow(() -> new EntityNotFoundException("LaboratorioOCP não encontrado com ID: " + laboratorioOCPAtualizado.getId()));

        existingLaboratorioOCP.getTiposEpi().clear();

        laboratorioOCPAtualizado.getTiposEpi().forEach(tipoEpi -> {
            TipoEpi existingTipoEpi = tipoEpiRepository.findById(tipoEpi.getId())
                    .orElseThrow(() -> new EntityNotFoundException("TipoEpi não encontrado com ID: " + tipoEpi.getId()));
            existingLaboratorioOCP.getTiposEpi().add(existingTipoEpi);
        });

        return laboratorioOCPRepository.save(existingLaboratorioOCP);
    }

    @Transactional
    public void excluirLaboratorioOCP(Long laboratorioOCPId) {
        LaboratorioOCP laboratorioOCP = laboratorioOCPRepository.findById(laboratorioOCPId)
                .orElseThrow(() -> new EntityNotFoundException("LaboratorioOCP não encontrado com ID: " + laboratorioOCPId));

        if (laboratorioOCP != null) {
            // Desvincular os tipos de EPIs relacionados ao laboratório
            laboratorioOCP.getTiposEpi().clear();

            // Remover referência do laboratório nos tipos de EPIs associados
            for (TipoEpi tipoEpi : laboratorioOCP.getTiposEpi()) {
                tipoEpi.getLaboratorios().remove(laboratorioOCP);
            }

            // Remover o laboratório
            laboratorioOCPRepository.deleteById(laboratorioOCPId);
        }
    }
}
