package br.com.caepi.service;

import br.com.caepi.DTOS.TipoEpiConjugadoDTO;
import br.com.caepi.DTOS.TipoEpiDTO;
import br.com.caepi.entities.TipoEpi;
import br.com.caepi.repository.TipoEpiRepository;
import br.com.caepi.repository.TipoEpiProtecaoBaseRepository;
import br.com.caepi.specifications.TipoEpiSpecification;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TipoEpiService {
    private final TipoEpiRepository tipoEpiRepository;
    private final TipoEpiProtecaoBaseRepository tipoEpiProtecaoBaseRepository;

    @Autowired
    public TipoEpiService(TipoEpiRepository tipoEpiRepository, TipoEpiProtecaoBaseRepository tipoEpiProtecaoBaseRepository) {
        this.tipoEpiRepository = tipoEpiRepository;
        this.tipoEpiProtecaoBaseRepository = tipoEpiProtecaoBaseRepository;
    }

    public Set<TipoEpi> listarTodosTipoEpi() {
        List<TipoEpi> tipoepis = tipoEpiRepository.findAll();
        return new HashSet<>(tipoepis);
    }

    public Optional<TipoEpi> buscarPorId(Long id) {
        return tipoEpiRepository.findById(id);
    }

    public TipoEpi salvar(TipoEpi tipoEpi) {
        return tipoEpiRepository.save(tipoEpi);
    }


    @Transactional
    public TipoEpi atualizar(TipoEpi tipoEpi) {
        TipoEpi existingTipoEpi = tipoEpiRepository.findById(tipoEpi.getId())
                .orElseThrow(() -> new EntityNotFoundException("TipoEpi não encontrado com ID: " + tipoEpi.getId()));

        existingTipoEpi.setTitulo(tipoEpi.getTitulo());
        existingTipoEpi.setTextoProtecao(tipoEpi.getTextoProtecao());
        existingTipoEpi.setDescricao(tipoEpi.getDescricao());
        existingTipoEpi.setDataRevogacao(tipoEpi.getDataRevogacao());
        existingTipoEpi.setCertificadoDeConformidade(tipoEpi.isCertificadoDeConformidade());
        existingTipoEpi.setEnsaioNoExterior(tipoEpi.isEnsaioNoExterior());
        existingTipoEpi.setIndicacaoDeUso(tipoEpi.getIndicacaoDeUso());
        existingTipoEpi.setProcessoExclusivoFabricanteImportador(tipoEpi.getProcessoExclusivoFabricanteImportador());
        existingTipoEpi.setPermitirVerificacaoPorLink(tipoEpi.getPermitirVerificacaoPorLink());

        if (existingTipoEpi.isConjugado()) {
            tipoEpi.setConjugaveis(existingTipoEpi.getConjugaveis());
        }

        Set<TipoEpi> managedConjugaveis = tipoEpi.getConjugaveis().stream()
                .map(conjugavel -> tipoEpiRepository.findById(conjugavel.getId())
                        .orElseThrow(() -> new EntityNotFoundException("TipoEpi conjugavel não encontrado com ID: " + conjugavel.getId())))
                .collect(Collectors.toSet());

        // Antes de ajustar os conjugáveis do existingTipoEpi, vamos garantir que cada conjugável também
        // contém o existingTipoEpi como um de seus conjugáveis.
        for (TipoEpi managedConjugavel : managedConjugaveis) {
            managedConjugavel.getConjugaveis().add(existingTipoEpi);
            tipoEpiRepository.save(managedConjugavel);
        }

        existingTipoEpi.setConjugaveis(managedConjugaveis);
        return tipoEpiRepository.save(existingTipoEpi);
    }

    @Transactional
    public TipoEpi atualizarEpiConjugado(TipoEpiDTO tipoEpi) {
        TipoEpi existingTipoEpi = tipoEpiRepository.findById(tipoEpi.getId())
                .orElseThrow(() -> new EntityNotFoundException("TipoEpi não encontrado com ID: " + tipoEpi.getId()));

        Set<TipoEpi> managedConjugaveis = tipoEpi.getConjugaveis().stream()
                .map(conjugavel -> tipoEpiRepository.findById(conjugavel.getId())
                        .orElseThrow(() -> new EntityNotFoundException("TipoEpi conjugavel não encontrado com ID: " + conjugavel.getId())))
                .collect(Collectors.toSet());

        // Antes de ajustar os conjugáveis do existingTipoEpi, vamos garantir que cada conjugável também
        // contém o existingTipoEpi como um de seus conjugáveis.
        for (TipoEpi managedConjugavel : managedConjugaveis) {
            managedConjugavel.getConjugaveis().add(existingTipoEpi);
            tipoEpiRepository.save(managedConjugavel);
        }

        existingTipoEpi.setConjugaveis(managedConjugaveis);
        return tipoEpiRepository.save(existingTipoEpi);
    }

    public void excluirPorId(Long id) {
        tipoEpiRepository.deleteById(id);
    }

    public void excluirTipoEpi(Long id) {
        TipoEpi tipoEpi = tipoEpiRepository.findById(id).orElse(null);
        if (tipoEpi != null) {
            tipoEpiRepository.delete(tipoEpi);
        } else {
            throw new RuntimeException("TipoEpi não encontrado.");
        }
    }

    public Page<TipoEpi> findAll(Map<String, String> queryParameters
            , Pageable pageable) {
        Specification<TipoEpi> spec = new TipoEpiSpecification(queryParameters);
        return tipoEpiRepository.findAll(spec, pageable);
    }

    public ByteArrayResource reportExcel(Map<String, String> queryParameters, HttpServletRequest request, HttpServletResponse response) throws IOException {

        Specification<TipoEpi> spec = new TipoEpiSpecification(queryParameters);

        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Tipos EPIs");
        sheet.setDefaultColumnWidth(30);

        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setColor(IndexedColors.BLACK.getIndex());
        style.setFont(font);

        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Código");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("Título");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("Data de Cadastro");
        header.getCell(2).setCellStyle(style);

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        int rowCount = 1;

        for(Map<String, Object> tipoEpi : this.getTiposEpis(spec)){
            Row userRow =  sheet.createRow(rowCount++);
            userRow.createCell(0).setCellValue(tipoEpi.get("id").toString());
            userRow.createCell(1).setCellValue(tipoEpi.get("titulo").toString());
            userRow.createCell(2).setCellValue(tipoEpi.get("dataCadastro").toString());
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        try {
            workbook.write(outputStream);
            workbook.close();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    System.out.println("Erro ao criar excel" + e);
                }
            }
        }
        return new ByteArrayResource(outputStream.toByteArray());
    }

    private List<Map<String, Object>> getTiposEpis(Specification<TipoEpi> spec) {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        for (TipoEpi tipoEpi : tipoEpiRepository.findAll(spec)) {
            Map<String, Object> item = new HashMap<String, Object>();
            item.put("id", tipoEpi.getId());
            item.put("titulo", tipoEpi.getTitulo() != null ? tipoEpi.getTitulo() : "");
            item.put("dataCadastro", tipoEpi.getDataCadastro() != null ? dateFormat.format(tipoEpi.getDataCadastro()) : "");
            result.add(item);
        }
        return result;
    }
}