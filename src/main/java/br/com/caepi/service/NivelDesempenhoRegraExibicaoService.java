package br.com.caepi.service;

import br.com.caepi.repository.NivelDesempenhoRegraExibicaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NivelDesempenhoRegraExibicaoService {

    @Autowired
    NivelDesempenhoRegraExibicaoRepository nivelDesempenhoRegraExibicaoRepository;

    public void excluir(Long id) {
        this.nivelDesempenhoRegraExibicaoRepository.deleteById(id);
    }
}
