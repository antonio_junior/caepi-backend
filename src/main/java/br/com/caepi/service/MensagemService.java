package br.com.caepi.service;

import br.com.caepi.entities.Mensagem;
import br.com.caepi.entities.ProtecaoProcesso;
import br.com.caepi.entities.TipoEpiProcesso;
import br.com.caepi.repository.MensagemRepository;
import br.com.caepi.repository.ProtecaoProcessoRepository;
import br.com.caepi.repository.TipoEpiProcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service(value = "MensagemService")
public class MensagemService {

    @Autowired
    private MensagemRepository mensagemRepository;

    @Autowired
    private TipoEpiProcessoRepository tipoEpiProcessoRepository;

    @Autowired
    private ProtecaoProcessoRepository protecaoProcessoRepository;

    private String ESPECIFICIDADES_GERAIS = "ESPECIFICIDADES_GERAIS";
    private String PROTECAO = "PROTECAO";

    public Page<Mensagem> findByEntidadeOrigemAndIdEntidadeOrigem(String entidadeOrigem, Long idEntidadeOrigem, Pageable pageable) {
        return mensagemRepository.findAllByEntidadeOrigemAndIdEntidadeOrigem(entidadeOrigem, idEntidadeOrigem, pageable);
    }

    public Mensagem save(Mensagem mensagem) {
        mensagem.setDataEnvio(new java.util.Date());
        if(mensagem.getEntidadeOrigem().equals(ESPECIFICIDADES_GERAIS) && mensagem.getTipoRemetente().equals("FABRICANTE_IMPORTADOR")) {
            TipoEpiProcesso tipoEpiProcesso = tipoEpiProcessoRepository.getReferenceById(mensagem.getIdEntidadeOrigem());

            if(tipoEpiProcesso.getEmRevisaoEspecificidadesGerais() == null || !tipoEpiProcesso.getEmRevisaoEspecificidadesGerais()) {
                tipoEpiProcesso.setEmRevisaoEspecificidadesGerais(true);
                tipoEpiProcesso.setDataFimEspecificidadesGerais(null);
                tipoEpiProcessoRepository.save(tipoEpiProcesso);
            }
        }

        if(mensagem.getEntidadeOrigem().equals(PROTECAO)) {
            ProtecaoProcesso protecaoProcesso = protecaoProcessoRepository.getReferenceById(mensagem.getIdEntidadeOrigem());

            if(protecaoProcesso.getEmRevisao() == null || !protecaoProcesso.getEmRevisao()) {
                protecaoProcesso.setEmRevisao(true);
                protecaoProcesso.setDataFim(null);
                protecaoProcessoRepository.save(protecaoProcesso);
            }
        }

        return mensagemRepository.save(mensagem);
    }
}
