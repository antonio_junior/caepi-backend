package br.com.caepi.service;

import br.com.caepi.DTOS.AvancarEtapaLaboratorioOcpDTO;
import br.com.caepi.DTOS.IniciarEspecificidadesGeraisEpi;
import br.com.caepi.DTOS.IniciarProtecaoEpi;
import br.com.caepi.entities.*;
import br.com.caepi.repository.*;
import br.com.caepi.specifications.ProcessoSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProcessoService {

    private final ProcessoRepository processoRepository;

    private final ProtecaoProcessoRepository protecaoProcessoRepository;

    private final TipoEpiRepository tipoEpiRepository;

    private final EspecificidadeProcessoRepository especificidadeProcessoRepository;

    private final EspecificidadeOpcaoProcessoRepository especificidadeOpcaoProcessoRepository;

    private final NormaTecnicaProcessoRepository normaTecnicaProcessoRepository;

    private final NivelDesempenhoProcessoRepository nivelDesempenhoProcessoRepository;

    private final NivelDesempenhoRegraExibicaoProcessoRepository nivelDesempenhoRegraExibicaoProcessoRepository;

    private final NivelDesempenhoItemProcessoRepository nivelDesempenhoItemProcessoRepository;

    private final LaboratorioOCPRepository laboratorioOCPRepository;

    private final TipoEpiProcessoRepository tipoEpiProcessoRepository;

    @Autowired
    public ProcessoService(ProcessoRepository processoRepository,
                           ProtecaoProcessoRepository protecaoProcessoRepository,
                           TipoEpiRepository tipoEpiRepository,
                           EspecificidadeProcessoRepository especificidadeProcessoRepository,
                           EspecificidadeOpcaoProcessoRepository especificidadeOpcaoProcessoRepository,
                           NormaTecnicaProcessoRepository normaTecnicaProcessoRepository,
                           NivelDesempenhoProcessoRepository nivelDesempenhoProcessoRepository,
                           NivelDesempenhoRegraExibicaoProcessoRepository nivelDesempenhoRegraExibicaoProcessoRepository,
                           NivelDesempenhoItemProcessoRepository nivelDesempenhoItemProcessoRepository,
                           LaboratorioOCPRepository laboratorioOCPRepository,
                           TipoEpiProcessoRepository tipoEpiProcessoRepository) {
        this.processoRepository = processoRepository;
        this.protecaoProcessoRepository = protecaoProcessoRepository;
        this.tipoEpiRepository = tipoEpiRepository;
        this.especificidadeProcessoRepository = especificidadeProcessoRepository;
        this.especificidadeOpcaoProcessoRepository = especificidadeOpcaoProcessoRepository;
        this.normaTecnicaProcessoRepository = normaTecnicaProcessoRepository;
        this.nivelDesempenhoProcessoRepository = nivelDesempenhoProcessoRepository;
        this.nivelDesempenhoRegraExibicaoProcessoRepository = nivelDesempenhoRegraExibicaoProcessoRepository;
        this.nivelDesempenhoItemProcessoRepository = nivelDesempenhoItemProcessoRepository;
        this.laboratorioOCPRepository = laboratorioOCPRepository;
        this.tipoEpiProcessoRepository = tipoEpiProcessoRepository;
    }

    public Set<Processo> listarTodosProcesso() {
        List<Processo> processoList = processoRepository.findAll();
        Comparator<Processo> idComparator = Comparator.comparing(Processo::getId);
        return processoList.stream()
                .sorted(idComparator)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Optional<Processo> buscarPorId(Long id) {
        return processoRepository.findById(id);
    }

    public Processo criar(Processo processo) {
        Processo processoCriado = processoRepository.save(processo);

        Set<TipoEpiProcesso> epis = new HashSet<>();

        try {
            processoCriado.getEpis().stream().forEach(tipoEpiProcesso -> {
                List<ProtecaoProcesso> protecaoProcessos = new ArrayList<>();

                TipoEpi tipoEpi = tipoEpiRepository.findById(tipoEpiProcesso.getTipoEpiId()).get();

                tipoEpi.getTiposEpiSubTiposProtecao().stream()
                        .forEach(tipoEpiSubTipoProtecao -> {
                            ProtecaoProcesso protecaoProcesso = new ProtecaoProcesso();
                            protecaoProcesso.setSelecionado(null);
                            protecaoProcesso.setIdOrigem(tipoEpiSubTipoProtecao.getId()) ;
                            protecaoProcesso.setTipoEpiProcesso(tipoEpiProcesso);

                            if(tipoEpiSubTipoProtecao.getProtecaoBase() instanceof TipoProtecao) {
                                TipoProtecao tipoProtecao = (TipoProtecao) tipoEpiSubTipoProtecao.getProtecaoBase();
                                protecaoProcesso.setNome(tipoProtecao.getNome());
                                protecaoProcesso.setDescricao(tipoProtecao.getDescricao());

                            } else if(tipoEpiSubTipoProtecao.getProtecaoBase() instanceof SubTipoProtecao) {
                                SubTipoProtecao subTipoProtecao = (SubTipoProtecao) tipoEpiSubTipoProtecao.getProtecaoBase();
                                protecaoProcesso.setNome(subTipoProtecao.getTipoProtecao().getNome() + " - " + subTipoProtecao.getNome());
                                protecaoProcesso.setDescricao(subTipoProtecao.getDescricao());

                            }

                            ProtecaoProcesso protecaoProcessoCriado = protecaoProcessoRepository.save(protecaoProcesso);

                            Set<Especificidade> especificidades = tipoEpi.getEspecificidades().stream()
                                    .filter(esp -> esp.getProtecaoId() != null && esp.getProtecaoId().equals(protecaoProcessoCriado.getIdOrigem())).collect(Collectors.toSet());
                            try {
                                if (especificidades.size() > 0) {
                                    List<EspecificidadeProcesso> especificidadeProcessos = this.criarEspecificidadesProcesso(
                                            especificidades,
                                            "protecao",
                                            protecaoProcessoCriado
                                    );

                                    protecaoProcessoCriado.setEspecificidades(new HashSet<>(especificidadeProcessos));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            protecaoProcessos.add(protecaoProcessoCriado);

                            if(tipoEpiSubTipoProtecao.getProtecaoBase().getNormasTecnicas().size() > 0) {
                                criarNormasTecnicasProcesso(tipoEpiSubTipoProtecao.getProtecaoBase().getNormasTecnicas(), protecaoProcessoCriado);
                            }
                        });
                tipoEpiProcesso.setProtecoes(new HashSet<>(protecaoProcessos));

                try {
                    List<EspecificidadeProcesso> especificidadeProcessos = this.criarEspecificidadesProcesso(
                            tipoEpi.getEspecificidades().stream().filter(esp -> esp.getProtecaoId() == null).collect(Collectors.toSet()),
                            "geral",
                            tipoEpiProcesso
                    );

                    tipoEpiProcesso.setEspecificidadesGerais(new HashSet<>(especificidadeProcessos));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                epis.add(tipoEpiProcesso);
            });

            processoCriado.setEpis(epis);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return processoRepository.save(processoCriado);
    }

    public Processo atualizar(Processo processo) {
        Processo processoExistente = processoRepository.findById(processo.getId()).orElseThrow(() -> new RuntimeException("Processo not found"));

        if(processoExistente.getEtapa().equals(2)) {
            Set<LaboratorioOCP> labs = getLaboratoriosFromProcesso(processo);

            processoExistente.getEpis().forEach(epi -> {
                Optional<TipoEpiProcesso> optionalEpi = processo.getEpis().stream().filter(epi1 -> epi1.getId().equals(epi.getId())).findFirst();
                optionalEpi.ifPresent(epiProcesso -> epi.setLaboratorios(filterLaboratorios(labs, epiProcesso.getLaboratorios())));
            });
        }

        return processoRepository.save(processoExistente);
    }

    private List<StatusLaboratorioOCPProcesso> criarStatusLaboratorioOCPProcessos(Long processoId, Set<LaboratorioOCP> labs) {
        List<StatusLaboratorioOCPProcesso> statusLaboratorioOCPProcessos = new ArrayList<>();
        labs.stream().forEach(lab -> {
            StatusLaboratorioOCPProcesso statusLaboratorioOCPProcesso = new StatusLaboratorioOCPProcesso();
            statusLaboratorioOCPProcesso.setEtapa(1);
            statusLaboratorioOCPProcesso.setLaboratorioOcpId(lab.getId());
            statusLaboratorioOCPProcesso.setProcessoId(processoId);
            statusLaboratorioOCPProcesso.setEtapas(criarEtapas());
            statusLaboratorioOCPProcessos.add(statusLaboratorioOCPProcesso);
        });
        return statusLaboratorioOCPProcessos;
    }

    private List<StatusEtapa> criarEtapas() {
        List<StatusEtapa> statusEtapas = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            StatusEtapa statusEtapa = new StatusEtapa();
            statusEtapa.setEtapa(i);
            statusEtapas.add(statusEtapa);
        }
        return statusEtapas;
    }

    public Processo avancarEtapaFabricante(Processo processo) {
        Processo processoExistente = processoRepository.findById(processo.getId()).orElseThrow(() -> new RuntimeException("Processo not found"));

        if(processoExistente.getEtapa().equals(1)) {
            processoExistente.setEtapa(2);
            processoExistente.setDataRealizacaoEtapa1(new Date());
        } else if(processoExistente.getEtapa().equals(2)) {
            processoExistente.setEtapa(3);
            processoExistente.setDataRealizacaoEtapa2(new Date());

            Set<LaboratorioOCP> labs = getLaboratoriosFromProcesso(processo);

            processoExistente.setStatusLaboratorioOCPProcessos(criarStatusLaboratorioOCPProcessos(processo.getId(), labs));

            processoExistente.getEpis().forEach(epi -> {
                Optional<TipoEpiProcesso> optionalEpi = processo.getEpis().stream().filter(epi1 -> epi1.getId().equals(epi.getId())).findFirst();
                optionalEpi.ifPresent(epiProcesso -> epi.setLaboratorios(filterLaboratorios(labs, epiProcesso.getLaboratorios())));
            });
        } else if(processoExistente.getEtapa().equals(3)) {
            processoExistente.setEtapa(4);
            processoExistente.setDataRealizacaoEtapa3(new Date());
        }

        return processoRepository.save(processoExistente);

    }

    public Processo avancarEtapaLaboratorioOCP(AvancarEtapaLaboratorioOcpDTO data) {
        Processo processoExistente = processoRepository.findById(data.getProcessoId()).orElseThrow(() -> new RuntimeException("Processo not found"));

        StatusLaboratorioOCPProcesso statusLaboratorioOCPProcesso = processoExistente.getStatusLaboratorioOCPProcessos().stream()
                .filter(status -> status.getLaboratorioOcpId().equals(data.getLaboratorioOcpId())).findFirst().orElseThrow(() -> new RuntimeException("StatusLaboratorioOCPProcesso not found"));

        if(statusLaboratorioOCPProcesso.getEtapa().equals(1)) {
            statusLaboratorioOCPProcesso.setEtapa(2);
            statusLaboratorioOCPProcesso.getEtapas().stream().filter(etapa -> etapa.getEtapa().equals(1)).findFirst().get().setDataFim(new Date());
            statusLaboratorioOCPProcesso.getEtapas().stream().filter(etapa -> etapa.getEtapa().equals(2)).findFirst().get().setDataInicio(new Date());
        } else if(statusLaboratorioOCPProcesso.getEtapa().equals(2)) {
            statusLaboratorioOCPProcesso.setEtapa(3);
            statusLaboratorioOCPProcesso.getEtapas().stream().filter(etapa -> etapa.getEtapa().equals(2)).findFirst().get().setDataFim(new Date());
            statusLaboratorioOCPProcesso.getEtapas().stream().filter(etapa -> etapa.getEtapa().equals(3)).findFirst().get().setDataInicio(new Date());
        } else if(statusLaboratorioOCPProcesso.getEtapa().equals(3)) {
            statusLaboratorioOCPProcesso.setEtapa(4);
            statusLaboratorioOCPProcesso.getEtapas().stream().filter(etapa -> etapa.getEtapa().equals(3)).findFirst().get().setDataFim(new Date());
            statusLaboratorioOCPProcesso.getEtapas().stream().filter(etapa -> etapa.getEtapa().equals(4)).findFirst().get().setDataInicio(new Date());
        }

        return processoRepository.save(processoExistente);
    }

    private Set<LaboratorioOCP> getLaboratoriosFromProcesso(Processo processo) {
        return processo.getEpis().stream()
                .flatMap(epi -> epi.getLaboratorios().stream())
                .map(lab -> laboratorioOCPRepository.findById(lab.getId()).orElseThrow(() -> new RuntimeException("LaboratorioOCP not found")))
                .collect(Collectors.toSet());
    }

    private Set<LaboratorioOCP> filterLaboratorios(Set<LaboratorioOCP> labs, Set<LaboratorioOCP> laboratorios) {
        return labs.stream()
                .filter(lab -> laboratorios.stream().anyMatch(lab1 -> lab1.getId().equals(lab.getId())))
                .collect(Collectors.toSet());
    }

    public Page<Processo> findAll(Map<String, String> queryParameters, Pageable pageable) {
        Specification<Processo> spec = new ProcessoSpecification(queryParameters);
        return processoRepository.findAll(spec, pageable);
    }

    public void deletar(Long id) {
        processoRepository.deleteById(id);
    }

    public void iniciarEspecificidadesGeraisEpiProcesso(IniciarEspecificidadesGeraisEpi data) {
        Processo processo = processoRepository.findById(data.getProcessoId()).get();
        LaboratorioOCP laboratorioOCP = laboratorioOCPRepository.findById(data.getLaboratorioId()).get();

        processo.getEpis().stream().filter(epi -> epi.getId().equals(data.getTipoEpiProcessoId())).forEach(epi -> {
            if(epi.getDataInicioEspeficidadesGerais() != null || epi.getAutorEspecificidadesGerais() != null) {
                throw new RuntimeException("As especificidades gerais já foram iniciadas para este EPI.");
            }

            epi.setAutorEspecificidadesGerais(laboratorioOCP);
            epi.setDataInicioEspeficidadesGerais(new Date());

            tipoEpiProcessoRepository.save(epi);
        });
    }

    public void salvarEspecificidadesGeraisEpiProcesso(Long id, Set<EspecificidadeProcesso> data) {
        TipoEpiProcesso tipoEpiProcesso = tipoEpiProcessoRepository.findById(id).get();
        data.stream().forEach(especificidadeProcesso -> {
            especificidadeProcesso.setTipoEpiProcesso(tipoEpiProcesso);
        });
        tipoEpiProcesso.setEspecificidadesGerais(data);
        tipoEpiProcessoRepository.save(tipoEpiProcesso);
    }

    public void finalizarEspecificidadesGeraisEpiProcesso(Long id, Set<EspecificidadeProcesso> data) {
        TipoEpiProcesso tipoEpiProcesso = tipoEpiProcessoRepository.findById(id).get();
        data.stream().forEach(especificidadeProcesso -> {
            especificidadeProcesso.setTipoEpiProcesso(tipoEpiProcesso);
        });
        tipoEpiProcesso.setEspecificidadesGerais(data);
        tipoEpiProcesso.setDataFimEspecificidadesGerais(new Date());
        tipoEpiProcesso.setEmRevisaoEspecificidadesGerais(false);
        tipoEpiProcessoRepository.save(tipoEpiProcesso);
    }

    public void reiniciarEspecificidadesGeraisEpiProcesso(Long id) {
        TipoEpiProcesso tipoEpiProcesso = tipoEpiProcessoRepository.findById(id).get();
        tipoEpiProcesso.setDataFimEspecificidadesGerais(null);
        tipoEpiProcesso.setEmRevisaoEspecificidadesGerais(null);
        tipoEpiProcessoRepository.save(tipoEpiProcesso);
    }

    public void iniciarProtecaoEpiProcesso(IniciarProtecaoEpi data) {
        Processo processo = processoRepository.findById(data.getProcessoId()).get();
        LaboratorioOCP laboratorioOCP = laboratorioOCPRepository.findById(data.getLaboratorioId()).get();

        processo.getEpis().stream().filter(epi -> epi.getId().equals(data.getTipoEpiProcessoId())).forEach(epi -> {

            epi.getProtecoes().stream().filter(prot -> prot.getId().equals(data.getProtecaoId())).forEach(prot -> {
                if(prot.getDataInicio() != null || prot.getAutor() != null) {
                    throw new RuntimeException("A cadastro dessa proteção já foi iniciado.");
                }

                prot.setAutor(laboratorioOCP);
                prot.setDataInicio(new Date());
                prot.setSelecionado(true);

                if(prot.getEspecificidades().size() == 0) {
                    prot.setDataFim(new Date());
                }

                protecaoProcessoRepository.save(prot);
            });
        });
    }

    public void salvarProtecaoEpiProcesso(Long id, Set<EspecificidadeProcesso> data) {
        ProtecaoProcesso protecaoProcesso = protecaoProcessoRepository.findById(id).get();
        data.stream().forEach(especificidadeProcesso -> {
            especificidadeProcesso.setProtecaoProcesso(protecaoProcesso);
        });
        protecaoProcesso.setEspecificidades(data);
        protecaoProcessoRepository.save(protecaoProcesso);
    }

    public void finalizarProtecaoEpiProcesso(Long id, Set<EspecificidadeProcesso> data) {
        ProtecaoProcesso protecaoProcesso = protecaoProcessoRepository.findById(id).get();
        data.stream().forEach(especificidadeProcesso -> {
            especificidadeProcesso.setProtecaoProcesso(protecaoProcesso);
        });
        protecaoProcesso.setEspecificidades(data);
        protecaoProcesso.setDataFim(new Date());
        protecaoProcesso.setEmRevisao(false);
        protecaoProcessoRepository.save(protecaoProcesso);
    }

    public void reiniciarProtecaoEpiProcesso(Long id) {
        ProtecaoProcesso protecaoProcesso = protecaoProcessoRepository.findById(id).get();
        protecaoProcesso.setDataFim(null);
        protecaoProcesso.setEmRevisao(null);
        protecaoProcessoRepository.save(protecaoProcesso);
    }

    public void resetarProtecaoEpiProcesso(IniciarProtecaoEpi data) {
        Processo processo = processoRepository.findById(data.getProcessoId()).get();
        LaboratorioOCP laboratorioOCP = laboratorioOCPRepository.findById(data.getLaboratorioId()).get();

        processo.getEpis().stream().filter(epi -> epi.getId().equals(data.getTipoEpiProcessoId())).forEach(epi -> {

            epi.getProtecoes().stream().filter(prot -> prot.getId().equals(data.getProtecaoId())).forEach(prot -> {
                prot.setAutor(null);
                prot.setDataInicio(null);
                prot.setSelecionado(null);
                prot.setDataFim(null);

                protecaoProcessoRepository.save(prot);

                prot.getEspecificidades().stream().forEach(especificidadeProcesso -> {
                    especificidadeProcesso.setResposta(null);
                    especificidadeProcessoRepository.save(especificidadeProcesso);
                });
            });
        });
    }

    private List<EspecificidadeProcesso> criarEspecificidadesProcesso(Set<Especificidade> especificidades, String tipo, Object entity) {
        List<EspecificidadeProcesso> especificidadeProcessos = new ArrayList<>();
        try {
        especificidades.stream().forEach(especificidade -> {
            EspecificidadeProcesso especificidadeProcesso = new EspecificidadeProcesso();
            especificidadeProcesso.setTitulo(especificidade.getTitulo());
            especificidadeProcesso.setTipo(especificidade.getTipo());
            especificidadeProcesso.setOrdem(especificidade.getOrdem());
            especificidadeProcesso.setIdOrigem(especificidade.getId());
            especificidadeProcesso.setResposta(null);
            especificidadeProcesso.setObrigatorio(especificidade.getObrigatorio());

            if(tipo.equals("geral")) {
                TipoEpiProcesso tipoEpiProcesso = (TipoEpiProcesso) entity;
                especificidadeProcesso.setTipoEpiProcesso(tipoEpiProcesso);
            } else if(tipo.equals("protecao")) {
                ProtecaoProcesso protecaoProcesso = (ProtecaoProcesso) entity;
                especificidadeProcesso.setProtecaoProcesso(protecaoProcesso);
            }

//            if(especificidade.getProtecaoId() != null) {
//                especificidadeProcesso.setProtecaoProcesso(protecaoProcessos.stream()
//                        .filter(protecaoProcesso -> especificidade.getProtecaoId().equals(protecaoProcesso.getIdOrigem())).findFirst().get());
//            }

            EspecificidadeProcesso especificidadePai = especificidadeProcessos.stream()
                    .filter(especificidadeProcesso1 -> especificidadeProcesso1.getIdOrigem().equals(especificidade.getEspecificidadePai()))
                    .findFirst()
                    .orElse(null);

            especificidadeProcesso.setEspecificidadePai(especificidadePai != null ? especificidadePai.getId() : null);

            if(especificidade.getOpcoesRespostaPai() != null && especificidadePai != null) {
                especificidadeProcesso.setOpcoesRespostaPai(especificidadePai.getOpcoes().stream()
                        .filter(opcao -> Arrays.asList(especificidade.getOpcoesRespostaPai()).contains(opcao.getIdOrigem().toString()))
                        .map(opcao -> opcao.getId().toString()).toArray(String[]::new));
            }

            EspecificidadeProcesso especificidadeProcessoCriada = especificidadeProcessoRepository.save(especificidadeProcesso);

            Set<EspecificidadeOpcaoProcesso> opcoes = new HashSet<>();

            especificidade.getOpcoes().stream().forEach(opcao -> {
                EspecificidadeOpcaoProcesso opcaoProcesso = new EspecificidadeOpcaoProcesso();
                opcaoProcesso.setDescricao(opcao.getDescricao());
                opcaoProcesso.setIndicacaoDeUso(opcao.getIndicacaoDeUso());
                opcaoProcesso.setOrdem(opcao.getOrdem());
                opcaoProcesso.setIdOrigem(opcao.getId());
                opcaoProcesso.setEspecificidadeProcesso(especificidadeProcessoCriada);

                if(opcao.getRegraExibicao() != null) {
                    EspecificidadeProcesso especificidadeResposta = especificidadeProcessos.stream()
                            .filter(especificidadeProcesso1 -> especificidadeProcesso1.getIdOrigem().equals(opcao.getRegraExibicao().getEspecificidade().getId()))
                            .findFirst()
                            .orElse(null);

                    opcaoProcesso.setEspecificidadeRespostaId(especificidadeResposta.getId());

                    Set<EspecificidadeOpcaoProcesso> opcoesRespostaRegras = especificidadeResposta.getOpcoes().stream()
                            .filter(opcaoResposta -> opcao.getRegraExibicao().getRegrasEspecificidadesOpcoes()
                                    .contains(opcaoResposta.getIdOrigem()))
                            .collect(Collectors.toSet());

                    opcaoProcesso.setEspecificidadeRespostaRegra(opcoesRespostaRegras.stream()
                            .map(item -> item.getId().toString()).collect(Collectors.joining(",")));
                }

                EspecificidadeOpcaoProcesso especificidadeOpcaoProcessoCriada = especificidadeOpcaoProcessoRepository.save(opcaoProcesso);

                if(opcao.getNormasTecnicas().size() > 0) {
                    criarNormasTecnicasProcesso(opcao.getNormasTecnicas(), especificidadeOpcaoProcessoCriada);
                }

                opcoes.add(especificidadeOpcaoProcessoCriada);
            });

            especificidadeProcessoCriada.setOpcoes(opcoes);

            especificidadeProcessoRepository.save(especificidadeProcessoCriada);

            especificidadeProcessos.add(especificidadeProcessoCriada);
        });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return especificidadeProcessos;
    }

    private void criarNormasTecnicasProcesso(Set<NormaTecnica> normaTecnicas, Object entity) {
        Set<NormaTecnicaProcesso> normaTecnicaProcessos = new HashSet<>();
        if(normaTecnicas != null) {
            normaTecnicas.stream().forEach(normaTecnica -> {
                NormaTecnicaProcesso normaTecnicaProcesso = new NormaTecnicaProcesso();
                normaTecnicaProcesso.setNome(normaTecnica.getNome());
                normaTecnicaProcesso.setIdOrigem(normaTecnica.getId());
                normaTecnicaProcesso.setDescricao(normaTecnica.getDescricao());
                normaTecnicaProcesso.setNomeNormalizado(normaTecnica.getNomeNormalizado());
                normaTecnicaProcesso.setObservacaoNiveisDesempenhos(normaTecnica.getObservacaoNiveisDesempenhos());
                normaTecnicaProcesso.setPictograma(normaTecnica.getPictograma());

                if(entity instanceof ProtecaoProcesso) {
                    normaTecnicaProcesso.setProtecaoProcesso((ProtecaoProcesso) entity);
                } else if(entity instanceof EspecificidadeOpcaoProcesso) {
                    normaTecnicaProcesso.setEspecificidadeOpcaoProcesso((EspecificidadeOpcaoProcesso) entity);
                }

                NormaTecnicaProcesso normaTecnicaProcessoCriada = normaTecnicaProcessoRepository.save(normaTecnicaProcesso);

                if(normaTecnica.getNiveisDesempenhos().size() > 0) {
                    criarNivelDesempenhosProcesso(normaTecnica.getNiveisDesempenhos(), normaTecnicaProcessoCriada, null);
                }

                normaTecnicaProcessos.add(normaTecnicaProcessoCriada);
            });

            if(entity instanceof ProtecaoProcesso) {
                ProtecaoProcesso protecaoProcesso = (ProtecaoProcesso) entity;
                protecaoProcesso.setNormaTecnicaProcessos(normaTecnicaProcessos);
            } else if(entity instanceof EspecificidadeOpcaoProcesso) {
                EspecificidadeOpcaoProcesso especificidadeOpcaoProcesso = (EspecificidadeOpcaoProcesso) entity;
                especificidadeOpcaoProcesso.setNormaTecnicaProcessos(normaTecnicaProcessos);
            }
        }
    }

    private void criarNivelDesempenhosProcesso(Set<NivelDesempenho> nivelDesempenhos, NormaTecnicaProcesso normaTecnicaProcesso, NivelDesempenhoProcesso nivelDesempenhoProcessoPai) {
        Set<NivelDesempenhoProcesso> nivelDesempenhoProcessos = new HashSet<>();
        if(nivelDesempenhos != null) {
            nivelDesempenhos.stream().forEach(nivelDesempenho -> {
                NivelDesempenhoProcesso nivelDesempenhoProcesso = new NivelDesempenhoProcesso();
                nivelDesempenhoProcesso.setTitulo(nivelDesempenho.getTitulo());
                nivelDesempenhoProcesso.setIdOrigem(nivelDesempenho.getId());
                nivelDesempenhoProcesso.setIdentificacao(nivelDesempenho.getIdentificacao());
                nivelDesempenhoProcesso.setInformacao(nivelDesempenho.getInformacao());
                nivelDesempenhoProcesso.setIlimitado(nivelDesempenho.getIlimitado());
                nivelDesempenhoProcesso.setOrdem(nivelDesempenho.getOrdem());
                nivelDesempenhoProcesso.setCasasDecimais(nivelDesempenho.getCasasDecimais());
                nivelDesempenhoProcesso.setNumCaracteres(nivelDesempenho.getNumCaracteres());
                nivelDesempenhoProcesso.setTipo(nivelDesempenho.getTipo());
                nivelDesempenhoProcesso.setNivel(nivelDesempenho.getNivel());
                nivelDesempenhoProcesso.setTextoIlimitado(nivelDesempenho.getTextoIlimitado());
                nivelDesempenhoProcesso.setVlrmax(nivelDesempenho.getVlrmax());
                nivelDesempenhoProcesso.setVlrmin(nivelDesempenho.getVlrmin());
                nivelDesempenhoProcesso.setNormaTecnicaProcesso(normaTecnicaProcesso);
                if(nivelDesempenhoProcessoPai != null) {
                    nivelDesempenhoProcesso.setNivelDesempenhoPai(nivelDesempenhoProcessoPai);
                }

                NivelDesempenhoProcesso nivelDesempenhoProcessoCriado = nivelDesempenhoProcessoRepository.save(nivelDesempenhoProcesso);

                if(nivelDesempenho.getItems().size() > 0) {
                    criarNivelDesempenhoItems(nivelDesempenho.getItems(), nivelDesempenhoProcessoCriado);
                }

                if(nivelDesempenho.getRegrasExibicao().size() > 0) {
                    criarNivelDesempenhoRegrasExibicao(nivelDesempenho.getRegrasExibicao(), nivelDesempenhoProcessoCriado, nivelDesempenhoProcessoPai);
                }

                if(nivelDesempenho.getNivelDesempenhoSubsequentes().size() > 0) {
                    criarNivelDesempenhosProcesso(new HashSet<>(nivelDesempenho.getNivelDesempenhoSubsequentes()), normaTecnicaProcesso, nivelDesempenhoProcessoCriado);
                }

                nivelDesempenhoProcessos.add(nivelDesempenhoProcessoCriado);
            });
        }

        normaTecnicaProcesso.setNiveisDesempenhosProcesso(nivelDesempenhoProcessos);
    }

    private void criarNivelDesempenhoItems(Set<NivelDesempenhoItem> items, NivelDesempenhoProcesso nivelDesempenhoProcessoCriado) {
        Set<NivelDesempenhoItemProcesso> nivelDesempenhoItemProcessos = new HashSet<>();
        items.stream().forEach(item -> {
            NivelDesempenhoItemProcesso nivelDesempenhoItemProcesso = new NivelDesempenhoItemProcesso();
            nivelDesempenhoItemProcesso.setRotulo(item.getRotulo());
            nivelDesempenhoItemProcesso.setTipo(item.getTipo());
            nivelDesempenhoItemProcesso.setOrdem(item.getOrdem());
            nivelDesempenhoItemProcesso.setOpcoes(item.getOpcoes());
            nivelDesempenhoItemProcesso.setInformacao(item.getInformacao());
            nivelDesempenhoItemProcesso.setTextoIlimitado(item.getTextoIlimitado());
            nivelDesempenhoItemProcesso.setNumCaracteres(item.getNumCaracteres());
            nivelDesempenhoItemProcesso.setCasasDecimais(item.getCasasDecimais());
            nivelDesempenhoItemProcesso.setIlimitado(item.getIlimitado());
            nivelDesempenhoItemProcesso.setVlrmin(item.getVlrmin());
            nivelDesempenhoItemProcesso.setVlrmax(item.getVlrmax());
            nivelDesempenhoItemProcesso.setNivelDesempenhoProcesso(nivelDesempenhoProcessoCriado);
            nivelDesempenhoItemProcesso.setIdOrigem(item.getId());
            NivelDesempenhoItemProcesso nivelDesempenhoItemProcessoCriado = nivelDesempenhoItemProcessoRepository.save(nivelDesempenhoItemProcesso);
            nivelDesempenhoItemProcessos.add(nivelDesempenhoItemProcessoCriado);
        });
        nivelDesempenhoProcessoCriado.setItems(nivelDesempenhoItemProcessos);
    }


    private void criarNivelDesempenhoRegrasExibicao(Set<NivelDesempenhoRegraExibicao> nivelDesempenhoRegraExibicaos, NivelDesempenhoProcesso nivelDesempenhoProcessoCriado, NivelDesempenhoProcesso nivelDesempenhoProcessoPai){
        Set<NivelDesempenhoRegraExibicaoProcesso> nivelDesempenhoRegraExibicaoProcessos = new HashSet<>();

        nivelDesempenhoRegraExibicaos.stream().forEach(regra -> {
            NivelDesempenhoRegraExibicaoProcesso nivelDesempenhoRegraExibicaoProcesso = new NivelDesempenhoRegraExibicaoProcesso();
            nivelDesempenhoRegraExibicaoProcesso.setNivelDesempenhoProcesso(nivelDesempenhoProcessoCriado);
            nivelDesempenhoRegraExibicaoProcesso.setIdOrigem(regra.getId());
            nivelDesempenhoRegraExibicaoProcesso.setOperador(regra.getOperador());
            nivelDesempenhoRegraExibicaoProcesso.setValorReferencia(regra.getValorReferencia());
            nivelDesempenhoRegraExibicaoProcesso.setOrdem(regra.getOrdem());
            nivelDesempenhoRegraExibicaoProcesso.setOperadorLogico(regra.getOperadorLogico());
            nivelDesempenhoRegraExibicaoProcesso.setCampoIdentificador(nivelDesempenhoProcessoPai.getId());
            Optional<NivelDesempenhoItemProcesso> itemIdentificador = nivelDesempenhoProcessoPai.getItems().stream()
                    .filter(item -> item.getIdOrigem().equals(regra.getItemIdentificador())).findFirst();

            if(itemIdentificador.isPresent()) {
                nivelDesempenhoRegraExibicaoProcesso.setItemIdentificador(itemIdentificador.get().getId());
            }


            NivelDesempenhoRegraExibicaoProcesso nivelDesempenhoRegraExibicaoProcessoCriado = nivelDesempenhoRegraExibicaoProcessoRepository.save(nivelDesempenhoRegraExibicaoProcesso);

            nivelDesempenhoRegraExibicaoProcessos.add(nivelDesempenhoRegraExibicaoProcessoCriado);
        });

        nivelDesempenhoProcessoCriado.setRegrasExibicao(nivelDesempenhoRegraExibicaoProcessos);
    }


    public void saveOrUpdateNormaTecnicaProcesso(NormaTecnicaProcesso normaTecnicaProcesso) {
        setNormaTecnicaProcessoInNiveisDesempenho(normaTecnicaProcesso);
        normaTecnicaProcessoRepository.save(normaTecnicaProcesso);
    }

    // Método recursivo para ajustar a referência em todos os níveis de desempenho e seus subsequentes
    private void setNormaTecnicaProcessoInNiveisDesempenho(NormaTecnicaProcesso normaTecnicaProcesso) {
        if (normaTecnicaProcesso.getNiveisDesempenhosProcesso() != null) {
            for (NivelDesempenhoProcesso nivel : normaTecnicaProcesso.getNiveisDesempenhosProcesso()) {
                nivel.setNormaTecnicaProcesso(normaTecnicaProcesso);
                setNormaTecnicaProcessoInNiveisDesempenhoSubsequentes(nivel, normaTecnicaProcesso);
            }
        }
    }

    // Método recursivo para ajustar a referência em todos os níveis de desempenho subsequentes
    private void setNormaTecnicaProcessoInNiveisDesempenhoSubsequentes(NivelDesempenhoProcesso nivel, NormaTecnicaProcesso normaTecnicaProcesso) {
        if (nivel.getNivelDesempenhoProcessoSubsequentes() != null) {
            for (NivelDesempenhoProcesso subNivel : nivel.getNivelDesempenhoProcessoSubsequentes()) {
                subNivel.setNormaTecnicaProcesso(normaTecnicaProcesso);
                setNormaTecnicaProcessoInNiveisDesempenhoSubsequentes(subNivel, normaTecnicaProcesso);
            }
        }
    }
}
