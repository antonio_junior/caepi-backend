package br.com.caepi.service;

import br.com.caepi.entities.Estado;
import br.com.caepi.repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service(value = "EstadoService")
public class EstadoService {
	
	@Autowired
	private EstadoRepository estadoRepository;
	
	public List<Estado> findAll() {
		List<Estado> list = new ArrayList<>();
		estadoRepository.findAll().iterator().forEachRemaining(list::add);
		return list;
	}
	
}
