package br.com.caepi.service;

import br.com.caepi.entities.NivelDesempenho;
import br.com.caepi.entities.NivelDesempenhoItem;
import br.com.caepi.repository.NivelDesempenhoItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class NivelDesempenhoItemService {

    @Autowired
    NivelDesempenhoItemRepository nivelDesempenhoItemRepository;

    public NivelDesempenhoItem salvar(NivelDesempenhoItem nivelDesempenhoItem) {
        NivelDesempenho nivelDesempenho = nivelDesempenhoItem.getNivelDesempenho();
        nivelDesempenhoItem.setOrdem(this.gerarProximaOrdemByNivelDesempenho(nivelDesempenho));
        return this.nivelDesempenhoItemRepository.save(nivelDesempenhoItem);
    }

    public NivelDesempenhoItem atualizar(NivelDesempenhoItem nivelDesempenhoItem) {
        return this.nivelDesempenhoItemRepository.save(nivelDesempenhoItem);
    }

    public Optional<NivelDesempenhoItem> buscarPorId(Long id) {
        return nivelDesempenhoItemRepository.findById(id);
    }

    public void excluir(Long id) {
        this.nivelDesempenhoItemRepository.deleteById(id);
    }

    private Integer gerarProximaOrdemByNivelDesempenho(NivelDesempenho nivelDesempenho) {
        Integer ordem = 1;
        Integer lastOrdem = nivelDesempenhoItemRepository.findMaxOrdemNivelDesempenhoItemByNivelDesempenho(nivelDesempenho);
        if(lastOrdem != null) {
            return lastOrdem + 1;
        }
        return ordem;
    }
}
