package br.com.caepi.service;

import br.com.caepi.entities.ArquivoProcesso;
import br.com.caepi.entities.Empresa;
import br.com.caepi.entities.Processo;
import br.com.caepi.repository.ArquivoProcessoRepository;
import br.com.caepi.repository.EmpresaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@Service
public class ArquivoProcessoService {

    private final ArquivoProcessoRepository arquivoProcessoRepository;
    private final EmpresaRepository empresaRepository;

    @Value("${file.upload-dir}")
    private String directory;

    @Autowired
    public ArquivoProcessoService(ArquivoProcessoRepository arquivoProcessoRepository, EmpresaRepository empresaRepository) {
        this.arquivoProcessoRepository = arquivoProcessoRepository;
        this.empresaRepository = empresaRepository;
    }

    public ArquivoProcesso salvarArquivo(MultipartFile file, Processo processo, String codigo, String descricao, Integer etapa,
                                         Long empresaId,
                                         String tipo,
                                         String visibilidade) throws IOException {
        String originalFileName = file.getOriginalFilename();
        if (originalFileName == null) {
            throw new IllegalArgumentException("O nome do arquivo não pode ser nulo.");
        }

        String fileExtension = "";
        int indexOfDot = originalFileName.lastIndexOf('.');
        if (indexOfDot > 0) {
            fileExtension = originalFileName.substring(indexOfDot);
        }

        Path filePath = Paths.get(directory + originalFileName);

        try (InputStream inputStream = file.getInputStream()) {
            Files.copy(inputStream, filePath);
        } catch (FileAlreadyExistsException e) {
            throw new RuntimeException("Já existe um arquivo com o nome: " + originalFileName);
        } catch (IOException e) {
            throw new RuntimeException("Erro ao salvar o arquivo: " + e.getMessage());
        }

        if (codigo == null || codigo.trim().isEmpty()) {
            codigo = "";
        }

        if (descricao == null || descricao.trim().isEmpty()) {
            descricao = "";
        }

        ArquivoProcesso arquivoProcesso = new ArquivoProcesso();
        arquivoProcesso.setNomeArquivo(originalFileName);
        arquivoProcesso.setCaminhoArquivo(filePath.toString());
        arquivoProcesso.setCodigo(codigo);
        arquivoProcesso.setDescricao(descricao);
        arquivoProcesso.setProcesso(processo);
        arquivoProcesso.setEtapa(etapa);

        arquivoProcesso.setTipo(tipo);
        arquivoProcesso.setVisibilidade(visibilidade);

        if (empresaId != null) {
            Empresa empresa = empresaRepository.findById(empresaId)
                    .orElseThrow(() -> new IllegalArgumentException("Empresa não encontrada para o ID " + empresaId));
            arquivoProcesso.setEmpresa(empresa);
        }

        return arquivoProcessoRepository.save(arquivoProcesso);
    }

    public byte[] downloadArquivo(Long arquivoId) throws IOException {
        Optional<ArquivoProcesso> arquivoProcessoOptional = arquivoProcessoRepository.findById(arquivoId);
        if (arquivoProcessoOptional.isPresent()) {
            Path filePath = Paths.get(arquivoProcessoOptional.get().getCaminhoArquivo());
            return Files.readAllBytes(filePath);
        }
        return null;
    }

    public void deletarArquivo(Long arquivoId) throws IOException {
        Optional<ArquivoProcesso> arquivoProcessoOptional = arquivoProcessoRepository.findById(arquivoId);
        if (arquivoProcessoOptional.isPresent()) {
            Path filePath = Paths.get(arquivoProcessoOptional.get().getCaminhoArquivo());
            Files.deleteIfExists(filePath);
            arquivoProcessoRepository.deleteById(arquivoId);
        }
    }

    public Optional<ArquivoProcesso> findById(Long arquivoId) {
        return arquivoProcessoRepository.findById(arquivoId);
    }
}