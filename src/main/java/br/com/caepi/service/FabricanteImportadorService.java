package br.com.caepi.service;

import br.com.caepi.entities.FabricanteImportador;
import br.com.caepi.entities.LaboratorioOCP;
import br.com.caepi.entities.TipoEpi;
import br.com.caepi.repository.FabricanteImportadorRepository;
import br.com.caepi.repository.LaboratorioOCPRepository;
import br.com.caepi.repository.TipoEpiRepository;
import br.com.caepi.specifications.FabricanteImportadorSpecification;
import br.com.caepi.specifications.LaboratorioOCPSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class FabricanteImportadorService {

    private final FabricanteImportadorRepository fabricanteImportadorRepository;
    private final TipoEpiRepository tipoEpiRepository;

    @Autowired
    public FabricanteImportadorService(FabricanteImportadorRepository fabricanteImportadorRepository, TipoEpiRepository tipoEpiRepository) {
        this.fabricanteImportadorRepository = fabricanteImportadorRepository;
        this.tipoEpiRepository = tipoEpiRepository;
    }

    public Set<FabricanteImportador> listarTodosFabricantesImportadores() {
        List<FabricanteImportador> fabricanteImportadorList = fabricanteImportadorRepository.findAll();
        Comparator<FabricanteImportador> idComparator = Comparator.comparing(FabricanteImportador::getId);
        return fabricanteImportadorList.stream()
                .sorted(idComparator)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Page<FabricanteImportador> findAll(Map<String, String> queryParameters, Pageable pageable) {
        Specification<FabricanteImportador> spec = new FabricanteImportadorSpecification(queryParameters);
        return fabricanteImportadorRepository.findAll(spec, pageable);
    }

    public Optional<FabricanteImportador> buscarPorId(Long id) {
        return fabricanteImportadorRepository.findById(id);
    }

    public FabricanteImportador salvar(FabricanteImportador fabricanteImportador) {
        return fabricanteImportadorRepository.save(fabricanteImportador);
    }

    public FabricanteImportador atualizar(FabricanteImportador fabricanteImportadorAtualizado) {
        if (fabricanteImportadorAtualizado.getId() == null) {
            throw new EntityNotFoundException("O ID do FabricanteImportador é necessário para atualização.");
        }

        Optional<FabricanteImportador> fabricanteImportadorOptional = fabricanteImportadorRepository.findById(fabricanteImportadorAtualizado.getId());

        if (fabricanteImportadorOptional.isPresent()) {
            FabricanteImportador fabricanteImportadorExistente = fabricanteImportadorOptional.get();

            fabricanteImportadorExistente.setRazaoSocial(fabricanteImportadorAtualizado.getRazaoSocial());
            fabricanteImportadorExistente.setNomeFantasia(fabricanteImportadorAtualizado.getNomeFantasia());
            fabricanteImportadorExistente.setCnpj(fabricanteImportadorAtualizado.getCnpj());
            fabricanteImportadorExistente.setDataCadastro(fabricanteImportadorAtualizado.getDataCadastro());
            fabricanteImportadorExistente.setEndereco(fabricanteImportadorAtualizado.getEndereco());
            fabricanteImportadorExistente.setCep(fabricanteImportadorAtualizado.getCep());
            fabricanteImportadorExistente.setEmail(fabricanteImportadorAtualizado.getEmail());
            fabricanteImportadorExistente.setTelefone(fabricanteImportadorAtualizado.getTelefone());
            //fabricanteImportadorExistente.setTipoEpiProcessos(fabricanteImportadorAtualizado.getTipoEpiProcessos());
            fabricanteImportadorExistente.setDataRevogacao(fabricanteImportadorAtualizado.getDataRevogacao());
            fabricanteImportadorExistente.setEstado(fabricanteImportadorAtualizado.getEstado());

            return fabricanteImportadorRepository.save(fabricanteImportadorExistente);
        } else {
            throw new EntityNotFoundException("FabricanteImportador não encontrado com o ID: " + fabricanteImportadorAtualizado.getId());
        }
    }

    public void excluirFabricanteImportador(Long id) {
        FabricanteImportador fabricanteImportador = fabricanteImportadorRepository.findById(id).orElse(null);
        if (fabricanteImportador != null) {
            fabricanteImportadorRepository.delete(fabricanteImportador);
        } else {
            throw new RuntimeException("FabricanteImportador não encontrado.");
        }
    }
}
