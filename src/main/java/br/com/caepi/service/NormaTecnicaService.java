package br.com.caepi.service;

import br.com.caepi.entities.NormaTecnica;
import br.com.caepi.repository.NormaTecnicaRepository;
import br.com.caepi.specifications.NormaTecnicaSpecification;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class NormaTecnicaService {
    private final NormaTecnicaRepository normaTecnicaRepository;

    @Autowired
    public NormaTecnicaService(NormaTecnicaRepository normaTecnicaRepository) {
        this.normaTecnicaRepository = normaTecnicaRepository;
    }

    public Set<NormaTecnica> listarTodasNormaTecnica() {
        List<NormaTecnica> normaTecnicaList = normaTecnicaRepository.findAll();
        Comparator<NormaTecnica> idComparator = Comparator.comparing(NormaTecnica::getId);
        return normaTecnicaList.stream()
                .sorted(idComparator)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }
    public Page<NormaTecnica> findAll(Map<String, String> queryParameters
            , Pageable pageable) {
        Specification<br.com.caepi.entities.NormaTecnica> spec = new NormaTecnicaSpecification(queryParameters);
        return normaTecnicaRepository.findAll(spec, pageable);
    }

    public Optional<NormaTecnica> buscarPorId(Long id) {
        return normaTecnicaRepository.findById(id);
    }

    public NormaTecnica salvar(NormaTecnica normaTecnica) {
        String nomeNormalizado = normalizarNome(normaTecnica.getNome());
        normaTecnica.setNomeNormalizado(nomeNormalizado);

        if (verificarNormaTecnicaExistente(nomeNormalizado)) {
            throw new IllegalArgumentException("Já existe uma Norma Técnica com esse nome");
        }

        return normaTecnicaRepository.save(normaTecnica);
    }

    public NormaTecnica atualizar(NormaTecnica normaTecnicaAtualizada) {
        if (normaTecnicaAtualizada.getId() == null) {
            throw new IllegalArgumentException("ID da Norma Técnica não pode ser nulo.");
        }

        Optional<NormaTecnica> normaTecnicaExistenteOpt = normaTecnicaRepository.findById(normaTecnicaAtualizada.getId());
        if (!normaTecnicaExistenteOpt.isPresent()) {
            throw new IllegalArgumentException("Norma Técnica não encontrada.");
        }

        NormaTecnica normaTecnicaExistente = normaTecnicaExistenteOpt.get();
        String nomeNormalizado = normalizarNome(normaTecnicaAtualizada.getNome());

        if (normaTecnicaAtualizada.getNome() != null && !normaTecnicaAtualizada.getNome().equals(normaTecnicaExistente.getNome())) {
            normaTecnicaExistente.setNome(normaTecnicaAtualizada.getNome());
            normaTecnicaExistente.setNomeNormalizado(nomeNormalizado);

            if (verificarNormaTecnicaExistente(nomeNormalizado)) {
                throw new IllegalArgumentException("Já existe uma Norma Técnica com esse nome");
            }
        }

        if (normaTecnicaAtualizada.getDescricao() != null && !normaTecnicaAtualizada.getDescricao().equals(normaTecnicaExistente.getDescricao())) {
            normaTecnicaExistente.setDescricao(normaTecnicaAtualizada.getDescricao());
        }

        return normaTecnicaRepository.save(normaTecnicaAtualizada);
    }

    public boolean verificarNormaTecnicaExistente(String nomeNormalizado) {
        return normaTecnicaRepository.existsByNomeNormalizado(nomeNormalizado);
    }

    private String normalizarNome(String nome) {
        // Manter apenas números e letras e converter para maiúsculas
        String nomeNormalizado = nome.replaceAll("[^a-zA-Z0-9]", "").toUpperCase();

        return nomeNormalizado;
    }

    public void excluirPorId(Long id) {
        normaTecnicaRepository.deleteById(id);
    }

    public void excluirNormaTecnica(Long id) {
        normaTecnicaRepository.deleteById(id);
    }

    public ByteArrayResource reportExcel(Map<String, String> queryParameters, HttpServletRequest request, HttpServletResponse response) throws IOException {

        Specification<NormaTecnica> spec = new NormaTecnicaSpecification(queryParameters);

        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Norma Técnica");
        sheet.setDefaultColumnWidth(30);

        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setColor(IndexedColors.BLACK.getIndex());
        style.setFont(font);

        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Código");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("Nome");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("Descrição");
        header.getCell(2).setCellStyle(style);
        header.createCell(3).setCellValue("Data de Cadastro");
        header.getCell(3).setCellStyle(style);
        header.createCell(4).setCellValue("Data Revogação");
        header.getCell(4).setCellStyle(style);

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        int rowCount = 1;

        for(Map<String, Object> normaTecnica : this.getNormasTecnicas(spec)){
            Row userRow =  sheet.createRow(rowCount++);
            userRow.createCell(0).setCellValue(normaTecnica.get("id").toString());
            userRow.createCell(1).setCellValue(normaTecnica.get("nome").toString());
            userRow.createCell(2).setCellValue(normaTecnica.get("descricao").toString());
            userRow.createCell(3).setCellValue(normaTecnica.get("dataCadastro").toString());
            userRow.createCell(4).setCellValue(normaTecnica.get("dataRevogacao").toString());
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        try {
            workbook.write(outputStream);
            workbook.close();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    System.out.println("Erro ao criar excel" + e);
                }
            }
        }
        return new ByteArrayResource(outputStream.toByteArray());
    }

    private List<Map<String, Object>> getNormasTecnicas(Specification<NormaTecnica> spec) {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        for (NormaTecnica normaTecnica : normaTecnicaRepository.findAll(spec)) {
            Map<String, Object> item = new HashMap<String, Object>();
            item.put("id", normaTecnica.getId());
            item.put("nome", normaTecnica.getNome() != null ? normaTecnica.getNome() : "");
            item.put("descricao", normaTecnica.getDescricao() != null ? normaTecnica.getDescricao() : "");
            item.put("dataCadastro", normaTecnica.getDataCadastro() != null ? dateFormat.format(normaTecnica.getDataCadastro()) : "");
            item.put("dataRevogacao", normaTecnica.getDataRevogacao()!= null ? dateFormat.format(normaTecnica.getDataRevogacao()) : "");
            result.add(item);
        }
        return result;
    }
}