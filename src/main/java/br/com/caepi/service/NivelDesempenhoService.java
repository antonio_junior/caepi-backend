package br.com.caepi.service;

import br.com.caepi.entities.NivelDesempenho;
import br.com.caepi.entities.NivelDesempenhoItem;
import br.com.caepi.entities.NormaTecnica;
import br.com.caepi.repository.NivelDesempenhoItemRepository;
import br.com.caepi.repository.NivelDesempenhoRepository;
import br.com.caepi.utils.IdentifierGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class NivelDesempenhoService {

    @Autowired
    private NivelDesempenhoRepository nivelDesempenhoRepository;

    @Autowired
    private NivelDesempenhoItemRepository nivelDesempenhoItemRepository;

    public NivelDesempenho salvar(NivelDesempenho nivelDesempenho) {
        Integer nivel = levelOfNivelDesempenho(nivelDesempenho);
        nivelDesempenho.setIdentificacao(gerarProximaIdentificacao(nivelDesempenho));
        nivelDesempenho.setNivel(nivel);

        nivelDesempenho.setOrdem(gerarProximaOrdemByNormaTecnica(nivelDesempenho.getNormaTecnica(), nivel));

        NivelDesempenho nivelDesempenhoCriada = this.nivelDesempenhoRepository.save(nivelDesempenho);

        if(nivelDesempenhoCriada.getTipo().equals("Grupo")) {
            NivelDesempenhoItem item = new NivelDesempenhoItem();
            item.setRotulo("Item de Desempenho 1");
            item.setOrdem(1);
            item.setNivelDesempenho(nivelDesempenhoCriada);

            NivelDesempenhoItem nivelDesempenhoItemCriada = nivelDesempenhoItemRepository.save(item);

            nivelDesempenhoCriada.getItems().add(nivelDesempenhoItemCriada);
        }

        if(nivelDesempenhoCriada.getTipo().equals("Tabela Dinâmica")) {
            NivelDesempenhoItem item = new NivelDesempenhoItem();
            item.setRotulo("Coluna 1");
            item.setOrdem(1);
            item.setNivelDesempenho(nivelDesempenhoCriada);

            NivelDesempenhoItem nivelDesempenhoItemCriada = nivelDesempenhoItemRepository.save(item);

            nivelDesempenhoCriada.getItems().add(nivelDesempenhoItemCriada);
        }

        if(nivelDesempenhoCriada.getTipo().equals("Seleção")
            || nivelDesempenhoCriada.getTipo().equals("Escolha Única")
            || nivelDesempenhoCriada.getTipo().equals("Múltipla Escolha")){

            for (int i = 1; i <= 2; i++) {
                NivelDesempenhoItem item = new NivelDesempenhoItem();
                item.setRotulo("Opcão " + i);
                item.setTipo("Opção");
                item.setOrdem(i);
                item.setNivelDesempenho(nivelDesempenhoCriada);

                NivelDesempenhoItem nivelDesempenhoItemCriada = nivelDesempenhoItemRepository.save(item);

                nivelDesempenhoCriada.getItems().add(nivelDesempenhoItemCriada);
            }
        }

        return nivelDesempenhoCriada;
    }

    public NivelDesempenho atualizar(NivelDesempenho nivelDesempenho) {
        return this.nivelDesempenhoRepository.save(nivelDesempenho);
    }

    public Optional<NivelDesempenho> buscarPorId(Long id) {
        return nivelDesempenhoRepository.findById(id);
    }

    public void excluir(Long id) {
        this.nivelDesempenhoRepository.deleteById(id);
    }

    private String gerarProximaIdentificacao(NivelDesempenho nivelDesempenho) {
        NivelDesempenho pai = nivelDesempenho.getNivelDesempenhoPai();
        Optional<String> lastChildIdentificacao;
        String parentIdentifier = null;

        if(pai != null) {
            lastChildIdentificacao = nivelDesempenhoRepository.findLastChildIdentificacao(pai.getId());
            parentIdentifier = pai.getIdentificacao();
        } else {
            lastChildIdentificacao = nivelDesempenhoRepository.findLastIdentificacaoByNormaTecnica(nivelDesempenho.getNormaTecnica().getId(), 1);
        }

        return IdentifierGenerator.incrementIdentifier(parentIdentifier, lastChildIdentificacao.orElse(null));
    }

    private Integer gerarProximaOrdemByNormaTecnica(NormaTecnica normaTecnica, Integer nivel) {
        Integer ordem = 1;
        Integer lastOrdem = nivelDesempenhoRepository.findMaxOrdemNivelDesempenhoByNormaTecnica(normaTecnica, nivel);
        if(lastOrdem != null) {
            return lastOrdem + 1;
        }
        return ordem;
    }

    private Integer levelOfNivelDesempenho(NivelDesempenho nivelDesempenho) {
        if(nivelDesempenho.getNivelDesempenhoPai() == null)
            return 1;

        return nivelDesempenhoRepository.findNivelById(nivelDesempenho.getNivelDesempenhoPai().getId()) + 1;
    }

    public List<NivelDesempenho> importarNiveisDeDesempenho(Long normaTecnicaOrigemId, Long normaTecnicaDestinoId) {
        List<NivelDesempenho> niveisDesempenhoOrigem = nivelDesempenhoRepository.findByNormaTecnicaId(normaTecnicaOrigemId);

        NormaTecnica normaDestino = new NormaTecnica();
        normaDestino.setId(normaTecnicaDestinoId);

        List<NivelDesempenho> niveisDesempenhoImportados = new ArrayList<>();

        List<NivelDesempenho> niveisRaizOrigem = niveisDesempenhoOrigem.stream()
                .filter(n -> n.getNivelDesempenhoPai() == null)
                .collect(Collectors.toList());

        for (NivelDesempenho nivelOrigemRaiz : niveisRaizOrigem) {
            NivelDesempenho novoNivelRaiz = importarNivelRecursivo(nivelOrigemRaiz, normaDestino);
            niveisDesempenhoImportados.add(novoNivelRaiz);
        }

        return niveisDesempenhoImportados;
    }

    /**
     * Faz a importação de um NivelDesempenho de origem para destino,
     * copiando propriedades, itens e chamando recursivamente para subníveis.
     */
    private NivelDesempenho importarNivelRecursivo(NivelDesempenho nivelOrigem, NormaTecnica normaDestino) {
        NivelDesempenho novoNivel = copiarNivelDesempenho(nivelOrigem, normaDestino);

        novoNivel = nivelDesempenhoRepository.save(novoNivel);

        importarItens(nivelOrigem, novoNivel);

        importarSubNiveis(nivelOrigem, novoNivel, normaDestino);

        return novoNivel;
    }

    /**
     * Copia as propriedades do nivelOrigem para um novo objeto NivelDesempenho
     * (sem salvar ainda) ajustando alguns campos (norma, identificação, ordem, etc).
     */
    private NivelDesempenho copiarNivelDesempenho(NivelDesempenho nivelOrigem, NormaTecnica normaDestino) {
        NivelDesempenho novoNivel = new NivelDesempenho();

        novoNivel.setTitulo(nivelOrigem.getTitulo());
        novoNivel.setNormaTecnica(normaDestino);
        novoNivel.setIdentificacao(gerarProximaIdentificacao(novoNivel));
        novoNivel.setNivel(nivelOrigem.getNivel());
        novoNivel.setOrdem(
                gerarProximaOrdemByNormaTecnica(normaDestino, nivelOrigem.getNivel())
        );
        novoNivel.setTipo(nivelOrigem.getTipo());
        novoNivel.setInformacao(nivelOrigem.getInformacao());
        novoNivel.setTextoIlimitado(nivelOrigem.getTextoIlimitado());
        novoNivel.setNumCaracteres(nivelOrigem.getNumCaracteres());
        novoNivel.setCasasDecimais(nivelOrigem.getCasasDecimais());
        novoNivel.setIlimitado(nivelOrigem.getIlimitado());
        novoNivel.setVlrmin(nivelOrigem.getVlrmin());
        novoNivel.setVlrmax(nivelOrigem.getVlrmax());
        novoNivel.setFormula(nivelOrigem.getFormula());
        novoNivel.setFormulaRespostaPai(nivelOrigem.getFormulaRespostaPai());

        return novoNivel;
    }

    /**
     * Copia os itens de nivelOrigem para novoNivel.
     */
    private void importarItens(NivelDesempenho nivelOrigem, NivelDesempenho novoNivel) {
        Set<NivelDesempenhoItem> itensOrigem = nivelOrigem.getItems();

        for (NivelDesempenhoItem itemOrigem : itensOrigem) {
            NivelDesempenhoItem novoItem = new NivelDesempenhoItem();
            novoItem.setRotulo(itemOrigem.getRotulo());
            novoItem.setTipo(itemOrigem.getTipo());
            novoItem.setOrdem(itemOrigem.getOrdem());
            novoItem.setOpcoes(itemOrigem.getOpcoes());
            novoItem.setInformacao(itemOrigem.getInformacao());
            novoItem.setTextoIlimitado(itemOrigem.getTextoIlimitado());
            novoItem.setNumCaracteres(itemOrigem.getNumCaracteres());
            novoItem.setCasasDecimais(itemOrigem.getCasasDecimais());
            novoItem.setIlimitado(itemOrigem.getIlimitado());
            novoItem.setVlrmin(itemOrigem.getVlrmin());
            novoItem.setVlrmax(itemOrigem.getVlrmax());

            novoItem.setNivelDesempenho(novoNivel);

            nivelDesempenhoItemRepository.save(novoItem);

            novoNivel.getItems().add(novoItem);
        }
    }

    /**
     * Percorre os subníveis (nivelOrigem.getNivelDesempenhoSubsequentes()) e os importa.
     */
    private void importarSubNiveis(
            NivelDesempenho nivelOrigem,
            NivelDesempenho novoNivel,
            NormaTecnica normaDestino
    ) {
        List<NivelDesempenho> subniveisOrigem = nivelOrigem.getNivelDesempenhoSubsequentes();
        if (subniveisOrigem != null) {
            for (NivelDesempenho subNivelOrigem : subniveisOrigem) {
                NivelDesempenho novoSubNivel = copiarNivelDesempenho(subNivelOrigem, normaDestino);

                novoSubNivel.setNivelDesempenhoPai(novoNivel);

                novoSubNivel = nivelDesempenhoRepository.save(novoSubNivel);

                importarItens(subNivelOrigem, novoSubNivel);

                importarSubNiveis(subNivelOrigem, novoSubNivel, normaDestino);

                novoNivel.getNivelDesempenhoSubsequentes().add(novoSubNivel);
            }
        }
    }
}
