package br.com.caepi.service;

import br.com.caepi.entities.Especificidade;
import br.com.caepi.entities.EspecificidadeOpcao;
import br.com.caepi.entities.TipoEpi;
import br.com.caepi.repository.EspecificidadeRepository;
import br.com.caepi.repository.TipoEpiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EspecificidadeService {

    @Autowired
    private EspecificidadeRepository especificidadeRepository;

    @Autowired
    private TipoEpiRepository tipoEpiRepository;


    public Especificidade salvar(Especificidade especificidade) {

        TipoEpi tipoEpi = tipoEpiRepository.findById(especificidade.getTipoEpi().getId()).get();

        especificidade.setTipoEpi(tipoEpi);

        Integer ultimaOrdem = this.pegarUltimaOrdem(especificidade.getTipoEpi().getId());
        especificidade.setOrdem(ultimaOrdem == null ? 1 : ultimaOrdem + 1);

        Especificidade especificidadeCriada = this.especificidadeRepository.save(especificidade);

        if (especificidade.getTipo().equals("Texto")
                || especificidade.getTipo().equals("Área de Texto")
                || especificidade.getTipo().equals("Relação de Arquivos")) {
            return especificidadeCriada;
        }

        EspecificidadeOpcao opcao1 = new EspecificidadeOpcao();
        opcao1.setEspecificidade(especificidadeCriada);
        opcao1.setDescricao("Opção 1");
        opcao1.setOrdem(1);
        especificidadeCriada.getOpcoes().add(opcao1);

        EspecificidadeOpcao opcao2 = new EspecificidadeOpcao();
        opcao2.setEspecificidade(especificidadeCriada);
        opcao2.setDescricao("Opção 2");
        opcao2.setOrdem(2);
        especificidadeCriada.getOpcoes().add(opcao2);

        return this.especificidadeRepository.save(especificidadeCriada);
    }

    public Especificidade atualizar(Especificidade especificidade) {
        return this.especificidadeRepository.save(especificidade);
    }

    public Optional<Especificidade> buscarPorId(Long id) {
        return especificidadeRepository.findById(id);
    }

    public void excluir(Long id) {
        this.especificidadeRepository.deleteById(id);
    }

    private Integer pegarUltimaOrdem(Long idTipoEpi) {
        return this.especificidadeRepository.pegarUltimaOrdem(idTipoEpi);
    }
}
