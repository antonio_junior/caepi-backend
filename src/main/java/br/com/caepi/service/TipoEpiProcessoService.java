package br.com.caepi.service;

import br.com.caepi.entities.LaboratorioOCP;
import br.com.caepi.entities.TipoEpiProcesso;
import br.com.caepi.repository.LaboratorioOCPRepository;
import br.com.caepi.repository.TipoEpiProcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TipoEpiProcessoService {

    @Autowired
    private TipoEpiProcessoRepository tipoEpiProcessoRepository;

    @Autowired
    private LaboratorioOCPRepository laboratorioOCPRepository;

    public TipoEpiProcesso addLaboratorio(Long tipoEpiProcessoId, Long laboratorioId) {
        Optional<TipoEpiProcesso> tipoEpiProcessoOpt = tipoEpiProcessoRepository.findById(tipoEpiProcessoId);
        Optional<LaboratorioOCP> laboratorioOpt = laboratorioOCPRepository.findById(laboratorioId);

        if (tipoEpiProcessoOpt.isPresent() && laboratorioOpt.isPresent()) {
            TipoEpiProcesso tipoEpiProcesso = tipoEpiProcessoOpt.get();
            LaboratorioOCP laboratorio = laboratorioOpt.get();
            tipoEpiProcesso.addLaboratorio(laboratorio);
            return tipoEpiProcessoRepository.save(tipoEpiProcesso);
        }

        return null;
    }

    public TipoEpiProcesso removeLaboratorio(Long tipoEpiProcessoId, Long laboratorioId) {
        Optional<TipoEpiProcesso> tipoEpiProcessoOpt = tipoEpiProcessoRepository.findById(tipoEpiProcessoId);
        Optional<LaboratorioOCP> laboratorioOpt = laboratorioOCPRepository.findById(laboratorioId);

        if (tipoEpiProcessoOpt.isPresent() && laboratorioOpt.isPresent()) {
            TipoEpiProcesso tipoEpiProcesso = tipoEpiProcessoOpt.get();
            LaboratorioOCP laboratorio = laboratorioOpt.get();
            tipoEpiProcesso.removeLaboratorio(laboratorio);
            return tipoEpiProcessoRepository.save(tipoEpiProcesso);
        }

        return null;
    }
}