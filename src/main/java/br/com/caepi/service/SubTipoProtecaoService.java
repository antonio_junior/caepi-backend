package br.com.caepi.service;


import br.com.caepi.DTOS.SubTipoProtecaoDTO;
import br.com.caepi.DTOS.TipoProtecaoDTO;
import br.com.caepi.entities.NormaTecnica;
import br.com.caepi.entities.SubTipoProtecao;
import br.com.caepi.entities.TipoProtecao;
import br.com.caepi.repository.SubTipoProtecaoRepository;
import br.com.caepi.specifications.NormaTecnicaSpecification;
import br.com.caepi.specifications.SubTipoProtecaoSpecification;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class SubTipoProtecaoService {
    private final SubTipoProtecaoRepository subTipoProtecaoRepository;

    @Autowired
    public SubTipoProtecaoService(SubTipoProtecaoRepository subTipoProtecaoRepository) {
        this.subTipoProtecaoRepository = subTipoProtecaoRepository;
    }

    public Set<SubTipoProtecao> listarTodosTipoProtecao() {
        List<SubTipoProtecao> subTipoProtecoes = subTipoProtecaoRepository.findAllByOrderByTipoProtecao_NomeAsc();
        return new HashSet<>(subTipoProtecoes);
    }

    public Page<SubTipoProtecao> findAll(Map<String, String> queryParameters
            , Pageable pageable) {
        Specification<SubTipoProtecao> spec = new SubTipoProtecaoSpecification(queryParameters);

        return subTipoProtecaoRepository.findAll(spec, pageable);
    }

    public Page<SubTipoProtecaoDTO> findAllDTO(Map<String, String> queryParameters
            , Pageable pageable) {
        Specification<SubTipoProtecao> spec = new SubTipoProtecaoSpecification(queryParameters);

        return subTipoProtecaoRepository.findAll(spec, pageable).map(this::convertToDTO);
    }

    private SubTipoProtecaoDTO convertToDTO(SubTipoProtecao subTipoProtecao) {
        SubTipoProtecaoDTO subTipoProtecaoDTO = new SubTipoProtecaoDTO();
        subTipoProtecaoDTO.setId(subTipoProtecao.getId());
        subTipoProtecaoDTO.setNome(subTipoProtecao.getNome());
        subTipoProtecaoDTO.setDescricao(subTipoProtecao.getDescricao());
        subTipoProtecaoDTO.setDataCadastro(subTipoProtecao.getDataCadastro());
        subTipoProtecaoDTO.setStatus(subTipoProtecao.getStatus());
        subTipoProtecaoDTO.setTipo("subTipoProtecao");

        TipoProtecaoDTO tipoProtecaoDTO = new TipoProtecaoDTO();
        tipoProtecaoDTO.setId(subTipoProtecao.getTipoProtecao().getId());
        tipoProtecaoDTO.setNome(subTipoProtecao.getTipoProtecao().getNome());
        tipoProtecaoDTO.setDescricao(subTipoProtecao.getTipoProtecao().getDescricao());
        tipoProtecaoDTO.setDataCadastro(subTipoProtecao.getTipoProtecao().getDataCadastro());
        tipoProtecaoDTO.setStatus(subTipoProtecao.getTipoProtecao().getStatus());
        tipoProtecaoDTO.setTipo("tipoProtecao");

        subTipoProtecaoDTO.setTipoProtecao(tipoProtecaoDTO);
        return subTipoProtecaoDTO;
    }

    public Optional<SubTipoProtecao> buscarPorId(Long id) {
        return subTipoProtecaoRepository.findById(id);
    }

    public SubTipoProtecao salvar(SubTipoProtecao subTipoProtecao) {
        return subTipoProtecaoRepository.save(subTipoProtecao);
    }

    public SubTipoProtecao atualizar(SubTipoProtecao subTipoProtecao) {
        return subTipoProtecaoRepository.save(subTipoProtecao);
    }

    public void excluirTipoProtecao(Long id) {
        subTipoProtecaoRepository.deleteById(id);
    }

    public SubTipoProtecao vincularNormaTecnica(Long subTipoProtecaoId, Set<NormaTecnica> normasTecnicas) {
        SubTipoProtecao subTipoProtecao = subTipoProtecaoRepository.findById(subTipoProtecaoId)
                .orElseThrow(() -> new RuntimeException("SubTipoProtecao não encontrado com o ID: " + subTipoProtecaoId));

        subTipoProtecao.setNormasTecnicas(normasTecnicas);

        return subTipoProtecaoRepository.save(subTipoProtecao);
    }

    public ByteArrayResource reportExcel(Map<String, String> queryParameters, HttpServletRequest request, HttpServletResponse response) throws IOException {

        Specification<SubTipoProtecao> spec = new SubTipoProtecaoSpecification(queryParameters);

        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("SubTipo de Proteção");
        sheet.setDefaultColumnWidth(30);

        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setColor(IndexedColors.BLACK.getIndex());
        style.setFont(font);

        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Código");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("Nome");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("Tipo Proteção");
        header.getCell(2).setCellStyle(style);
        header.createCell(3).setCellValue("Data de Cadastro");
        header.getCell(3).setCellStyle(style);

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        int rowCount = 1;

        for(Map<String, Object> subTipoProtecao : this.getSubTiposProtecao(spec)){
            Row userRow =  sheet.createRow(rowCount++);
            userRow.createCell(0).setCellValue(subTipoProtecao.get("id").toString());
            userRow.createCell(1).setCellValue(subTipoProtecao.get("nome").toString());
            userRow.createCell(2).setCellValue(subTipoProtecao.get("tipoProtecao").toString());
            userRow.createCell(3).setCellValue(subTipoProtecao.get("dataCadastro").toString());
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        try {
            workbook.write(outputStream);
            workbook.close();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    System.out.println("Erro ao criar excel" + e);
                }
            }
        }
        return new ByteArrayResource(outputStream.toByteArray());
    }

    private List<Map<String, Object>> getSubTiposProtecao(Specification<SubTipoProtecao> spec) {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        for (SubTipoProtecao subTipoProtecao : subTipoProtecaoRepository.findAll(spec)) {
            Map<String, Object> item = new HashMap<String, Object>();
            item.put("id", subTipoProtecao.getId());
            item.put("nome", subTipoProtecao.getNome() != null ? subTipoProtecao.getNome() : "");
            item.put("tipoProtecao", subTipoProtecao.getTipoProtecao() != null ? subTipoProtecao.getTipoProtecao().getNome() : "");
            item.put("dataCadastro", subTipoProtecao.getDataCadastro() != null ? dateFormat.format(subTipoProtecao.getDataCadastro()) : "");
            result.add(item);
        }
        return result;
    }
}
