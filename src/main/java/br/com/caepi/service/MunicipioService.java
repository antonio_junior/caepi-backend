package br.com.caepi.service;

import br.com.caepi.entities.Municipio;
import br.com.caepi.repository.MunicipioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "MunicipioService")
public class MunicipioService {
	
	@Autowired
	private MunicipioRepository municipioRepository;

	public List<Municipio> findByEstado(Long estadoId) {
		return municipioRepository.findByEstadoId(estadoId);
	}
}
