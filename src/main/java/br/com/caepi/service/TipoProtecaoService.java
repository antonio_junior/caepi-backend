package br.com.caepi.service;

import br.com.caepi.DTOS.SubTipoProtecaoDTO;
import br.com.caepi.DTOS.TipoProtecaoDTO;
import br.com.caepi.entities.NormaTecnica;
import br.com.caepi.entities.TipoProtecao;
import br.com.caepi.repository.TipoProtecaoRepository;
import br.com.caepi.repository.SubTipoProtecaoRepository;
import br.com.caepi.specifications.TipoProtecaoSpecification;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class TipoProtecaoService {
    private final TipoProtecaoRepository tipoProtecaoRepository;
    private final SubTipoProtecaoRepository subTipoProtecaoRepository;

    @Autowired
    public TipoProtecaoService(TipoProtecaoRepository tipoProtecaoRepository, SubTipoProtecaoRepository subTipoProtecaoRepository) {
        this.tipoProtecaoRepository = tipoProtecaoRepository;
        this.subTipoProtecaoRepository = subTipoProtecaoRepository;
    }

    public Set<TipoProtecao> listarTodosGrupoTipoProtecao() {
        List<TipoProtecao> tipoProtecoes = tipoProtecaoRepository.findAll();
        return new HashSet<>(tipoProtecoes);
    }

    public Page<TipoProtecaoDTO> findAllDTO(Map<String, String> queryParameters
            , Pageable pageable) {
        Specification<TipoProtecao> spec = new TipoProtecaoSpecification(queryParameters);
        return tipoProtecaoRepository.findAll(spec, pageable).map(this::convertToDTO);
    }

    public TipoProtecaoDTO convertToDTO(TipoProtecao tipoProtecao) {
        TipoProtecaoDTO tipoProtecaoDTO = new TipoProtecaoDTO();
        tipoProtecaoDTO.setId(tipoProtecao.getId());
        tipoProtecaoDTO.setNome(tipoProtecao.getNome());
        tipoProtecaoDTO.setDescricao(tipoProtecao.getDescricao());
        tipoProtecaoDTO.setDataCadastro(tipoProtecao.getDataCadastro());
        tipoProtecaoDTO.setStatus(tipoProtecao.getStatus());
        tipoProtecaoDTO.setTipo("tipoProtecao");
        Set<SubTipoProtecaoDTO> subTipoProtecaoDTOS = new HashSet<>();
        tipoProtecao.getSubTipoProtecoes().forEach(subTipoProtecao -> {
            SubTipoProtecaoDTO subTipoProtecaoDTO = new SubTipoProtecaoDTO();
            subTipoProtecaoDTO.setId(subTipoProtecao.getId());
            subTipoProtecaoDTO.setNome(subTipoProtecao.getNome());
            subTipoProtecaoDTO.setDescricao(subTipoProtecao.getDescricao());
            subTipoProtecaoDTO.setDataCadastro(subTipoProtecao.getDataCadastro());
            subTipoProtecaoDTO.setStatus(subTipoProtecao.getStatus());
            subTipoProtecaoDTO.setTipo("subTipoProtecao");

            subTipoProtecaoDTOS.add(subTipoProtecaoDTO);
        });

        tipoProtecaoDTO.setSubTipoProtecoes(subTipoProtecaoDTOS);

        return tipoProtecaoDTO;
    }

    public Page<TipoProtecao> findAll(Map<String, String> queryParameters
            , Pageable pageable) {
        Specification<TipoProtecao> spec = new TipoProtecaoSpecification(queryParameters);
        return tipoProtecaoRepository.findAll(spec, pageable);
    }

    public Optional<TipoProtecao> buscarPorId(Long id) {
        return tipoProtecaoRepository.findById(id);
    }

    public TipoProtecao salvar(TipoProtecao tipoProtecao) {
        return tipoProtecaoRepository.save(tipoProtecao);
    }

    public TipoProtecao atualizar(TipoProtecao tipoProtecao) {
        return tipoProtecaoRepository.save(tipoProtecao);
    }

    public ResponseEntity<String> excluirGrupoTipoProtecao(Long id) {
        try {
            tipoProtecaoRepository.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (DataIntegrityViolationException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Erro: O objeto não pode ser excluído porque está sendo utilizado.");
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    public TipoProtecao vincularNormaTecnica(Long tipoProtecaoId, Set<NormaTecnica> normasTecnicas) {
        TipoProtecao tipoProtecao = tipoProtecaoRepository.findById(tipoProtecaoId)
                .orElseThrow(() -> new RuntimeException("TipoProtecao não encontrado com o ID: " + tipoProtecaoId));

        tipoProtecao.setNormasTecnicas(normasTecnicas);

        return tipoProtecaoRepository.save(tipoProtecao);
    }

    public ByteArrayResource reportExcel(Map<String, String> queryParameters, HttpServletRequest request, HttpServletResponse response) throws IOException {

        Specification<TipoProtecao> spec = new TipoProtecaoSpecification(queryParameters);

        Workbook workbook = new XSSFWorkbook();

        // create excel xls sheet
        Sheet sheet = workbook.createSheet("Tipo Proteção");
        sheet.setDefaultColumnWidth(30);

        // create style for header cells
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setColor(IndexedColors.BLACK.getIndex());
        style.setFont(font);

        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Código");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("Nome");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("Data de Cadastro");
        header.getCell(2).setCellStyle(style);

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        int rowCount = 1;

        for(Map<String, Object> tipoProtecao : this.getTiposProtecao(spec)){
            Row userRow =  sheet.createRow(rowCount++);
            userRow.createCell(0).setCellValue(tipoProtecao.get("id").toString());
            userRow.createCell(1).setCellValue(tipoProtecao.get("nome").toString());
            userRow.createCell(2).setCellValue(tipoProtecao.get("dataCadastro").toString());
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        try {
            workbook.write(outputStream);
            workbook.close();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    System.out.println("Erro ao criar excel" + e);
                }
            }
        }
        return new ByteArrayResource(outputStream.toByteArray());
    }

    private List<Map<String, Object>> getTiposProtecao(Specification<TipoProtecao> spec) {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        for (TipoProtecao tipoProtecao : tipoProtecaoRepository.findAll(spec)) {
            Map<String, Object> item = new HashMap<String, Object>();
            item.put("id", tipoProtecao.getId());
            item.put("nome", tipoProtecao.getNome() != null ? tipoProtecao.getNome() : "");
            item.put("dataCadastro", tipoProtecao.getDataCadastro() != null ? dateFormat.format(tipoProtecao.getDataCadastro()) : "");
            result.add(item);
        }
        return result;
    }
}


