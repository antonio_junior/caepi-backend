package br.com.caepi.utils;

public class IdentifierGenerator {

    public static String incrementIdentifier(String parentIdentifier, String lastChildIdentifier) {
        if (lastChildIdentifier == null) {
            return (parentIdentifier == null ? "" : parentIdentifier) + "A";
        }

        String identifierToIncrement = lastChildIdentifier;

        while (true) {
            int length = identifierToIncrement.length();
            char lastChar = identifierToIncrement.charAt(length - 1);

            if (lastChar < 'Z') {
                return identifierToIncrement.substring(0, length - 1) + (char) (lastChar + 1);
            }

            int notZIndex = length - 1;
            while (notZIndex >= 0 && identifierToIncrement.charAt(notZIndex) == 'Z') {
                notZIndex--;
            }

            if (notZIndex < 0) {
                return identifierToIncrement + 'A';
            }

            StringBuilder sb = new StringBuilder(identifierToIncrement.substring(0, notZIndex));
            sb.append((char) (identifierToIncrement.charAt(notZIndex) + 1));
            for (int i = notZIndex + 1; i < length; i++) {
                sb.append('A');
            }

            identifierToIncrement = sb.toString();
        }
    }
}





