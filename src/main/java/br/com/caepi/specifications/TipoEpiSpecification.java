package br.com.caepi.specifications;

import br.com.caepi.entities.TipoEpi;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TipoEpiSpecification implements Specification<TipoEpi> {

    private Map<String, String> queryParameters;
    List<Predicate> predicates = new ArrayList<>();

    public TipoEpiSpecification(Map<String, String> queryParameters) {
        super();
        this.queryParameters = queryParameters;
    }

    @Override
    public Predicate toPredicate(Root<TipoEpi> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

        if (!queryParameters.isEmpty()) {
            if (queryParameters.containsKey("titulo") && !queryParameters.get("titulo").isEmpty()) {
                predicates.add(builder.like(builder.lower(root.get("titulo")), "%" + queryParameters.get("titulo").toLowerCase() + "%"));
            }

            if (queryParameters.containsKey("descricao") && !queryParameters.get("descricao").isEmpty()) {
                predicates.add(builder.like(builder.lower(root.get("descricao")), "%" + queryParameters.get("descricao").toLowerCase() + "%"));
            }

            if (queryParameters.containsKey("dataInicioCadastro") && !queryParameters.get("dataInicioCadastro").isEmpty()) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    predicates.add(builder.greaterThanOrEqualTo(root.get("dataCadastro"), sdf.parse(queryParameters.get("dataInicioCadastro"))));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (queryParameters.containsKey("dataFimCadastro") && !queryParameters.get("dataFimCadastro").isEmpty()) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date dataFim = sdf.parse(queryParameters.get("dataFimCadastro"));

                    // Adicionar 1 dia à data fim
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(dataFim);
                    calendar.add(Calendar.DATE, 1);
                    dataFim = calendar.getTime();

                    predicates.add(builder.lessThanOrEqualTo(root.get("dataCadastro"), dataFim));

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (queryParameters.containsKey("dataInicioRevogacao") && !queryParameters.get("dataInicioRevogacao").isEmpty()) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    predicates.add(builder.greaterThanOrEqualTo(root.get("dataRevogacao"), sdf.parse(queryParameters.get("dataInicioRevogacao"))));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (queryParameters.containsKey("dataFimRevogacao") && !queryParameters.get("dataFimRevogacao").isEmpty()) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date dataFim = sdf.parse(queryParameters.get("dataFimRevogacao"));

                    // Adicionar 1 dia à data fim
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(dataFim);
                    calendar.add(Calendar.DATE, 1);
                    dataFim = calendar.getTime();

                    predicates.add(builder.lessThanOrEqualTo(root.get("dataRevogacao"), dataFim));

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (queryParameters.containsKey("certificadoDeConformidade") && !queryParameters.get("certificadoDeConformidade").isEmpty()) {
                boolean certificado = Boolean.parseBoolean(queryParameters.get("certificadoDeConformidade"));
                predicates.add(builder.equal(root.get("certificadoDeConformidade"), certificado));
            }

            if (queryParameters.containsKey("conjugado") && !queryParameters.get("conjugado").isEmpty()) {
                boolean conjugado = Boolean.parseBoolean(queryParameters.get("conjugado"));
                predicates.add(builder.equal(root.get("conjugado"), conjugado));
            }

            if (queryParameters.containsKey("ensaioNoExterior") && !queryParameters.get("ensaioNoExterior").isEmpty()) {
                boolean ensaioNoExterior = Boolean.parseBoolean(queryParameters.get("ensaioNoExterior"));
                predicates.add(builder.equal(root.get("ensaioNoExterior"), ensaioNoExterior));
            }
        }

        if (!predicates.isEmpty()) {
            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        }
        return null;
    }
}