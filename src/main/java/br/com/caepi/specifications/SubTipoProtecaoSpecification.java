package br.com.caepi.specifications;

import br.com.caepi.entities.SubTipoProtecao;
import br.com.caepi.entities.TipoProtecao;
import br.com.caepi.enums.Status;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class SubTipoProtecaoSpecification implements Specification<SubTipoProtecao> {

    private Map<String, String> queryParameters;
    List<Predicate> predicates = new ArrayList<>();

    public SubTipoProtecaoSpecification(Map<String, String> queryParameters) {
        super();
        this.queryParameters = queryParameters;
    }

    @Override
    public Predicate toPredicate(Root<SubTipoProtecao> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

        if (!queryParameters.isEmpty()) {
            if (queryParameters.containsKey("nome") && !queryParameters.get("nome").isEmpty()) {
                predicates.add(builder.like(builder.lower(root.get("nome")), "%" + queryParameters.get("nome").toLowerCase() + "%"));
            }

            if (queryParameters.containsKey("tipoProtecao") && !queryParameters.get("tipoProtecao").isEmpty()) {
                String nomeTipoProtecao = queryParameters.get("tipoProtecao").toLowerCase();

                Join<SubTipoProtecao, TipoProtecao> tipoProtecaoJoin = root.join("tipoProtecao", JoinType.INNER);

                predicates.add(builder.like(
                        builder.function("unaccent", String.class, builder.lower(tipoProtecaoJoin.get("nome"))),
                        "%" + stripAccents(nomeTipoProtecao) + "%"
                ));
            }

            if (queryParameters.containsKey("descricao") && !queryParameters.get("descricao").isEmpty()) {
                predicates.add(builder.like(builder.lower(root.get("descricao")), "%" + queryParameters.get("descricao").toLowerCase() + "%"));
            }

            if (queryParameters.containsKey("dataInicioCadastro") && !queryParameters.get("dataInicioCadastro").isEmpty()) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    predicates.add(builder.greaterThanOrEqualTo(root.get("dataCadastro"), sdf.parse(queryParameters.get("dataInicioCadastro"))));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (queryParameters.containsKey("dataFimCadastro") && !queryParameters.get("dataFimCadastro").isEmpty()) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date dataFim = sdf.parse(queryParameters.get("dataFimCadastro"));

                    // Adicionar 1 dia à data fim
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(dataFim);
                    calendar.add(Calendar.DATE, 1);
                    dataFim = calendar.getTime();

                    predicates.add(builder.lessThanOrEqualTo(root.get("dataCadastro"), dataFim));

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (queryParameters.containsKey("status") && !queryParameters.get("status").isEmpty()) {
                predicates.add(builder.equal(root.get("status"), Status.valueOf(queryParameters.get("status"))));
            }
        }

        if (!predicates.isEmpty()) {
            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        }
        return null;
    }

    private static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s;
    }
}