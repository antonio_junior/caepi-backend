package br.com.caepi.specifications;

import br.com.caepi.entities.NormaTecnica;
import br.com.caepi.entities.Processo;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProcessoSpecification implements Specification<Processo> {

    private Map<String, String> queryParameters;
    List<Predicate> predicates = new ArrayList<>();

    public ProcessoSpecification(Map<String, String> queryParameters) {
        super();
        this.queryParameters = queryParameters;
    }

    @Override
    public Predicate toPredicate(Root<Processo> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

            if (!queryParameters.isEmpty()) {
                if (queryParameters.containsKey("numeroProcesso") && !queryParameters.get("numeroProcesso").isEmpty()) {
                    predicates.add(builder.like(builder.lower(root.get("numeroProcesso")), "%" + queryParameters.get("numeroProcesso").toLowerCase() + "%"));
                }

                if (queryParameters.containsKey("etapa") && !queryParameters.get("etapa").isEmpty()) {
                    predicates.add(builder.equal(root.get("etapa"), queryParameters.get("etapa")));
                }

                if (queryParameters.containsKey("descricao") && !queryParameters.get("descricao").isEmpty()) {
                    predicates.add(builder.like(builder.lower(root.get("descricao")), "%" + queryParameters.get("descricao").toLowerCase() + "%"));
                }

                if (queryParameters.containsKey("dataCadastro") && !queryParameters.get("dataCadastro").isEmpty()) {
                    predicates.add(builder.equal(root.get("dataCadastro"), queryParameters.get("dataCadastro")));
                }

                if (queryParameters.containsKey("status") && !queryParameters.get("status").isEmpty()) {
                    predicates.add(builder.equal(root.get("status"), queryParameters.get("status")));
                }
            }

            if (!predicates.isEmpty()) {
                return builder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
            return null;
        }

    }
