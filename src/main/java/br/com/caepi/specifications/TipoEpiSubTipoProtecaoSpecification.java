package br.com.caepi.specifications;

import br.com.caepi.entities.TipoEpiProtecaoBase;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TipoEpiSubTipoProtecaoSpecification implements Specification<TipoEpiProtecaoBase> {

    private Map<String, String> queryParameters;
    List<Predicate> predicates = new ArrayList<>();

    public TipoEpiSubTipoProtecaoSpecification(Map<String, String> queryParameters) {
        super();
        this.queryParameters = queryParameters;
    }

    @Override
    public Predicate toPredicate(Root<TipoEpiProtecaoBase> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

        if (!queryParameters.isEmpty()) {
            if (queryParameters.containsKey("tipoEpi") && !queryParameters.get("tipoEpi").isEmpty()) {
                predicates.add(builder.equal(root.get("tipoEpi").get("id"), queryParameters.get("tipoEpi")));
            }
        }

        if (!predicates.isEmpty()) {
            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        }
        return null;
    }
}
