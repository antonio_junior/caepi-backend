package br.com.caepi.specifications;

import br.com.caepi.entities.NormaTecnica;
import br.com.caepi.enums.Status;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class NormaTecnicaSpecification implements Specification<NormaTecnica> {

    private Map<String, String> queryParameters;
    List<Predicate> predicates = new ArrayList<>();

    public NormaTecnicaSpecification(Map<String, String> queryParameters) {
        super();
        this.queryParameters = queryParameters;
    }

    @Override
    public Predicate toPredicate(Root<NormaTecnica> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

        if (!queryParameters.isEmpty()) {
            if (queryParameters.containsKey("nome") && !queryParameters.get("nome").isEmpty()) {
                predicates.add(builder.like(builder.lower(root.get("nome")), "%" + queryParameters.get("nome").toLowerCase() + "%"));
            }

            if (queryParameters.containsKey("descricao") && !queryParameters.get("descricao").isEmpty()) {
                predicates.add(builder.like(builder.lower(root.get("descricao")), "%" + queryParameters.get("descricao").toLowerCase() + "%"));
            }

            if (queryParameters.containsKey("dataInicioCadastro") && !queryParameters.get("dataInicioCadastro").isEmpty()) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    predicates.add(builder.greaterThanOrEqualTo(root.get("dataCadastro"), sdf.parse(queryParameters.get("dataInicioCadastro"))));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (queryParameters.containsKey("dataFimCadastro") && !queryParameters.get("dataFimCadastro").isEmpty()) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date dataFim = sdf.parse(queryParameters.get("dataFimCadastro"));

                    // Adicionar 1 dia à data fim
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(dataFim);
                    calendar.add(Calendar.DATE, 1);
                    dataFim = calendar.getTime();

                    predicates.add(builder.lessThanOrEqualTo(root.get("dataCadastro"), dataFim));

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (queryParameters.containsKey("dataInicioRevogacao") && !queryParameters.get("dataInicioRevogacao").isEmpty()) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    predicates.add(builder.greaterThanOrEqualTo(root.get("dataRevogacao"), sdf.parse(queryParameters.get("dataInicioRevogacao"))));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (queryParameters.containsKey("dataFimRevogacao") && !queryParameters.get("dataFimRevogacao").isEmpty()) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date dataFim = sdf.parse(queryParameters.get("dataFimRevogacao"));

                    // Adicionar 1 dia à data fim
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(dataFim);
                    calendar.add(Calendar.DATE, 1);
                    dataFim = calendar.getTime();

                    predicates.add(builder.lessThanOrEqualTo(root.get("dataRevogacao"), dataFim));

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (queryParameters.containsKey("status") && !queryParameters.get("status").isEmpty()) {
                predicates.add(builder.equal(root.get("status"), Status.valueOf(queryParameters.get("status"))));
            }
        }

        if (!predicates.isEmpty()) {
            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        }
        return null;
    }
}