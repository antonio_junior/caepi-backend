package br.com.caepi.DTOS;

import br.com.caepi.entities.Estado;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class LaboratorioOCPDTO {

    private Long id;
    private String razaoSocial;
    private String nomeFantasia;
    private String cnpj;
    private String endereco;
    private String cep;
    private Estado estado;
    private String municipio;
    private String email;
    private String telefone;
    private Date dataCadastro;
    private Date dataRevogacao;
    private String status;
    private String tipo;
    private List<TipoEpiDTO> tiposEpi;
    private boolean ativo;
}
