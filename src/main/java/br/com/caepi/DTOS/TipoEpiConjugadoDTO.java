package br.com.caepi.DTOS;

import br.com.caepi.entities.TipoEpi;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
public class TipoEpiConjugadoDTO {
    private Long id;
    private String titulo;
    private String descricao;
    private String textoProtecao;
    private Date dataRevogacao;
    private Date dataCadastro;
    private boolean certificadoDeConformidade;
    private boolean ensaioNoExterior;
    private String indicacaoDeUso;
    private boolean conjugado;
    private Set<TipoEpi> conjugaveis;
}
