package br.com.caepi.DTOS;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IniciarEspecificidadesGeraisEpi {

    private Long processoId;
    private Long tipoEpiProcessoId;
    private Long laboratorioId;
}
