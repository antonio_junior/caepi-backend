package br.com.caepi.DTOS;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IniciarProtecaoEpi {
    private Long processoId;
    private Long tipoEpiProcessoId;
    private Long protecaoId;
    private Long laboratorioId;
}
