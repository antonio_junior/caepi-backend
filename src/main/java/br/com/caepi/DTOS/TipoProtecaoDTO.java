package br.com.caepi.DTOS;

import br.com.caepi.entities.SubTipoProtecao;
import br.com.caepi.enums.Status;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class TipoProtecaoDTO {
    protected Long id;
    protected String nome;
    protected String descricao;
    protected Date dataCadastro;
    protected Status status;
    protected String tipo;
    private Set<SubTipoProtecaoDTO> subTipoProtecoes = new HashSet<>();

}
