package br.com.caepi.DTOS;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AvancarEtapaLaboratorioOcpDTO {

    private Long laboratorioOcpId;
    private Long processoId;
}
