package br.com.caepi.DTOS;

import br.com.caepi.entities.NormaTecnica;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class VincularNormasTecnicasRequest {
    private Long entityId;
    private Set<NormaTecnica> normasTecnicas;
}
