package br.com.caepi.DTOS;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class ErrorResponse {
    private LocalDateTime timestamp;
    private int status;
    private String error;
    private String message;
    private String path;


    public ErrorResponse(LocalDateTime now, int value, String error, String message, String path) {
        this.timestamp = now;
        this.status = value;
        this.error = error;
        this.message = message;
        this.path = path;
    }
}

