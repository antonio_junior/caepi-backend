package br.com.caepi.DTOS;

import br.com.caepi.enums.Status;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
public class SubTipoProtecaoDTO {
    protected Long id;
    protected String nome;
    protected String descricao;
    protected Date dataCadastro;
    protected Status status;
    protected String tipo;
    protected TipoProtecaoDTO tipoProtecao;
}
