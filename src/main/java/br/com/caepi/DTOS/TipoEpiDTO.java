package br.com.caepi.DTOS;

import br.com.caepi.entities.Especificidade;
import br.com.caepi.entities.TipoEpiProtecaoBase;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class TipoEpiDTO {

    private Long id;
    private String titulo;
    private String descricao;
    private String textoProtecao;
    private Date dataRevogacao;
    private Date dataCadastro;
    private boolean certificadoDeConformidade;
    private boolean ensaioNoExterior;
    private String indicacaoDeUso;
    private boolean conjugado;
    private List<Especificidade> especificidades;
    private List<TipoEpiProtecaoBase> tiposEpiSubTiposProtecao;
    private Set<TipoEpiDTO> conjugaveis;
}
