package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@Table(name = "norma_tecnica_processo")
public class NormaTecnicaProcesso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "nome_normalizado")
    private String nomeNormalizado;

    @Column(name="descricao", columnDefinition="TEXT")
    private String descricao;

    @Column(name="observacao_niveis_desempenhos", columnDefinition="TEXT")
    private String observacaoNiveisDesempenhos;

    @Column(name = "pictograma")
    private Boolean pictograma;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "protecao_processo_id")
    @JsonBackReference("protecaoProcesso")
    private ProtecaoProcesso protecaoProcesso;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "especificidade_opcao_processo_id")
    @JsonBackReference("especificidadeOpcaoProcesso")
    private EspecificidadeOpcaoProcesso especificidadeOpcaoProcesso;

    @Column(name = "id_origem")
    private Long idOrigem;

    @ToString.Exclude
    @JsonManagedReference("normaTecnica-nivelDesempenhoProcesso")
    @OneToMany(
            mappedBy = "normaTecnicaProcesso",
            cascade = CascadeType.ALL
    )
    @Where(clause = "nivel = 1")
    @OrderBy("ordem ASC")
    private Set<NivelDesempenhoProcesso> niveisDesempenhosProcesso = new HashSet<>();
}
