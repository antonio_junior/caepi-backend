package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@Table(name = "nivel_desempenho")
public class NivelDesempenho {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "identificacao")
    private String identificacao;

    @Column(name = "titulo")
    private String titulo;

    @ToString.Exclude
    @JsonBackReference("normaTecnica-nivelDesempenho")
    @ManyToOne
    private NormaTecnica normaTecnica;

    @Column(name = "nivel")
    private Integer nivel;

    @Column(name = "ordem")
    private Integer ordem;

    @Column(name = "tipo")
    private String tipo;

    @Column(name="informacao", columnDefinition="TEXT")
    private String informacao;

    @Column(name = "texto_ilimitado")
    private String textoIlimitado;

    @Column(name = "num_caracteres")
    private Integer numCaracteres;

    @Column(name = "casas_decimais")
    private Integer casasDecimais;

    @Column(name = "ilimitado")
    private String ilimitado;

    @Column(name = "vlrmin")
    private String vlrmin;

    @Column(name = "vlrmax")
    private String vlrmax;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nivel_desempenho_pai_id")
    @ToString.Exclude
    @JsonBackReference("nivelDesempenhoPai-nivelDesempenhoSubsequentes")
    private NivelDesempenho nivelDesempenhoPai;

    @OneToMany(mappedBy = "nivelDesempenhoPai", cascade=CascadeType.ALL)
    @OrderBy("ordem ASC")
    @ToString.Exclude
    @JsonManagedReference("nivelDesempenhoPai-nivelDesempenhoSubsequentes")
    List<NivelDesempenho> nivelDesempenhoSubsequentes = new ArrayList<>();

    @Column(name="formula", columnDefinition="TEXT")
    private String formula;

    @Column(name="formula_resposta_pai", columnDefinition="TEXT")
    private String formulaRespostaPai;

    @ToString.Exclude
    @JsonManagedReference
    @OneToMany(
            mappedBy = "nivelDesempenho",
            cascade = CascadeType.ALL
    )
    @OrderBy("id ASC")
    private Set<NivelDesempenhoItem> items = new HashSet<>();

    @ToString.Exclude
    @JsonManagedReference("nivel_desempenho_regra_exibicao")
    @OneToMany(
            mappedBy = "nivelDesempenho",
            cascade = CascadeType.ALL
    )
    @OrderBy("ordem ASC")
    private Set<NivelDesempenhoRegraExibicao> regrasExibicao = new HashSet<>();
}
