package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@Table(name = "nivel_desempenho_processo")
public class NivelDesempenhoProcesso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "id_origem")
    private Long idOrigem;

    @ToString.Exclude
    @JoinColumn(name = "norma_tecnica_processo_id")
    @JsonBackReference("normaTecnica-nivelDesempenhoProcesso")
    @ManyToOne
    private NormaTecnicaProcesso normaTecnicaProcesso;

    @Column(name = "identificacao")
    private String identificacao;

    @Column(name = "titulo")
    private String titulo;

    @Column(name = "nivel")
    private Integer nivel;

    @Column(name = "ordem")
    private Integer ordem;

    @Column(name = "tipo")
    private String tipo;

    @Column(name="informacao", columnDefinition="TEXT")
    private String informacao;

    @Column(name = "texto_ilimitado")
    private String textoIlimitado;

    @Column(name = "num_caracteres")
    private Integer numCaracteres;

    @Column(name = "casas_decimais")
    private Integer casasDecimais;

    @Column(name = "ilimitado")
    private String ilimitado;

    @Column(name = "vlrmin")
    private String vlrmin;

    @Column(name = "vlrmax")
    private String vlrmax;

    @Column(name = "resposta", columnDefinition="TEXT")
    private String resposta;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nivel_desempenho_processo_pai_id")
    @ToString.Exclude
    @JsonBackReference("nivelDesempenhoPaiProcesso-nivelDesempenhoSubsequentes")
    private NivelDesempenhoProcesso nivelDesempenhoPai;

    @OneToMany(mappedBy = "nivelDesempenhoPai", cascade=CascadeType.ALL)
    @OrderBy("ordem ASC")
    @ToString.Exclude
    @JsonManagedReference("nivelDesempenhoPaiProcesso-nivelDesempenhoSubsequentes")
    List<NivelDesempenhoProcesso> nivelDesempenhoProcessoSubsequentes = new ArrayList<>();

    @ToString.Exclude
    @JsonManagedReference
    @OneToMany(
            mappedBy = "nivelDesempenhoProcesso",
            cascade = CascadeType.ALL
    )
    @OrderBy("id ASC")
    private Set<NivelDesempenhoItemProcesso> items = new HashSet<>();

    @ToString.Exclude
    @JsonManagedReference("nivel_desempenho_regra_exibicao_processo")
    @OneToMany(
            mappedBy = "nivelDesempenhoProcesso",
            cascade = CascadeType.ALL
    )
    @OrderBy("ordem ASC")
    private Set<NivelDesempenhoRegraExibicaoProcesso> regrasExibicao = new HashSet<>();
}
