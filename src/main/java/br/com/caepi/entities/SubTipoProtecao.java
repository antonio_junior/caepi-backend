package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@DiscriminatorValue("SubTipoProtecao")
@Getter
@Setter
@ToString
public class SubTipoProtecao extends ProtecaoBase {

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.EAGER)
    public TipoProtecao tipoProtecao;

    @PrePersist
    public void prePersist() {
        final Date atual = new Date();
        dataCadastro = atual;
    }
}
