package br.com.caepi.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@ToString
@Table(name = "mensagem")
public class Mensagem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="texto", columnDefinition="TEXT")
    private String texto;

    @Column(name="data_envio")
    private Date dataEnvio;

    @Column(name="data_leitura")
    private Date dataLeitura;

    @Column(name="tipo_remetente")
    private String tipoRemetente;

    @Column(name="id_remetente")
    private Long idRemetente;

    @Column(name="tipo_destinatario")
    private String tipoDestinatario;

    @Column(name="id_destinatario")
    private Long idDestinatario;

    @Column(name="entidade_origem")
    private String entidadeOrigem;

    @Column(name="id_entidade_origem")
    private Long idEntidadeOrigem;
}
