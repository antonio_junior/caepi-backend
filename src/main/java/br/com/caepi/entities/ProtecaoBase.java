package br.com.caepi.entities;

import br.com.caepi.enums.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "protecao_base")
@DiscriminatorColumn(name = "tipo", discriminatorType = DiscriminatorType.STRING)
@Getter
@Setter
@ToString
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "tipo")
@JsonSubTypes({
        @JsonSubTypes.Type(value = SubTipoProtecao.class, name = "subTipoProtecao"),
        @JsonSubTypes.Type(value = TipoProtecao.class, name = "tipoProtecao")
})
public abstract class ProtecaoBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "nome")
    protected String nome;

    @Column(name="descricao", columnDefinition="TEXT")
    protected String descricao;

    @Column(name = "data_cadastro")
    protected Date dataCadastro;

    @Enumerated(EnumType.STRING)
    protected Status status;

    @ManyToMany
    @JoinTable(
            name = "protecao_base_norma_tecnica",
            joinColumns = @JoinColumn(name = "protecao_base_id"),
            inverseJoinColumns = @JoinColumn(name = "norma_tecnica_id")
    )
    private Set<NormaTecnica> normasTecnicas;

    @JsonIgnore
    @OneToMany(mappedBy = "protecaoBase", fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH })
    List<TipoEpiProtecaoBase> epis;
}
