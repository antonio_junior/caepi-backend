package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@DiscriminatorValue("FabricanteImportador")
public class FabricanteImportador extends Empresa {

    @PrePersist
    public void prePersist() {
        final Date atual = new Date();
        dataCadastro = atual;
    }

    public boolean isAtivo() {
        if (dataRevogacao == null) {
            return true; // Se não houver data de revogação, o status é ativo
        }

        Date dataAtual = new Date();
        return dataAtual.before(dataRevogacao); // Verificar se a data atual é anterior à data de revogação
    }
}