package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
public class StatusLaboratorioOCPProcesso implements Serializable {

    Long laboratorioOcpId;
    Long processoId;
    Integer etapa;
    List<StatusEtapa> etapas;
}
