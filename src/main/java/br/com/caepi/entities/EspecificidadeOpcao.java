package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@Table(name = "especificidade_opcao")
public class EspecificidadeOpcao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ToString.Exclude
    @JsonBackReference
    @ManyToOne
    private Especificidade especificidade;

    @Column(name = "descricao")
    private String descricao;

    @Column(name="indicacao_de_uso", columnDefinition="TEXT")
    private String indicacaoDeUso;

    @Column(name="ordem")
    private Integer ordem;

    @ToString.Exclude
    @ManyToMany
    @JoinTable(
            name = "especificidade_opcao_norma_tecnica",
            joinColumns = @JoinColumn(name = "especificidade_opcao_id"),
            inverseJoinColumns = @JoinColumn(name = "norma_tecnica_id")
    )
    private Set<NormaTecnica> normasTecnicas;

    @OneToOne(mappedBy = "especificidadeOpcao", cascade = CascadeType.ALL)
    @JsonManagedReference
    @ToString.Exclude
    private EspecificidadeOpcaoRegraExibicao regraExibicao;
}
