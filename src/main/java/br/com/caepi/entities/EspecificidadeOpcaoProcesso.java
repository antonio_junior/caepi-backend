package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@Table(name = "especificidade_opcao_processo")
public class EspecificidadeOpcaoProcesso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "especificidade_processo_id")
    @ToString.Exclude
    @JsonBackReference()
    private EspecificidadeProcesso especificidadeProcesso;

    @Column(name = "descricao")
    private String descricao;

    @Column(name="indicacao_de_uso", columnDefinition="TEXT")
    private String indicacaoDeUso;

    @Column(name="ordem")
    private Integer ordem;

    @Column(name = "id_origem")
    private Long idOrigem;

    @Column(name = "especificidade_resposta_id")
    private Long especificidadeRespostaId;

    @Column(name = "especificidade_resposta_regra")
    private String especificidadeRespostaRegra;

    @OneToMany(mappedBy = "especificidadeOpcaoProcesso", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonManagedReference("especificidadeOpcaoProcesso")
    private Set<NormaTecnicaProcesso> normaTecnicaProcessos;
}
