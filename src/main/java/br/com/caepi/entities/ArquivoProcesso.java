package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "arquivo_processo")
public class ArquivoProcesso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome_arquivo")
    private String nomeArquivo;

    @Column(name = "caminho_arquivo")
    private String caminhoArquivo;

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "data_cadastro")
    protected Date dataCadastro;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "processo_id")
    private Processo processo;

    @PrePersist
    public void prePersist() {
        final Date atual = new Date();
        dataCadastro = atual;
    }

    @Column(name = "etapa")
    private Integer etapa;

    @ManyToOne
    @JoinColumn(name = "empresa_id")
    private Empresa empresa;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "visibilidade")
    private String visibilidade;
}