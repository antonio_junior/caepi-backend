package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@DiscriminatorValue("LaboratorioOCP")
@JsonIgnoreProperties(ignoreUnknown = true)
public class LaboratorioOCP extends Empresa {

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "laboratorios")
    @JsonIgnore
    private Set<TipoEpiProcesso> tipoEpiProcessos;

    @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinTable(name = "empresa_tipo_epi",
            joinColumns = @JoinColumn(name = "empresa_id"),
            inverseJoinColumns = @JoinColumn(name = "tipo_epi_id"))
    private Set<TipoEpi> tiposEpi = new HashSet<>();

    public void addTipoEpiProcesso(TipoEpiProcesso tipoEpiProcesso) {
        if(!tipoEpiProcessos.contains(tipoEpiProcesso)) {
            tipoEpiProcessos.add(tipoEpiProcesso);
            tipoEpiProcesso.addLaboratorio(this);
        }
    }

    public void removeTipoEpiProcesso(TipoEpiProcesso tipoEpiProcesso) {
        if(tipoEpiProcessos.contains(tipoEpiProcesso)) {
            tipoEpiProcessos.remove(tipoEpiProcesso);
            tipoEpiProcesso.removeLaboratorio(this);
        }
    }

    @PrePersist
    public void prePersist() {
        final Date atual = new Date();
        dataCadastro = atual;
    }

    public boolean isAtivo() {
        if (dataRevogacao == null) {
            return true; // Se não houver data de revogação, o status é ativo
        }

        Date dataAtual = new Date();
        return dataAtual.before(dataRevogacao); // Verificar se a data atual é anterior à data de revogação
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LaboratorioOCP that = (LaboratorioOCP) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}