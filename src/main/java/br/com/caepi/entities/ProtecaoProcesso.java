package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@Table(name = "protecao_processo")
public class ProtecaoProcesso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference
    private TipoEpiProcesso tipoEpiProcesso;

    @Column(name = "nome")
    private String nome;

    @Column(name = "descricao", columnDefinition="TEXT")
    private String descricao;

    @Column(name = "selecionado")
    private Boolean selecionado;

    @Column(name = "id_origem")
    private Long idOrigem;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "autor_id")
    private LaboratorioOCP autor;

    @Column(name = "data_inicio")
    private Date dataInicio;

    @Column(name = "data_fim")
    private Date dataFim;

    @Column(name = "em_revisao")
    private Boolean emRevisao;

    @OneToMany(mappedBy = "protecaoProcesso", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonManagedReference("protecaoProcesso")
    private Set<NormaTecnicaProcesso> normaTecnicaProcessos;

    @OneToMany(mappedBy = "protecaoProcesso", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonManagedReference("especificidadeProtecao")
    private Set<EspecificidadeProcesso> especificidades;
}
