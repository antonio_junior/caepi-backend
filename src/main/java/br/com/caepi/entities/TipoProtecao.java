package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@DiscriminatorValue("TipoProtecao")
@Getter
@Setter
@ToString
public class TipoProtecao extends ProtecaoBase {

    @ToString.Exclude
    @OneToMany(mappedBy = "tipoProtecao")
    @JsonBackReference
    @OrderBy("id ASC")
    private Set<SubTipoProtecao> subTipoProtecoes = new HashSet<>();

    @PrePersist
    public void prePersist() {
        final Date atual = new Date();
        dataCadastro = atual;
    }
}
