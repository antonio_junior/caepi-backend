package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@Table(name = "nivel_desempenho_item_processo")
public class NivelDesempenhoItemProcesso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "id_origem")
    private Long idOrigem;

    @ToString.Exclude
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "nivel_desempenho_processo_id")
    private NivelDesempenhoProcesso nivelDesempenhoProcesso;

    @Column(name = "rotulo")
    private String rotulo;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "ordem")
    private Integer ordem;

    @Column(name="opcoes", columnDefinition="TEXT")
    private String opcoes;

    @Column(name="informacao", columnDefinition="TEXT")
    private String informacao;

    @Column(name = "texto_ilimitado")
    private String textoIlimitado;

    @Column(name = "num_caracteres")
    private Integer numCaracteres;

    @Column(name = "casas_decimais")
    private Integer casasDecimais;

    @Column(name = "ilimitado")
    private String ilimitado;

    @Column(name = "vlrmin")
    private String vlrmin;

    @Column(name = "vlrmax")
    private String vlrmax;
}
