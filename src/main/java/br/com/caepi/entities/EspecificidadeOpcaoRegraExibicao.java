package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;
@Entity
@Getter
@Setter
@ToString
@Table(name = "especificidade_opcao_regra_exibicao")
public class EspecificidadeOpcaoRegraExibicao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JsonBackReference
    @ToString.Exclude
    private EspecificidadeOpcao especificidadeOpcao;

    @OneToOne
    private Especificidade especificidade;

    @ElementCollection
    @CollectionTable(
            name="regrasEspecificidadesOpcoes",
            joinColumns=@JoinColumn(name="regra_id")
    )
    private Set<Long> regrasEspecificidadesOpcoes;
}
