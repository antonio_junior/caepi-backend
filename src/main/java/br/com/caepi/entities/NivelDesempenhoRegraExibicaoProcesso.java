package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@Table(name = "nivel_desempenho_regra_exibicao_processo")
public class NivelDesempenhoRegraExibicaoProcesso {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "id_origem")
    private Long idOrigem;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("nivel_desempenho_regra_exibicao_processo")
    @JoinColumn(name = "nivel_desempenho_processo_id")
    private NivelDesempenhoProcesso nivelDesempenhoProcesso;

    @Column(name = "ordem")
    private Integer ordem;

    @Column(name = "campo_identificador")
    private Long campoIdentificador;

    @Column(name = "item_identificador")
    private Long itemIdentificador;

    @Column(name = "operador")
    private String operador;

    @Column(name = "valor_referencia")
    private String valorReferencia;

    @Column(name = "operador_logico")
    private String operadorLogico;
}
