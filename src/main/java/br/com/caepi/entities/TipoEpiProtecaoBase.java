package br.com.caepi.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@Table(name = "tipo_epi_protecao_base")
public class TipoEpiProtecaoBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    private TipoEpi tipoEpi;

    @ManyToOne(fetch = FetchType.EAGER)
    private ProtecaoBase protecaoBase;

    @Column(name="indicacao_de_uso", columnDefinition="TEXT")
    private String indicacaoDeUso;
}
