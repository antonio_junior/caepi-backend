package br.com.caepi.entities;

import br.com.caepi.enums.Status;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.vladmihalcea.hibernate.type.array.IntArrayType;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "processo")
@TypeDefs({
        @TypeDef(name = "string-array", typeClass = StringArrayType.class),
        @TypeDef(name = "int-array", typeClass = IntArrayType.class),
        @TypeDef(name = "json", typeClass = JsonStringType.class),
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
public class Processo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "numero_processo")
    private String numeroProcesso;

    @Column(name = "etapa")
    private Integer etapa;

    @Column(name = "etapa_avaliacao")
    private Integer etapaAvaliacao;

    @Column(name="descricao", columnDefinition="TEXT")
    private String descricao;

    @Column(name = "data_cadastro")
    private Date dataCadastro;

    @Column(name = "data_realizacao_etapa1")
    private Date dataRealizacaoEtapa1;

    @Column(name = "data_realizacao_etapa2")
    private Date dataRealizacaoEtapa2;

    @Column(name = "data_realizacao_etapa3")
    private Date dataRealizacaoEtapa3;

    @Column(name = "data_avaliacao_etapa1")
    private Date dataAvaliacaoEtapa1;

    @Column(name = "data_avaliacao_etapa2")
    private Date dataAvaliacaoEtapa2;

    @Column(name = "data_avaliacao_etapa3")
    private Date dataAvaliacaoEtapa3;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Type(type = "jsonb")
    @Column(name = "status_laboratorio_ocp_processos" ,columnDefinition = "jsonb")
    List<StatusLaboratorioOCPProcesso> statusLaboratorioOCPProcessos;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "autor_id")
    private FabricanteImportador autor;

    @OneToMany(mappedBy = "processo", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<TipoEpiProcesso> epis;

    @JsonProperty("tituloEpiPrincipal")
    public String getTituloEpiPrincipal() {
        for (TipoEpiProcesso tipoEpiProcesso : epis) {
            if (Boolean.TRUE.equals(tipoEpiProcesso.getPrincipal())) {
                return tipoEpiProcesso.getTitulo();
            }
        }
        return null;
    }

    @OneToMany(mappedBy = "processo", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<ArquivoProcesso> arquivos;

    @PrePersist
    public void prePersist() {
        final Date atual = new Date();
        dataCadastro = atual;
    }
}
