package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.*;

@Entity
@Getter
@Setter
@ToString
@Table(name = "tipo_epi")
public class TipoEpi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "titulo")
    private String titulo;

    @Column(name="descricao", columnDefinition="TEXT")
    private String descricao;

    @Column(name="texto_protecao", columnDefinition="TEXT")
    private String textoProtecao;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "America/Sao_Paulo")
    @Column(name = "data_revogacao")
    private Date dataRevogacao;

    @Column(name = "data_cadastro")
    private Date dataCadastro;

    @Column(name = "certificado_de_conformidade")
    private boolean certificadoDeConformidade;

    @Column(name = "ensaio_no_exterior")
    private boolean ensaioNoExterior;

    @Column(name="indicacao_de_uso", columnDefinition="TEXT")
    private String indicacaoDeUso;

    @Transient
    public boolean isConjugado() {
        return conjugaveis.size() > 0;
    }

    @Column(name = "processo_exlusivo_fabricante_importador")
    public Boolean processoExclusivoFabricanteImportador;

    @Column(name = "permitir_verificacao_por_link")
    public Boolean permitirVerificacaoPorLink;

    @ToString.Exclude
    @JsonManagedReference
    @OneToMany(
            mappedBy = "tipoEpi",
            cascade = CascadeType.ALL
    )
    @OrderBy("ordem ASC")
    private Set<Especificidade> especificidades = new HashSet<>();

    @OneToMany(mappedBy = "tipoEpi", fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH })
    @JsonIgnoreProperties({"tipoEpi"})
    List<TipoEpiProtecaoBase> tiposEpiSubTiposProtecao;

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinTable(
            name = "tipo_epi_conjugaveis",
            joinColumns = @JoinColumn(name = "tipo_epi_id"),
            inverseJoinColumns = @JoinColumn(name = "tipo_epi_conjugavel_id")
    )
    @JsonIgnoreProperties({"conjugaveis", "especificidades", "tiposEpiSubTiposProtecao"})
    private Set<TipoEpi> conjugaveis = new HashSet<>();

    @JsonIgnore
    @ManyToMany(mappedBy = "tiposEpi", fetch = FetchType.LAZY)
    private Set<LaboratorioOCP> laboratorios = new HashSet<>();

    public void addConjugavel(TipoEpi tipoEpi) {
        this.conjugaveis.add(tipoEpi);
        tipoEpi.getConjugaveis().add(this);
    }

    public void removeConjugavel(TipoEpi tipoEpi) {
        this.conjugaveis.remove(tipoEpi);
        tipoEpi.getConjugaveis().remove(this);
    }

    @PrePersist
    public void prePersist() {
        final Date atual = new Date();
        dataCadastro = atual;
    }

    //teste runner

    public boolean isAtivo() {
        if (dataRevogacao == null) {
            return true; // Se não houver data de revogação, o status é ativo
        }

        Date dataAtual = new Date();
        return dataAtual.before(dataRevogacao); // Verificar se a data atual é anterior à data de revogação
    }
}
