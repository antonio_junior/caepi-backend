package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@Table(name = "especificidade_processo")
@TypeDefs({
        @TypeDef(name = "string-array", typeClass = StringArrayType.class)
})
public class EspecificidadeProcesso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("especificidadeProcesso")
    private TipoEpiProcesso tipoEpiProcesso;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("especificidadeProtecao")
    private ProtecaoProcesso protecaoProcesso;

    @Column(name = "titulo")
    private String titulo;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "ordem")
    private Integer ordem;

    @Column(name = "id_origem")
    private Long idOrigem;

    @Column(name = "resposta")
    private String resposta;

    @ToString.Exclude
    @JsonManagedReference
    @OneToMany(
            mappedBy = "especificidadeProcesso",
            cascade = CascadeType.ALL
    )
    @OrderBy("ordem ASC")
    private Set<EspecificidadeOpcaoProcesso> opcoes = new HashSet<>();

    @Column(name = "especificidade_pai")
    private Long especificidadePai;

    @Type(type = "string-array")
    @Column(name= "opcoes_resposta_pai",columnDefinition = "text[]")
    private String[] opcoesRespostaPai;

    @Column(name = "obrigatorio")
    private Boolean obrigatorio;
}
