package br.com.caepi.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
public class StatusEtapa implements Serializable {

    Integer etapa;
    Date dataInicio;
    Date dataFim;
}
