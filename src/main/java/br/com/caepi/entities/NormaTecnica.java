package br.com.caepi.entities;

import br.com.caepi.enums.Status;
import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@Table(name = "norma_tecnica")
@JsonIgnoreProperties(ignoreUnknown = true)
public class NormaTecnica {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name="descricao", columnDefinition="TEXT")
    private String descricao;

    @Column(name="observacao_niveis_desempenhos", columnDefinition="TEXT")
    private String observacaoNiveisDesempenhos;

    @Column(name = "pictograma")
    private Boolean pictograma;

    @Column(name = "data_cadastro")
    private Date dataCadastro;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "America/Sao_Paulo")
    @Column(name = "data_revogacao")
    private Date dataRevogacao;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "nome_normalizado")
    private String nomeNormalizado;

    @JsonIgnore
    @ManyToMany(mappedBy = "normasTecnicas", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH })
    private List<TipoProtecao> tipoProtecoes;

    @JsonIgnore
    @ManyToMany(mappedBy = "normasTecnicas", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH })
    private List<SubTipoProtecao> subTipoProtecoes;

    @JsonIgnore
    @ManyToMany(mappedBy = "normasTecnicas", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH })
    private Set<EspecificidadeOpcao> especificidadeOpcoes;

    @ToString.Exclude
    @JsonManagedReference("normaTecnica-nivelDesempenho")
    @OneToMany(
            mappedBy = "normaTecnica",
            cascade = CascadeType.ALL
    )
    @Where(clause = "nivel = 1")
    @OrderBy("ordem ASC")
    private Set<NivelDesempenho> niveisDesempenhos = new HashSet<>();

    public NormaTecnica() {}

    public NormaTecnica(Long id) {
        this.id = id;
    }

    @PrePersist
    public void prePersist() {
        final Date atual = new Date();
        dataCadastro = atual;
    }

    public boolean isAtivo() {
        if (dataRevogacao == null) {
            return true; // Se não houver data de revogação, o status é ativo
        }

        Date dataAtual = new Date();
        return dataAtual.before(dataRevogacao); // Verificar se a data atual é anterior à data de revogação
    }
}
