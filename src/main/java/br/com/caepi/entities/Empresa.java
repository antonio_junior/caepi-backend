package br.com.caepi.entities;

import br.com.caepi.enums.Status;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;


@Entity
@Getter
@Setter
@ToString
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "empresa")
@DiscriminatorColumn(name = "tipo_empresa", discriminatorType = DiscriminatorType.STRING)
public abstract class Empresa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "razao_social")
    protected String razaoSocial;

    @Column(name = "nome_fantasia")
    protected String nomeFantasia;

    @Column(name = "cnpj")
    protected String cnpj;

    @Column(name="endereco", columnDefinition="TEXT")
    protected String endereco;

    @Column(name = "cep")
    protected String cep;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "estado_id")
    private Estado estado;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "municipio_id")
    private Municipio municipio;

    @Column(name = "email")
    protected String email;

    @Column(name = "telefone")
    protected String telefone;

    @Column(name = "data_cadastro")
    protected Date dataCadastro;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "America/Sao_Paulo")
    @Column(name = "data_revogacao")
    protected Date dataRevogacao;

    @Enumerated(EnumType.STRING)
    protected Status status;

    @Column(name = "tipo")
    protected String tipo;
}