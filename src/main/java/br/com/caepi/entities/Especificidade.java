package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@Table(name = "especificidade")
@TypeDefs({
        @TypeDef(name = "string-array", typeClass = StringArrayType.class)
})
public class Especificidade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "titulo")
    private String titulo;

    @Column(name = "tipo")
    private String tipo;

    @ToString.Exclude
    @JsonBackReference
    @ManyToOne
    private TipoEpi tipoEpi;

    @Column(name = "ordem")
    private Integer ordem;

    @ElementCollection
    @CollectionTable(
            name="tiposEpisSubTiposProtecao",
            joinColumns=@JoinColumn(name="especificidade_id")
    )
    private Set<Long> tiposEpisSubTiposProtecao;

    @Column(name = "protecao_id")
    private Long protecaoId;

    @Column(name = "especificidade_pai")
    private Long especificidadePai;

    @Type(type = "string-array")
    @Column(name= "opcoes_resposta_pai",columnDefinition = "text[]")
    private String[] opcoesRespostaPai;

    @ToString.Exclude
    @JsonManagedReference
    @OneToMany(
            mappedBy = "especificidade",
            cascade = CascadeType.ALL
    )
    @OrderBy("ordem ASC")
    private Set<EspecificidadeOpcao> opcoes = new HashSet<>();

    @Column(name = "obrigatorio")
    private Boolean obrigatorio;

    @Column(name = "assinado_digitalmente")
    private Boolean assinadoDigitalmente;
}
