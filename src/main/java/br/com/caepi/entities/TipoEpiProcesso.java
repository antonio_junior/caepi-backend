package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@Table(name = "tipo_epi_processo")
public class TipoEpiProcesso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference
    private Processo processo;

    @Column(name = "tipo_epi_id")
    private Long tipoEpiId;

    @Column(name = "principal")
    private Boolean principal;

    @Column(name = "titulo")
    private String titulo;

    @Column(name = "descricao", columnDefinition="TEXT")
    private String descricao;

    @Column(name = "texto_protecao", columnDefinition="TEXT")
    private String textoProtecao;

    @Column(name = "indicacao_de_uso", columnDefinition="TEXT")
    private String indicacaoDeUso;

    @OneToMany(mappedBy = "tipoEpiProcesso", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<ProtecaoProcesso> protecoes;

    @OneToMany(mappedBy = "tipoEpiProcesso", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonManagedReference("especificidadeProcesso")
    private Set<EspecificidadeProcesso> especificidadesGerais;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "autor_especificidades_gerais_id")
    private LaboratorioOCP autorEspecificidadesGerais;

    @Column(name = "data_inicio_especificidades_gerais")
    private Date dataInicioEspeficidadesGerais;

    @Column(name = "data_fim_especificidades_gerais")
    private Date dataFimEspecificidadesGerais;

    @Column(name = "em_revisao_especificidades_gerais")
    private Boolean emRevisaoEspecificidadesGerais;

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinTable(
            name = "tipo_epi_processo_empresa",
            joinColumns = @JoinColumn(name = "tipo_epi_processo_id"),
            inverseJoinColumns = @JoinColumn(name = "empresa_id")
    )
    private Set<LaboratorioOCP> laboratorios;

    public void addLaboratorio(LaboratorioOCP laboratorioOCP) {
        if(!laboratorios.contains(laboratorioOCP)) {
            laboratorios.add(laboratorioOCP);
            laboratorioOCP.addTipoEpiProcesso(this);
        }
    }

    public void removeLaboratorio(LaboratorioOCP laboratorioOCP) {
        if(laboratorios.contains(laboratorioOCP)) {
            laboratorios.remove(laboratorioOCP);
            laboratorioOCP.removeTipoEpiProcesso(this);
        }
    }
}