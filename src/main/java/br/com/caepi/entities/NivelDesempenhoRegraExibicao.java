package br.com.caepi.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@Table(name = "nivel_desempenho_regra_exibicao")
public class NivelDesempenhoRegraExibicao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference("nivel_desempenho_regra_exibicao")
    private NivelDesempenho nivelDesempenho;

    @Column(name = "ordem")
    private Integer ordem;

    @Column(name = "campo_identificador")
    private Long campoIdentificador;

    @Column(name = "item_identificador")
    private Long itemIdentificador;

    @Column(name = "operador")
    private String operador;

    @Column(name = "valor_referencia")
    private String valorReferencia;

    @Column(name = "operador_logico")
    private String operadorLogico;
}
