package br.com.caepi.repository;

import br.com.caepi.entities.EspecificidadeOpcao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

public interface EspecificidadeOpcaoRepository extends JpaRepository<EspecificidadeOpcao, Long>, JpaSpecificationExecutor<EspecificidadeOpcao> {
    public Set<EspecificidadeOpcao> findAllByEspecificidade_Id(Long id);

    @Query(value = "SELECT MAX(ordem) FROM especificidade_opcao WHERE especificidade_id = ?1", nativeQuery = true)
    public Integer pegarUltimaOrdem(Long idEspecificidade);
}
