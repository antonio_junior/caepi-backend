package br.com.caepi.repository;

import br.com.caepi.entities.NivelDesempenho;
import br.com.caepi.entities.NivelDesempenhoItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

public interface NivelDesempenhoItemRepository extends JpaRepository<NivelDesempenhoItem, Long>, JpaSpecificationExecutor<NivelDesempenhoItem> {
    @Query("SELECT MAX(ndi.ordem) FROM NivelDesempenhoItem ndi WHERE ndi.nivelDesempenho = :nivelDesempenho")
    public Integer findMaxOrdemNivelDesempenhoItemByNivelDesempenho(NivelDesempenho nivelDesempenho);
}
