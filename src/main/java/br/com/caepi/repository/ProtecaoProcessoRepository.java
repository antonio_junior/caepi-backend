package br.com.caepi.repository;

import br.com.caepi.entities.ProtecaoProcesso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ProtecaoProcessoRepository extends JpaRepository<ProtecaoProcesso, Long>, JpaSpecificationExecutor<ProtecaoProcesso> {
}
