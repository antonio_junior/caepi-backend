package br.com.caepi.repository;


import br.com.caepi.entities.Municipio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MunicipioRepository extends JpaRepository<Municipio, Long> {

    @Query("SELECT m FROM Municipio m WHERE m.estado.id = :estadoId")
    List<Municipio> findByEstadoId(Long estadoId);
}

