package br.com.caepi.repository;

import br.com.caepi.entities.NivelDesempenhoItemProcesso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface NivelDesempenhoItemProcessoRepository extends JpaRepository<NivelDesempenhoItemProcesso, Long>, JpaSpecificationExecutor<NivelDesempenhoItemProcesso> {
}
