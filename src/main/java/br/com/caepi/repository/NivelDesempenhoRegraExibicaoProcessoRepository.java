package br.com.caepi.repository;

import br.com.caepi.entities.NivelDesempenhoRegraExibicaoProcesso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface NivelDesempenhoRegraExibicaoProcessoRepository extends JpaRepository<NivelDesempenhoRegraExibicaoProcesso, Long>, JpaSpecificationExecutor<NivelDesempenhoRegraExibicaoProcesso> {
}
