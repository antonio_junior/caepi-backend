package br.com.caepi.repository;

import br.com.caepi.entities.NivelDesempenhoProcesso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface NivelDesempenhoProcessoRepository extends JpaRepository<NivelDesempenhoProcesso, Long>, JpaSpecificationExecutor<NivelDesempenhoProcesso> {
}
