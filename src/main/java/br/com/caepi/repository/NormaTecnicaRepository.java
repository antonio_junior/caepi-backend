package br.com.caepi.repository;

import br.com.caepi.entities.NormaTecnica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface NormaTecnicaRepository extends JpaRepository<NormaTecnica, Long>, JpaSpecificationExecutor<NormaTecnica> {

    @Query("SELECT CASE WHEN COUNT(nt) > 0 THEN true ELSE false END FROM NormaTecnica nt WHERE nt.nomeNormalizado = :nomeNormalizado")
    boolean existsByNomeNormalizado(@Param("nomeNormalizado") String nomeNormalizado);
}

