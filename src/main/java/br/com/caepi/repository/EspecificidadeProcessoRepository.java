package br.com.caepi.repository;

import br.com.caepi.entities.EspecificidadeProcesso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface EspecificidadeProcessoRepository extends JpaRepository<EspecificidadeProcesso, Long>, JpaSpecificationExecutor<EspecificidadeProcesso> {
}
