package br.com.caepi.repository;

import br.com.caepi.entities.ArquivoProcesso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArquivoProcessoRepository extends JpaRepository<ArquivoProcesso, Long> {
}
