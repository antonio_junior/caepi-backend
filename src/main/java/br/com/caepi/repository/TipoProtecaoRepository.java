package br.com.caepi.repository;

import br.com.caepi.entities.TipoProtecao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TipoProtecaoRepository extends JpaRepository<TipoProtecao, Long>, JpaSpecificationExecutor<TipoProtecao> {
    @Override
    @Query("SELECT tp FROM TipoProtecao tp order by tp.nome ASC")
    List<TipoProtecao> findAll();
}
