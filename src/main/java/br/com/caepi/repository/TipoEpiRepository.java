package br.com.caepi.repository;

import br.com.caepi.entities.TipoEpi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TipoEpiRepository extends JpaRepository<TipoEpi, Long>, JpaSpecificationExecutor<TipoEpi> {
}
