package br.com.caepi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TipoEpiProcessoRepository extends JpaRepository<br.com.caepi.entities.TipoEpiProcesso, Long>, JpaSpecificationExecutor<br.com.caepi.entities.TipoEpiProcesso> {
}
