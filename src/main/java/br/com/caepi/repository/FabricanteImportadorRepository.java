package br.com.caepi.repository;

import br.com.caepi.entities.FabricanteImportador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface FabricanteImportadorRepository extends JpaRepository<FabricanteImportador, Long>, JpaSpecificationExecutor<FabricanteImportador> {
}
