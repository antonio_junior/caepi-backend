package br.com.caepi.repository;

import br.com.caepi.entities.NivelDesempenho;
import br.com.caepi.entities.NormaTecnica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface NivelDesempenhoRepository extends JpaRepository<NivelDesempenho, Long>, JpaSpecificationExecutor<NivelDesempenho> {

    @Query(value = "SELECT nd.identificacao FROM nivel_desempenho nd WHERE nd.norma_tecnica_id = :normaTecnicaId AND nd.nivel = :nivel ORDER BY nd.id DESC LIMIT 1", nativeQuery = true)
    Optional<String> findLastIdentificacaoByNormaTecnica(Long normaTecnicaId, Integer nivel);
    @Query("SELECT MAX(nd.ordem) FROM NivelDesempenho nd WHERE nd.normaTecnica = :normaTecnica AND nd.nivel = :nivel")
    Integer findMaxOrdemNivelDesempenhoByNormaTecnica(NormaTecnica normaTecnica, Integer nivel);
    @Query(value = "SELECT identificacao FROM nivel_desempenho WHERE nivel_desempenho_pai_id = :parentId ORDER BY id DESC LIMIT 1", nativeQuery = true)
    Optional<String> findLastChildIdentificacao(@Param("parentId") Long parentId);

    @Query(value = "WITH RECURSIVE subniveis AS (" +
            "SELECT id, nivel_desempenho_pai_id, 1 AS nivel " +
            "FROM nivel_desempenho WHERE id = :id " +
            "UNION ALL " +
            "SELECT nd.id, nd.nivel_desempenho_pai_id, sn.nivel + 1 " +
            "FROM nivel_desempenho nd " +
            "JOIN subniveis sn ON nd.id = sn.nivel_desempenho_pai_id" +
            ") " +
            "SELECT MAX(nivel) FROM subniveis", nativeQuery = true)
    Integer findNivelById(@Param("id") Long id);

    List<NivelDesempenho> findByNormaTecnicaId(Long normaTecnicaId);

    @Query("SELECT MAX(n.ordem) FROM NivelDesempenho n WHERE n.normaTecnica.id = :normaTecnicaId AND n.nivel = :nivel")
    Integer findMaxOrdemNivelDesempenhoByNormaTecnica(Long normaTecnicaId, Integer nivel);
}
