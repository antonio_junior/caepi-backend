package br.com.caepi.repository;

import br.com.caepi.entities.NormaTecnicaProcesso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface NormaTecnicaProcessoRepository extends JpaRepository<NormaTecnicaProcesso, Long>, JpaSpecificationExecutor<NormaTecnicaProcesso> {
}
