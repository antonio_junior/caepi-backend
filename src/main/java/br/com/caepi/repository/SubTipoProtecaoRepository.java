package br.com.caepi.repository;

import br.com.caepi.entities.SubTipoProtecao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubTipoProtecaoRepository extends JpaRepository<SubTipoProtecao, Long>, JpaSpecificationExecutor<SubTipoProtecao> {
    List<SubTipoProtecao> findAllByOrderByTipoProtecao_NomeAsc();
}