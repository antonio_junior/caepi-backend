package br.com.caepi.repository;

import br.com.caepi.entities.LaboratorioOCP;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface LaboratorioOCPRepository extends JpaRepository<LaboratorioOCP, Long>, JpaSpecificationExecutor<LaboratorioOCP> {
}
