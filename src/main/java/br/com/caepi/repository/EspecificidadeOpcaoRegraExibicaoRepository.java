package br.com.caepi.repository;

import br.com.caepi.entities.EspecificidadeOpcaoRegraExibicao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface EspecificidadeOpcaoRegraExibicaoRepository  extends JpaRepository<EspecificidadeOpcaoRegraExibicao, Long>, JpaSpecificationExecutor<EspecificidadeOpcaoRegraExibicao> {

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM regras_especificidades_opcoes WHERE regra_id = :id", nativeQuery = true)
    void deleteRegrasEspecificidadesOpcoesByRegraId(Long id);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM especificidade_opcao_regra_exibicao WHERE id = :id", nativeQuery = true)
    void deleteEspecificidadeOpcaoRegraExibicaoById(Long id);
}
