package br.com.caepi.repository;

import br.com.caepi.entities.EspecificidadeOpcaoProcesso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface EspecificidadeOpcaoProcessoRepository extends JpaRepository<EspecificidadeOpcaoProcesso, Long>, JpaSpecificationExecutor<EspecificidadeOpcaoProcesso> {
}
