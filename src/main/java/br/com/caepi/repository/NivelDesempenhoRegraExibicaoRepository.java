package br.com.caepi.repository;

import br.com.caepi.entities.NivelDesempenhoRegraExibicao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface NivelDesempenhoRegraExibicaoRepository extends JpaRepository<NivelDesempenhoRegraExibicao, Long>, JpaSpecificationExecutor<NivelDesempenhoRegraExibicao> {
}
