package br.com.caepi.repository;

import br.com.caepi.entities.TipoEpiProtecaoBase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface TipoEpiProtecaoBaseRepository extends JpaRepository<TipoEpiProtecaoBase, Long>, JpaSpecificationExecutor<TipoEpiProtecaoBase> {
    @Transactional
    @Modifying
    @Query(value = "DELETE FROM tipo_epi_protecao_base WHERE id = ?1", nativeQuery = true)
    void deleteByIdNative(Long id);
}
