package br.com.caepi.repository;

import br.com.caepi.entities.Mensagem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MensagemRepository extends JpaRepository<Mensagem, Long>, JpaSpecificationExecutor<Mensagem> {

    @Query("SELECT msg FROM Mensagem msg WHERE msg.entidadeOrigem = :entidadeOrigem AND msg.idEntidadeOrigem = :idEntidadeOrigem ORDER BY msg.dataEnvio DESC")
    Page<Mensagem> findAllByEntidadeOrigemAndIdEntidadeOrigem(@Param("entidadeOrigem") String entidadeOrigem, @Param("idEntidadeOrigem") Long idEntidadeOrigem, Pageable pageable);
}
