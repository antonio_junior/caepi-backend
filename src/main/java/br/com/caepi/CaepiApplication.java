package br.com.caepi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CaepiApplication {
	// teste comentario
	public static void main(String[] args) {
		SpringApplication.run(CaepiApplication.class, args);
	}

}
