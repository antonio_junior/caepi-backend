package br.com.caepi.controller;

import br.com.caepi.DTOS.VincularNormasTecnicasRequest;
import br.com.caepi.entities.EspecificidadeOpcao;
import br.com.caepi.service.EspecificidadeOpcaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/especificidades-opcoes")
public class EspecificidadeOpcaoController {

    @Autowired
    private EspecificidadeOpcaoService especificidadeOpcaoService;

    @PostMapping
    public EspecificidadeOpcao salvar(@RequestBody EspecificidadeOpcao especificidadeOpcao) {
        return especificidadeOpcaoService.salvar(especificidadeOpcao);
    }

    @PutMapping("/exibicao/{id}")
    public ResponseEntity<EspecificidadeOpcao> atualizarExibicao(@PathVariable Long id, @RequestBody EspecificidadeOpcao especificidadeOpcaoAtualizada) {
        Optional<EspecificidadeOpcao> especificidadeOpcao = especificidadeOpcaoService.buscarPorId(id);
        if (especificidadeOpcao.isPresent()) {
            especificidadeOpcaoAtualizada.setId(especificidadeOpcao.get().getId());
            return ResponseEntity.ok(especificidadeOpcaoService.atualizarExibicao(id, especificidadeOpcaoAtualizada));
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @PutMapping("/{id}")
    public ResponseEntity<EspecificidadeOpcao> atualizar(@PathVariable Long id, @RequestBody EspecificidadeOpcao especificidadeOpcaoAtualizada) {
        Optional<EspecificidadeOpcao> especificidadeOpcao = especificidadeOpcaoService.buscarPorId(id);
        if (especificidadeOpcao.isPresent()) {
            especificidadeOpcaoAtualizada.setId(especificidadeOpcao.get().getId());
            return ResponseEntity.ok(especificidadeOpcaoService.atualizar(id, especificidadeOpcaoAtualizada));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<EspecificidadeOpcao> buscarPorId(@PathVariable Long id) {
        Optional<EspecificidadeOpcao> especificidadeOpcao = especificidadeOpcaoService.buscarPorId(id);
        return especificidadeOpcao.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> excluirEspecificidadeOpcao(@PathVariable("id") Long id) {
        try {
            especificidadeOpcaoService.excluir(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PostMapping("vincular-normas-tecnicas")
    public ResponseEntity<EspecificidadeOpcao> vincularNormasTecnicas(@RequestBody VincularNormasTecnicasRequest body) {

        EspecificidadeOpcao novoEspecificidadeOpcao = especificidadeOpcaoService.vincularNormaTecnica(body.getEntityId(), body.getNormasTecnicas());

        return ResponseEntity.status(HttpStatus.OK).body(novoEspecificidadeOpcao);
    }
}