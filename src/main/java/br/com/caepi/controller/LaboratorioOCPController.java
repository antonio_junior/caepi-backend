package br.com.caepi.controller;

import br.com.caepi.DTOS.LaboratorioOCPDTO;
import br.com.caepi.entities.LaboratorioOCP;
import br.com.caepi.service.LaboratorioOCPService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/laboratorios-ocp")
public class LaboratorioOCPController {

    private final LaboratorioOCPService laboratorioOCPService;

    public LaboratorioOCPController(LaboratorioOCPService laboratorioOCPService) {
        this.laboratorioOCPService = laboratorioOCPService;
    }

    @GetMapping
    public Set<LaboratorioOCP> listarTodosLaboratoriosOCPs() {
        return laboratorioOCPService.listarTodosLaboratoriosOCPs();
    }

    @GetMapping("/filtrados")
    public ResponseEntity<?> listarTodosLaboratoriosOCPsFiltrados(@RequestParam Map<String, String> queryParameters
            , Pageable pageable) {
        return new ResponseEntity<>(laboratorioOCPService.findAll(queryParameters, pageable), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<LaboratorioOCP> buscarPorId(@PathVariable Long id) {
        Optional<LaboratorioOCP> laboratorioOCP = laboratorioOCPService.buscarPorId(id);
        return laboratorioOCP.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public LaboratorioOCP salvar(@RequestBody LaboratorioOCP laboratorioOCP) {
        return laboratorioOCPService.salvar(laboratorioOCP);
    }

    @PutMapping("/{id}")
    public ResponseEntity<LaboratorioOCP> atualizar(@PathVariable Long id, @RequestBody LaboratorioOCP laboratorioOCPatualizado) {
        Optional<LaboratorioOCP> laboratorioOCP = laboratorioOCPService.buscarPorId(id);
        if (laboratorioOCP.isPresent()) {
            laboratorioOCPatualizado.setId(laboratorioOCP.get().getId());
            laboratorioOCPService.atualizar(laboratorioOCPatualizado);
            return ResponseEntity.ok(laboratorioOCPatualizado);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/tipo-epi/{id}")
    public ResponseEntity<LaboratorioOCPDTO> atualizarLaboratorioOCPTipoEPI(@PathVariable Long id, @RequestBody LaboratorioOCPDTO laboratorioOCPAtualizado) {
        Optional<LaboratorioOCP> laboratorioOCP = laboratorioOCPService.buscarPorId(id);

        if (laboratorioOCP.isPresent()) {
            laboratorioOCPAtualizado.setId(laboratorioOCP.get().getId());
            laboratorioOCPService.atualizarLaboratorioOCPTipoEPI(laboratorioOCPAtualizado);
            return ResponseEntity.ok(laboratorioOCPAtualizado);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> excluirLaboratorioOCP(@PathVariable("id") Long id) {
        laboratorioOCPService.excluirLaboratorioOCP(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
