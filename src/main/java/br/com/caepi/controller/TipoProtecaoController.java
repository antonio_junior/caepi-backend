package br.com.caepi.controller;

import br.com.caepi.DTOS.VincularNormasTecnicasRequest;
import br.com.caepi.entities.TipoProtecao;
import br.com.caepi.service.TipoProtecaoService;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/tipo-protecao")
public class TipoProtecaoController {
    private final TipoProtecaoService tipoProtecaoService;

    public TipoProtecaoController(TipoProtecaoService tipoProtecaoService) {
        this.tipoProtecaoService = tipoProtecaoService;
    }

    @GetMapping
    public Set<TipoProtecao> listarTodosGrupoTipoProtecao() {
        return tipoProtecaoService.listarTodosGrupoTipoProtecao();
    }

    @GetMapping("/filtrados")
    public ResponseEntity<?> listarTodosGruposTipoProtecaoFiltrados(@RequestParam Map<String, String> queryParameters
            , Pageable pageable) {
        return new ResponseEntity<>(tipoProtecaoService.findAllDTO(queryParameters, pageable), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TipoProtecao> buscarPorId(@PathVariable Long id) {
        Optional<TipoProtecao> grupoTipoProtecao = tipoProtecaoService.buscarPorId(id);
        return grupoTipoProtecao.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }


    @PostMapping
    public TipoProtecao salvar(@RequestBody TipoProtecao tipoProtecao) {
        return tipoProtecaoService.salvar(tipoProtecao);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TipoProtecao> atualizar(@PathVariable Long id, @RequestBody TipoProtecao tipoProtecaoAtualizado) {
        Optional<TipoProtecao> grupoTipoProtecao = tipoProtecaoService.buscarPorId(id);
        if (grupoTipoProtecao.isPresent()) {
            tipoProtecaoAtualizado.setId(grupoTipoProtecao.get().getId());
            tipoProtecaoService.atualizar(tipoProtecaoAtualizado);
            return ResponseEntity.ok(tipoProtecaoAtualizado);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> excluirGrupoTipoProtecao(@PathVariable("id") Long id) {
        tipoProtecaoService.excluirGrupoTipoProtecao(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("vincular-normas-tecnicas")
    public ResponseEntity<TipoProtecao> vincularNormasTecnicas(@RequestBody VincularNormasTecnicasRequest body) {

        TipoProtecao novoTipoProtecao = tipoProtecaoService.vincularNormaTecnica(body.getEntityId(), body.getNormasTecnicas());

        return ResponseEntity.status(HttpStatus.OK).body(novoTipoProtecao);
    }

    @RequestMapping(value="/report-excel", method = RequestMethod.GET)
    public ResponseEntity<?> reportExcel(@RequestParam Map<String, String> queryParameters, final HttpServletRequest request,
                                         final HttpServletResponse response) throws Exception{

        ByteArrayResource relatorio_excel = tipoProtecaoService.reportExcel(queryParameters, request, response);
        return new ResponseEntity<> (relatorio_excel, HttpStatus.OK);
    }
}

