package br.com.caepi.controller;

import br.com.caepi.entities.FabricanteImportador;
import br.com.caepi.service.FabricanteImportadorService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/fabricante-importador")
public class FabricanteImportadorController {

    private final FabricanteImportadorService fabricanteImportadorService;

    public FabricanteImportadorController(FabricanteImportadorService fabricanteImportadorService) {
        this.fabricanteImportadorService = fabricanteImportadorService;
    }

    @GetMapping
    public Set<FabricanteImportador> listarTodosFabricantesImportadores() {
        return fabricanteImportadorService.listarTodosFabricantesImportadores();
    }

    @GetMapping("/filtrados")
    public ResponseEntity<?> listarTodosFabricantesImportadoresFiltrados(@RequestParam Map<String, String> queryParameters, Pageable pageable) {
        return new ResponseEntity<>(fabricanteImportadorService.findAll(queryParameters, pageable), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<FabricanteImportador> buscarPorId(@PathVariable Long id) {
        Optional<FabricanteImportador> fabricanteImportador = fabricanteImportadorService.buscarPorId(id);
        return fabricanteImportador.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public FabricanteImportador salvar(@RequestBody FabricanteImportador fabricanteImportador) {
        return fabricanteImportadorService.salvar(fabricanteImportador);
    }

    @PutMapping("/{id}")
    public ResponseEntity<FabricanteImportador> atualizar(@PathVariable Long id, @RequestBody FabricanteImportador fabricanteImportadorAtualizado) {
        Optional<FabricanteImportador> fabricanteImportador = fabricanteImportadorService.buscarPorId(id);
        if (fabricanteImportador.isPresent()) {
            fabricanteImportadorAtualizado.setId(fabricanteImportador.get().getId());
            fabricanteImportadorService.atualizar(fabricanteImportadorAtualizado);
            return ResponseEntity.ok(fabricanteImportadorAtualizado);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> excluirFabricanteImportador(@PathVariable("id") Long id) {
        fabricanteImportadorService.excluirFabricanteImportador(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}