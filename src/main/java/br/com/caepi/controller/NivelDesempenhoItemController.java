package br.com.caepi.controller;

import br.com.caepi.entities.NivelDesempenhoItem;
import br.com.caepi.service.NivelDesempenhoItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/nivel-desempenho-items")
public class NivelDesempenhoItemController {

    @Autowired
    private NivelDesempenhoItemService nivelDesempenhoItemService;

    @PostMapping
    public NivelDesempenhoItem salvar(@RequestBody NivelDesempenhoItem nivelDesempenhoItem) {
        return nivelDesempenhoItemService.salvar(nivelDesempenhoItem);
    }

    @PutMapping("/{id}")
    public ResponseEntity<NivelDesempenhoItem> atualizar(@PathVariable Long id, @RequestBody NivelDesempenhoItem nivelDesempenhoItemAtualizada) {
        Optional<NivelDesempenhoItem> nivelDesempenhoItem = nivelDesempenhoItemService.buscarPorId(id);
        if (nivelDesempenhoItem.isPresent()) {
            nivelDesempenhoItemAtualizada.setId(nivelDesempenhoItem.get().getId());
            return ResponseEntity.ok(nivelDesempenhoItemService.atualizar(nivelDesempenhoItemAtualizada));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<NivelDesempenhoItem> buscarPorId(@PathVariable Long id) {
        Optional<NivelDesempenhoItem> nivelDesempenhoItem = nivelDesempenhoItemService.buscarPorId(id);
        return nivelDesempenhoItem.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> excluirNivelDesempenhoItem(@PathVariable("id") Long id) {
        try {
            nivelDesempenhoItemService.excluir(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
