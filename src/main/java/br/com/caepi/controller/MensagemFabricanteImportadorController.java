package br.com.caepi.controller;

import br.com.caepi.entities.LaboratorioOCP;
import br.com.caepi.entities.Mensagem;
import br.com.caepi.service.MensagemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/fabricante-importador/mensagens")
public class MensagemFabricanteImportadorController {

    @Autowired
    private MensagemService mensagemService;

    @PostMapping
    public Mensagem salvar(@RequestBody Mensagem mensagem) {
        return mensagemService.save(mensagem);
    }

    @GetMapping
    public Page<Mensagem> buscarPorEntidadeOrigemEIdEntidadeOrigem(@RequestParam Map<String, String> queryParameters, Pageable pageable) {
        return mensagemService.findByEntidadeOrigemAndIdEntidadeOrigem(queryParameters.get("entidadeOrigem"), Long.parseLong(queryParameters.get("idEntidadeOrigem")), pageable);
    }
}
