package br.com.caepi.controller;

import br.com.caepi.entities.TipoEpiProtecaoBase;
import br.com.caepi.service.TipoEpiProtecaoBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/tipos-epi-protecoes-base")
public class TipoEpiProtecaoBaseController {

    private final TipoEpiProtecaoBaseService tipoEpiProtecaoBaseService;

    @Autowired
    public TipoEpiProtecaoBaseController(TipoEpiProtecaoBaseService tipoEpiProtecaoBaseService) {
        this.tipoEpiProtecaoBaseService = tipoEpiProtecaoBaseService;
    }

    @GetMapping("/filtrados")
    public ResponseEntity<?> listarTodosTiposEpisProtecoesBaseFiltrados(@RequestParam Map<String, String> queryParameters
            , Pageable pageable) {
        return new ResponseEntity<>(tipoEpiProtecaoBaseService.findAll(queryParameters, pageable), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<TipoEpiProtecaoBase> criarTipoEpiProtecaoBase(@RequestBody Map<String, String> body) {
        Long tipoEpiId = Long.parseLong(body.get("tipoEpiId"));
        Long protecaoBaseId = Long.parseLong(body.get("protecaoBaseId"));
        String tipo = body.get("tipo");

        TipoEpiProtecaoBase novoTipoEpiProtecaoBase = tipoEpiProtecaoBaseService.criarTipoEpiProtecaoBase(
                tipoEpiId, protecaoBaseId, tipo);

        return ResponseEntity.status(HttpStatus.CREATED).body(novoTipoEpiProtecaoBase);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TipoEpiProtecaoBase> buscarPorId(@PathVariable Long id) {
        Optional<TipoEpiProtecaoBase> tipoEpiTipoProtecao = tipoEpiProtecaoBaseService.buscarPorId(id);
        return tipoEpiTipoProtecao.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> excluirTipoEpiProtecaoBase(@PathVariable("id") Long id) {
        tipoEpiProtecaoBaseService.excluirTipoEpiProtecaoBase(id);
        return ResponseEntity.noContent().build();
    }
}