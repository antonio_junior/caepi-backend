package br.com.caepi.controller;

import br.com.caepi.entities.ArquivoProcesso;
import br.com.caepi.entities.Processo;
import br.com.caepi.service.ArquivoProcessoService;
import br.com.caepi.service.ProcessoService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@RestController
@RequestMapping("/arquivos")
public class ArquivoProcessoController {

    private final ArquivoProcessoService arquivoProcessoService;
    private final ProcessoService processoService;

    public ArquivoProcessoController(ArquivoProcessoService arquivoProcessoService, ProcessoService processoService) {
        this.arquivoProcessoService = arquivoProcessoService;
        this.processoService = processoService;
    }

    @PostMapping("/processo/{processoId}")
    public ResponseEntity<ArquivoProcesso> uploadArquivo(@PathVariable Long processoId,
                                                         @RequestParam(value = "codigo", required = false) String codigo,
                                                         @RequestParam(value = "descricao", required = false) String descricao,
                                                         @RequestParam(value = "etapa", required = false) Integer etapa,
                                                         @RequestParam(value = "empresaId", required = false) Long empresaId,
                                                         @RequestParam("tipo") String tipo,
                                                         @RequestParam("visibilidade") String visibilidade,
                                                         @RequestParam("file") MultipartFile file) throws IOException {
        Optional<Processo> processoOptional = processoService.buscarPorId(processoId);
        if (processoOptional.isPresent()) {
            ArquivoProcesso arquivoProcesso = arquivoProcessoService.salvarArquivo(file, processoOptional.get(),
                    codigo, descricao, etapa, empresaId, tipo, visibilidade);
            return ResponseEntity.ok(arquivoProcesso);
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/{arquivoId}")
    public ResponseEntity<byte[]> downloadArquivo(@PathVariable Long arquivoId) throws IOException {
        Optional<ArquivoProcesso> arquivoProcessoOptional = arquivoProcessoService.findById(arquivoId);
        if (arquivoProcessoOptional.isPresent()) {
            ArquivoProcesso arquivoProcesso = arquivoProcessoOptional.get();
            Path filePath = Paths.get(arquivoProcesso.getCaminhoArquivo());
            byte[] arquivo = Files.readAllBytes(filePath);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            headers.setContentDispositionFormData("attachment", arquivoProcesso.getNomeArquivo());
            headers.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
            return ResponseEntity.ok().headers(headers).body(arquivo);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{arquivoId}")
    public ResponseEntity<Void> deletarArquivo(@PathVariable Long arquivoId) throws IOException {
        Optional<ArquivoProcesso> arquivoProcessoOptional = arquivoProcessoService.findById(arquivoId);
        if (arquivoProcessoOptional.isPresent()) {
            arquivoProcessoService.deletarArquivo(arquivoId);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }
}