package br.com.caepi.controller;

import br.com.caepi.entities.Processo;
import br.com.caepi.service.ProcessoService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/fabricante-importador/processos")
public class ProcessoFabricanteImportadorController {

    private final ProcessoService processoService;

    public ProcessoFabricanteImportadorController(ProcessoService processoService) {
        this.processoService = processoService;
    }

    @GetMapping
    public Set<Processo> listarTodosProcesso() {
        return processoService.listarTodosProcesso();
    }

    @GetMapping("/filtrados")
    public ResponseEntity<?> listarTodosProcessosFiltrados(@RequestParam Map<String, String> queryParameters
            , Pageable pageable) {
        return new ResponseEntity<>(processoService.findAll(queryParameters, pageable), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Processo> buscarPorId(@PathVariable Long id) {
        return processoService.buscarPorId(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Processo criar(@RequestBody Processo processo) {
        return processoService.criar(processo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Processo> salvar(@PathVariable Long id, @RequestBody Processo processoAtualizado) {
        return processoService.buscarPorId(id)
                .map(processo -> {
                    processoAtualizado.setId(processo.getId());
                    return ResponseEntity.ok(processoService.atualizar(processoAtualizado));
                })
                .orElse(ResponseEntity.notFound().build());
    }

    @PutMapping("/avancar-etapa/{id}")
    public ResponseEntity<Processo> avancarEtapa(@PathVariable Long id, @RequestBody Processo processoAtualizado) {
        return processoService.buscarPorId(id)
                .map(processo -> {
                    processoAtualizado.setId(processo.getId());
                    return ResponseEntity.ok(processoService.avancarEtapaFabricante(processoAtualizado));
                })
                .orElse(ResponseEntity.notFound().build());
    }
}
