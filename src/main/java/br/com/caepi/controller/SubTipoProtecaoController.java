package br.com.caepi.controller;

import br.com.caepi.DTOS.VincularNormasTecnicasRequest;
import br.com.caepi.entities.SubTipoProtecao;
import br.com.caepi.entities.TipoProtecao;
import br.com.caepi.service.SubTipoProtecaoService;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/sub-tipo-protecao")
public class SubTipoProtecaoController {
    private final SubTipoProtecaoService subTipoProtecaoService;

    public SubTipoProtecaoController(SubTipoProtecaoService subTipoProtecaoService) {
        this.subTipoProtecaoService = subTipoProtecaoService;
    }

    @GetMapping
    public Set<SubTipoProtecao> listarTodosTipoProtecao() {
        return subTipoProtecaoService.listarTodosTipoProtecao();
    }

    @GetMapping("/filtrados")
    public ResponseEntity<?> listarTodosTiposProtecaoFiltrados(@RequestParam Map<String, String> queryParameters
            , Pageable pageable) {
        return new ResponseEntity<>(subTipoProtecaoService.findAllDTO(queryParameters, pageable), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SubTipoProtecao> buscarPorId(@PathVariable Long id) {
        Optional<SubTipoProtecao> tipoProtecao = subTipoProtecaoService.buscarPorId(id);
        return tipoProtecao.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public SubTipoProtecao salvar(@RequestBody SubTipoProtecao subTipoProtecao) {
        return subTipoProtecaoService.salvar(subTipoProtecao);
    }
    @PutMapping("/{id}")
    public ResponseEntity<SubTipoProtecao> atualizar(@PathVariable Long id, @RequestBody SubTipoProtecao subTipoProtecaoAtualizado) {
        Optional<SubTipoProtecao> tipoProtecao = subTipoProtecaoService.buscarPorId(id);
        if (tipoProtecao.isPresent()) {
            subTipoProtecaoAtualizado.setId(tipoProtecao.get().getId());
            subTipoProtecaoService.atualizar(subTipoProtecaoAtualizado);
            return ResponseEntity.ok(subTipoProtecaoAtualizado);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> excluirTipoProtecao(@PathVariable("id") Long id) {
        subTipoProtecaoService.excluirTipoProtecao(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("vincular-normas-tecnicas")
    public ResponseEntity<SubTipoProtecao> vincularNormasTecnicas(@RequestBody VincularNormasTecnicasRequest body) {

        SubTipoProtecao novoSubTipoProtecao = subTipoProtecaoService.vincularNormaTecnica(body.getEntityId(), body.getNormasTecnicas());

        return ResponseEntity.status(HttpStatus.OK).body(novoSubTipoProtecao);
    }

    @RequestMapping(value="/report-excel", method = RequestMethod.GET)
    public ResponseEntity<?> reportExcel(@RequestParam Map<String, String> queryParameters, final HttpServletRequest request,
                                         final HttpServletResponse response) throws Exception{

        ByteArrayResource relatorio_excel = subTipoProtecaoService.reportExcel(queryParameters, request, response);
        return new ResponseEntity<> (relatorio_excel, HttpStatus.OK);
    }
}
