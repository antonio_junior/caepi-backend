package br.com.caepi.controller;

import br.com.caepi.entities.NivelDesempenho;
import br.com.caepi.service.NivelDesempenhoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/niveis-desempenhos")
public class NivelDesempenhoController {

    @Autowired
    private NivelDesempenhoService nivelDesempenhoService;

    @PostMapping
    public NivelDesempenho salvar(@RequestBody NivelDesempenho nivelDesempenho) {
        return nivelDesempenhoService.salvar(nivelDesempenho);
    }

    @PutMapping("/{id}")
    public ResponseEntity<NivelDesempenho> atualizar(@PathVariable Long id, @RequestBody NivelDesempenho nivelDesempenhoAtualizada) {
        Optional<NivelDesempenho> nivelDesempenho = nivelDesempenhoService.buscarPorId(id);
        if (nivelDesempenho.isPresent()) {
            nivelDesempenhoAtualizada.setId(nivelDesempenho.get().getId());
            return ResponseEntity.ok(nivelDesempenhoService.atualizar(nivelDesempenhoAtualizada));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/importar")
    public ResponseEntity<List<NivelDesempenho>> importarNiveisDeDesempenho(@RequestParam Long normaTecnicaOrigemId, @RequestParam Long normaTecnicaDestinoId) {
        List<NivelDesempenho> niveisDesempenhoImportados = nivelDesempenhoService.importarNiveisDeDesempenho(normaTecnicaOrigemId, normaTecnicaDestinoId);
        return ResponseEntity.ok(niveisDesempenhoImportados);
    }

    @GetMapping("/{id}")
    public ResponseEntity<NivelDesempenho> buscarPorId(@PathVariable Long id) {
        Optional<NivelDesempenho> nivelDesempenho = nivelDesempenhoService.buscarPorId(id);
        return nivelDesempenho.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> excluirNivelDesempenho(@PathVariable("id") Long id) {
        try {
            nivelDesempenhoService.excluir(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
