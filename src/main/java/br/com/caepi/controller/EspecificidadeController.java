package br.com.caepi.controller;

import br.com.caepi.entities.Especificidade;
import br.com.caepi.service.EspecificidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/especificidades")
public class EspecificidadeController {

    @Autowired
    private EspecificidadeService especificidadeService;

    @PostMapping
    public Especificidade salvar(@RequestBody Especificidade especificidade) {
        return especificidadeService.salvar(especificidade);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Especificidade> atualizar(@PathVariable Long id, @RequestBody Especificidade especificidadeAtualizada) {
        Optional<Especificidade> especificidade = especificidadeService.buscarPorId(id);
        if (especificidade.isPresent()) {
            especificidadeAtualizada.setId(especificidade.get().getId());
            especificidadeAtualizada.setTipoEpi(especificidade.get().getTipoEpi());
            return ResponseEntity.ok(especificidadeService.atualizar(especificidadeAtualizada));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Especificidade> buscarPorId(@PathVariable Long id) {
        Optional<Especificidade> especificidade = especificidadeService.buscarPorId(id);
        return especificidade.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> excluirEspecificidade(@PathVariable("id") Long id) {
        try {
            especificidadeService.excluir(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
