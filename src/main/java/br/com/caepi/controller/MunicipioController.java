package br.com.caepi.controller;

import br.com.caepi.entities.Municipio;
import br.com.caepi.service.MunicipioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/municipios")
public class MunicipioController {
	
	@Autowired
	private MunicipioService municipioService;

	@GetMapping("/list")
	public List<Municipio> listMunicipio(@RequestParam("estadoId") Long estadoId) {
		return municipioService.findByEstado(estadoId);
	}
}
