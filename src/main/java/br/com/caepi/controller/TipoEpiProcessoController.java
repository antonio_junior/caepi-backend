package br.com.caepi.controller;

import br.com.caepi.entities.TipoEpiProcesso;
import br.com.caepi.service.TipoEpiProcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tipo-epi-processo")
public class TipoEpiProcessoController {

    @Autowired
    private TipoEpiProcessoService tipoEpiProcessoService;

    @PostMapping("/{tipoEpiProcessoId}/add-laboratorio/{laboratorioId}")
    public ResponseEntity<TipoEpiProcesso> addLaboratorio(
            @PathVariable Long tipoEpiProcessoId,
            @PathVariable Long laboratorioId) {
        TipoEpiProcesso updatedTipoEpiProcesso = tipoEpiProcessoService.addLaboratorio(tipoEpiProcessoId, laboratorioId);
        if (updatedTipoEpiProcesso != null) {
            return ResponseEntity.ok(updatedTipoEpiProcesso);
        }
        return ResponseEntity.badRequest().build();
    }

    @DeleteMapping("/{tipoEpiProcessoId}/remove-laboratorio/{laboratorioId}")
    public ResponseEntity<TipoEpiProcesso> removeLaboratorio(
            @PathVariable Long tipoEpiProcessoId,
            @PathVariable Long laboratorioId) {
        TipoEpiProcesso updatedTipoEpiProcesso = tipoEpiProcessoService.removeLaboratorio(tipoEpiProcessoId, laboratorioId);
        if (updatedTipoEpiProcesso != null) {
            return ResponseEntity.ok(updatedTipoEpiProcesso);
        }
        return ResponseEntity.badRequest().build();
    }
}