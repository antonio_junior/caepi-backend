package br.com.caepi.controller;

import br.com.caepi.DTOS.AvancarEtapaLaboratorioOcpDTO;
import br.com.caepi.DTOS.IniciarEspecificidadesGeraisEpi;
import br.com.caepi.DTOS.IniciarProtecaoEpi;
import br.com.caepi.entities.EspecificidadeProcesso;
import br.com.caepi.entities.Processo;
import br.com.caepi.service.ProcessoService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/laboratorio-ocp/processos")
public class ProcessoLaboratorioOPCController {

    private final ProcessoService processoService;

    public ProcessoLaboratorioOPCController(ProcessoService processoService) {
        this.processoService = processoService;
    }

    @GetMapping
    public Set<Processo> listarTodosProcesso() {
        return processoService.listarTodosProcesso();
    }

    @GetMapping("/filtrados")
    public ResponseEntity<?> listarTodosProcessosFiltrados(@RequestParam Map<String, String> queryParameters
            , Pageable pageable) {
        return new ResponseEntity<>(processoService.findAll(queryParameters, pageable), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Processo> buscarPorId(@PathVariable Long id) {
        return processoService.buscarPorId(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("/iniciar-especificidades-gerais-epi")
    public ResponseEntity<?> iniciarEspecificidadesGeraisEpi(@RequestBody IniciarEspecificidadesGeraisEpi data) {
        try {
            processoService.iniciarEspecificidadesGeraisEpiProcesso(data);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PutMapping("/salvar-especificidades-gerais-epi/{id}")
    public ResponseEntity<?> salvarEspecificidadesGeraisEpi(@PathVariable Long id, @RequestBody Set<EspecificidadeProcesso> data) {
        try {
            processoService.salvarEspecificidadesGeraisEpiProcesso(id, data);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PutMapping("/finalizar-especificidades-gerais-epi/{id}")
    public ResponseEntity<?> finalizarEspecificidadesGeraisEpi(@PathVariable Long id, @RequestBody Set<EspecificidadeProcesso> data) {
        try {
            processoService.finalizarEspecificidadesGeraisEpiProcesso(id, data);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PutMapping("/reiniciar-especificidades-gerais-epi/{id}")
    public ResponseEntity<?> reiniciarEspecificidadesGeraisEpi(@PathVariable Long id) {
        try {
            processoService.reiniciarEspecificidadesGeraisEpiProcesso(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PostMapping("/iniciar-protecao-epi")
    public ResponseEntity<?> iniciarProtecaoEpi(@RequestBody IniciarProtecaoEpi data) {
        try {
            processoService.iniciarProtecaoEpiProcesso(data);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PutMapping("/salvar-protecao-epi/{id}")
    public ResponseEntity<?> salvarProtecaoEpi(@PathVariable Long id, @RequestBody Set<EspecificidadeProcesso> data) {
        try {
            processoService.salvarProtecaoEpiProcesso(id, data);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PutMapping("/finalizar-protecao-epi/{id}")
    public ResponseEntity<?> finalizarProtecaoEpi(@PathVariable Long id, @RequestBody Set<EspecificidadeProcesso> data) {
        try {
            processoService.finalizarProtecaoEpiProcesso(id, data);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PutMapping("/reiniciar-protecao-epi/{id}")
    public ResponseEntity<?> reiniciarProtecaoEpi(@PathVariable Long id) {
        try {
            processoService.reiniciarProtecaoEpiProcesso(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PutMapping("/avancar-etapa/{id}")
    public ResponseEntity<Processo> avancarEtapa(@PathVariable Long id, @RequestBody AvancarEtapaLaboratorioOcpDTO data) {
        return processoService.buscarPorId(id)
                .map(processo -> {
                    return ResponseEntity.ok(processoService.avancarEtapaLaboratorioOCP(data));
                })
                .orElse(ResponseEntity.notFound().build());
    }
}
