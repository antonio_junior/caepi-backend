package br.com.caepi.controller;

import br.com.caepi.service.NivelDesempenhoRegraExibicaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/niveis-desempenhos-regra-exibicao")
public class NivelDesempenhoRegraExibicaoController {

    @Autowired
    private NivelDesempenhoRegraExibicaoService nivelDesempenhoRegraExibicaoService;

    @DeleteMapping("/{id}")
    public ResponseEntity<String> excluirNivelDesempenhoRegraExibicao(@PathVariable("id") Long id) {
        try {
            nivelDesempenhoRegraExibicaoService.excluir(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
