package br.com.caepi.controller;

import br.com.caepi.DTOS.IniciarEspecificidadesGeraisEpi;
import br.com.caepi.DTOS.IniciarProtecaoEpi;
import br.com.caepi.entities.Processo;
import br.com.caepi.service.ProcessoService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/processos")
public class ProcessoController {

    private final ProcessoService processoService;

    public ProcessoController(ProcessoService processoService) {
        this.processoService = processoService;
    }

    @GetMapping
    public Set<Processo> listarTodosProcesso() {
        return processoService.listarTodosProcesso();
    }

    @GetMapping("/filtrados")
    public ResponseEntity<?> listarTodosProcessosFiltrados(@RequestParam Map<String, String> queryParameters
            , Pageable pageable) {
        return new ResponseEntity<>(processoService.findAll(queryParameters, pageable), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Processo> buscarPorId(@PathVariable Long id) {
        return processoService.buscarPorId(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletar(@PathVariable Long id) {
        processoService.deletar(id);
        return ResponseEntity.ok().build();
    }


    @PostMapping("/resetar-protecao-epi")
    public ResponseEntity<?> resetarProtecaoEpi(@RequestBody IniciarProtecaoEpi data) {
        try {
            processoService.resetarProtecaoEpiProcesso(data);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

}
