package br.com.caepi.controller;

import br.com.caepi.DTOS.TipoEpiDTO;
import br.com.caepi.entities.TipoEpi;
import br.com.caepi.service.TipoEpiService;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/tipos-epi")
public class TipoEpiController {
    private final TipoEpiService tipoEpiService;

    public TipoEpiController(TipoEpiService tipoEpiService) {
        this.tipoEpiService = tipoEpiService;
    }

    @GetMapping
    public Set<TipoEpi> listarTodosTipoEpi() {
        return tipoEpiService.listarTodosTipoEpi();
    }

    @GetMapping("/filtrados")
    public ResponseEntity<?> listarTodosTiposEpisFiltrados(@RequestParam Map<String, String> queryParameters
            , Pageable pageable) {
        return new ResponseEntity<>(tipoEpiService.findAll(queryParameters, pageable), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TipoEpi> buscarPorId(@PathVariable Long id) {
        Optional<TipoEpi> tipoEpi = tipoEpiService.buscarPorId(id);
        return tipoEpi.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public TipoEpi salvar(@RequestBody TipoEpi tipoEpi) {
        return tipoEpiService.salvar(tipoEpi);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TipoEpi> atualizar(@PathVariable Long id, @RequestBody TipoEpi tipoEpiAtualizado) {
        Optional<TipoEpi> tipoEpi = tipoEpiService.buscarPorId(id);
        if (tipoEpi.isPresent()) {
            tipoEpiAtualizado.setId(tipoEpi.get().getId());
            tipoEpiService.atualizar(tipoEpiAtualizado);
            return ResponseEntity.ok(tipoEpiAtualizado);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/conjugados/{id}")
    public ResponseEntity<TipoEpiDTO> atualizarEpiConjugado(@PathVariable Long id, @RequestBody TipoEpiDTO tipoEpiAtualizado) {
        Optional<TipoEpi> tipoEpi = tipoEpiService.buscarPorId(id);
        if (tipoEpi.isPresent()) {
            tipoEpiAtualizado.setId(tipoEpi.get().getId());
            tipoEpiService.atualizarEpiConjugado(tipoEpiAtualizado);
            return ResponseEntity.ok(tipoEpiAtualizado);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> excluirTipoEpi(@PathVariable("id") Long id) {
        tipoEpiService.excluirTipoEpi(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @RequestMapping(value="/report-excel", method = RequestMethod.GET)
    public ResponseEntity<?> reportExcel(@RequestParam Map<String, String> queryParameters, final HttpServletRequest request,
                                         final HttpServletResponse response) throws Exception{

        ByteArrayResource relatorio_excel = tipoEpiService.reportExcel(queryParameters, request, response);
        return new ResponseEntity<> (relatorio_excel, HttpStatus.OK);
    }
}