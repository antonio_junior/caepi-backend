package br.com.caepi.controller;

import br.com.caepi.entities.NormaTecnica;
import br.com.caepi.service.NormaTecnicaService;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/normas-tecnicas")
public class NormaTecnicaController {
    private final NormaTecnicaService normaTecnicaService;

    public NormaTecnicaController(NormaTecnicaService normaTecnicaService) {
        this.normaTecnicaService = normaTecnicaService;
    }

    @GetMapping
    public Set<NormaTecnica> listarTodasNormaTecnica() {
        return normaTecnicaService.listarTodasNormaTecnica();
    }

    @GetMapping("/filtrados")
    public ResponseEntity<?> listarTodasNormasTecnicasFiltradas(@RequestParam Map<String, String> queryParameters
            , Pageable pageable) {
        return new ResponseEntity<>(normaTecnicaService.findAll(queryParameters, pageable), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<NormaTecnica> buscarPorId(@PathVariable Long id) {
        Optional<NormaTecnica> normaTecnica = normaTecnicaService.buscarPorId(id);
        return normaTecnica.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public NormaTecnica salvar(@RequestBody NormaTecnica normaTecnica) {
        return normaTecnicaService.salvar(normaTecnica);
    }

    @PutMapping("/{id}")
    public ResponseEntity<NormaTecnica> atualizar(@PathVariable Long id, @RequestBody NormaTecnica normaTecnicaAtualizada) {
        Optional<NormaTecnica> normaTecnica = normaTecnicaService.buscarPorId(id);
        if (normaTecnica.isPresent()) {
            normaTecnicaAtualizada.setId(normaTecnica.get().getId());
            normaTecnicaService.atualizar(normaTecnicaAtualizada);
            return ResponseEntity.ok(normaTecnicaAtualizada);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> excluirNormaTecnica(@PathVariable("id") Long id) {
        try {
            normaTecnicaService.excluirNormaTecnica(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @RequestMapping(value="/report-excel", method = RequestMethod.GET)
    public ResponseEntity<?> reportExcel(@RequestParam Map<String, String> queryParameters, final HttpServletRequest request,
                                         final HttpServletResponse response) throws Exception{

        ByteArrayResource relatorio_excel = normaTecnicaService.reportExcel(queryParameters, request, response);
        return new ResponseEntity<> (relatorio_excel, HttpStatus.OK);
    }
}