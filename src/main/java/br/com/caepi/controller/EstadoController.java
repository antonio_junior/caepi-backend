package br.com.caepi.controller;

import br.com.caepi.entities.Estado;
import br.com.caepi.service.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/estados")
public class EstadoController {

    @Autowired
    private EstadoService estadoService;

    @RequestMapping(value="/list", method = RequestMethod.GET)
    public List<Estado> listEstado() {
        return estadoService.findAll();
    }
}