CREATE TABLE public.sub_tipo_protecao_norma_tecnica (
    sub_tipo_protecao_id int8 NOT NULL,
    norma_tecnica_id int8 NOT NULL,
    CONSTRAINT fk3083md8301idfx80t3ffkf8qq FOREIGN KEY (norma_tecnica_id) REFERENCES public.norma_tecnica(id),
    CONSTRAINT fkfe06dy3p40d03i3o76oy62aqg FOREIGN KEY (sub_tipo_protecao_id) REFERENCES public.sub_tipo_protecao(id)
);