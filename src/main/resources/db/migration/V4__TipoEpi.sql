CREATE TABLE public.tipo_epi (
                                 id bigserial NOT NULL,
                                 certificado_de_conformidade bool NULL,
                                 conjugado bool NULL,
                                 data_cadastro timestamp NULL,
                                 data_revogacao timestamp NULL,
                                 descricao varchar(255) NULL,
                                 ensaio_no_exterior bool NULL,
                                 texto_protecao varchar(255) NULL,
                                 titulo varchar(255) NULL,
                                 CONSTRAINT tipo_epi_pkey PRIMARY KEY (id)
);

INSERT INTO public.tipo_epi (certificado_de_conformidade,conjugado,data_cadastro,data_revogacao,descricao,ensaio_no_exterior,texto_protecao,titulo) VALUES
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção do antebraço do usuário contra:','BRAÇADEIRA'),
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção das pernas do usuário contra:','CALÇA'),
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção dos pés do usuário contra:','CALÇADO'),
                                                                                                                                                        (false,false,'2023-07-12 21:00:00',NULL,NULL,false,'Proteção do crânio e pescoço do usuário contra:','CAPUZ OU BALACLAVA'),
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção dos membros superiores do usuário contra:','CREME PROTETOR'),
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção dos dedos do usuário contra:','DEDEIRA'),
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção das mãos do usuário contra:','LUVA'),
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção do tronco e membros superiores e inferiores do usuário contra:','MACACÃO DE SEGURANÇA'),
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção do braço e antebraço do usuário contra:','MANGA'),
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção dos olhos e face do usuário contra:','MÁSCARA DE SOLDA');
INSERT INTO public.tipo_epi (certificado_de_conformidade,conjugado,data_cadastro,data_revogacao,descricao,ensaio_no_exterior,texto_protecao,titulo) VALUES
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção dos olhos do usuário contra:','ÓCULOS'),
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção dos olhos do usuário contra:','ÓCULOS DE TELA'),
                                                                                                                                                        (false,false,'2023-07-14 21:00:00',NULL,NULL,false,'Proteção das pernas do usuário contra:','PERNEIRA'),
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção do sistema auditivo do usuário contra:','PROTETOR AUDITIVO'),
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção dos olhos e face do usuário contra:','PROTETOR FACIAL'),
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção das vias respiratórias do usuário em:','RESPIRADOR DE ADUÇÃO DE AR TIPO LINHA DE AR COMPRIMIDO'),
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção das vias respiratórias do usuário em:','RESPIRADOR DE ADUÇÃO DE AR TIPO MÁSCARA AUTÔNOMA'),
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção das vias respiratórias do usuário contra:','RESPIRADOR PURIFICADOR DE AR NÃO MOTORIZADO'),
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção do crânio, pescoço, tronco, membros superiores e inferiores do usuário contra:','VESTIMENTA DE CORPO INTEIRO'),
                                                                                                                                                        (false,false,'2023-07-16 00:00:00',NULL,NULL,false,'Proteção do tronco e/ou dos membros superiores do usuário contra:','VESTIMENTA PARA PROTEÇÃO DO TRONCO');
