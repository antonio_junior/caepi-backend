ALTER TABLE public.processo ADD COLUMN IF NOT EXISTS autor_id int8 NULL;

ALTER TABLE public.processo ADD CONSTRAINT fk_processo_fabricante FOREIGN KEY (autor_id) REFERENCES public.empresa(id);