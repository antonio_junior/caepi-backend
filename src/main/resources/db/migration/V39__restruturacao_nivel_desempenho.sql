TRUNCATE TABLE public.nivel_desempenho CASCADE;

ALTER TABLE public.nivel_desempenho ADD COLUMN IF NOT EXISTS tipo varchar(255) NULL;
ALTER TABLE public.nivel_desempenho ADD COLUMN IF NOT EXISTS informacao TEXT NULL;
ALTER TABLE public.nivel_desempenho ADD COLUMN IF NOT EXISTS texto_ilimitado VARCHAR(10) NULL;
ALTER TABLE public.nivel_desempenho ADD COLUMN IF NOT EXISTS num_caracteres int8 NULL;
ALTER TABLE public.nivel_desempenho ADD COLUMN IF NOT EXISTS casas_decimais int8 NULL;
ALTER TABLE public.nivel_desempenho ADD COLUMN IF NOT EXISTS ilimitado VARCHAR(10) NULL;
ALTER TABLE public.nivel_desempenho ADD COLUMN IF NOT EXISTS vlrmin VARCHAR(255) NULL;
ALTER TABLE public.nivel_desempenho ADD COLUMN IF NOT EXISTS vlrmax VARCHAR(255) NULL;