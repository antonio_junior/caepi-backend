ALTER TABLE public.tipo_epi ALTER COLUMN descricao TYPE text;
ALTER TABLE public.tipo_epi ALTER COLUMN texto_protecao TYPE text;

ALTER TABLE public.tipo_protecao ALTER COLUMN descricao TYPE text;

ALTER TABLE public.sub_tipo_protecao ALTER COLUMN descricao TYPE text;

ALTER TABLE public.norma_tecnica ALTER COLUMN descricao TYPE text;