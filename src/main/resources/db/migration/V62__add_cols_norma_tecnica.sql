ALTER TABLE public.norma_tecnica ADD COLUMN IF NOT EXISTS observacao_niveis_desempenhos text NULL;
ALTER TABLE public.norma_tecnica ADD COLUMN IF NOT EXISTS pictograma bool NULL;

ALTER TABLE public.norma_tecnica_processo ADD COLUMN IF NOT EXISTS observacao_niveis_desempenhos text NULL;
ALTER TABLE public.norma_tecnica_processo ADD COLUMN IF NOT EXISTS pictograma bool NULL;