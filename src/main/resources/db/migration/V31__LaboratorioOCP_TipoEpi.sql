CREATE TABLE IF NOT EXISTS public.laboratorio_ocp_tipo_epi
(
    laboratorio_ocp_id int8 NOT NULL,
    tipo_epi_id int8 NOT NULL,
    CONSTRAINT laboratorio_ocp_tipo_epi_pkey PRIMARY KEY (laboratorio_ocp_id, tipo_epi_id),
    CONSTRAINT fk4a5vybn1s9ugma1ox1s96a31g FOREIGN KEY (tipo_epi_id) REFERENCES public.tipo_epi (id),
    CONSTRAINT fk71umkne92m32n7hjk1qfi96qf FOREIGN KEY (laboratorio_ocp_id) REFERENCES public.laboratorio_ocp (id)
)