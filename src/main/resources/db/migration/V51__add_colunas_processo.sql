ALTER TABLE public.processo ADD COLUMN IF NOT EXISTS etapa_avaliacao int4 NULL;
ALTER TABLE public.processo ADD COLUMN IF NOT EXISTS data_realizacao_etapa1 timestamp NULL;
ALTER TABLE public.processo ADD COLUMN IF NOT EXISTS data_realizacao_etapa2 timestamp NULL;
ALTER TABLE public.processo ADD COLUMN IF NOT EXISTS data_realizacao_etapa3 timestamp NULL;
ALTER TABLE public.processo ADD COLUMN IF NOT EXISTS data_avaliacao_etapa1 timestamp NULL;
ALTER TABLE public.processo ADD COLUMN IF NOT EXISTS data_avaliacao_etapa2 timestamp NULL;
ALTER TABLE public.processo ADD COLUMN IF NOT EXISTS data_avaliacao_etapa3 timestamp NULL;