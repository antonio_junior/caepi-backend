CREATE TABLE public.especificidade_processo (
  id bigserial NOT NULL,
  titulo varchar(255) NULL,
  tipo varchar(255) NULL,
  ordem int4 NULL,
  tipo_epi_processo_id int8 NULL,
  id_origem int8 NOT NULL,
  resposta varchar(255) NULL,

  CONSTRAINT especificidade_processo_pkey PRIMARY KEY (id),
  CONSTRAINT FK_tipo_epi_processo_especificidade_processo FOREIGN KEY (tipo_epi_processo_id) REFERENCES public.tipo_epi_processo(id)
);


CREATE TABLE public.especificidade_opcao_processo (
  id bigserial NOT NULL,
  descricao varchar(255) NULL,
  indicacao_de_uso text NULL,
  selecionado bool NULL,
  ordem int4 NULL,
  especificidade_processo_id int8 NULL,
  id_origem int8 NOT NULL,

  CONSTRAINT especificidade_opcao_processo_pkey PRIMARY KEY (id),
  CONSTRAINT FK_especificidade_processo FOREIGN KEY (especificidade_processo_id) REFERENCES public.especificidade_processo(id)
);