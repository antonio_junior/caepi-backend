ALTER TABLE public.tipo_epi_processo ADD COLUMN IF NOT EXISTS data_inicio_especificidades_gerais timestamp NULL;
ALTER TABLE public.tipo_epi_processo ADD COLUMN IF NOT EXISTS data_fim_especificidades_gerais timestamp NULL;
ALTER TABLE public.tipo_epi_processo ADD COLUMN IF NOT EXISTS autor_especificidades_gerais_id int8 NULL;
ALTER TABLE public.tipo_epi_processo ADD CONSTRAINT fk_tipoEpiProcesso_laboratorioOcp FOREIGN KEY (autor_especificidades_gerais_id) REFERENCES public.empresa(id);

ALTER TABLE public.protecao_processo ADD COLUMN IF NOT EXISTS data_inicio timestamp NULL;
ALTER TABLE public.protecao_processo ADD COLUMN IF NOT EXISTS data_fim timestamp NULL;
ALTER TABLE public.protecao_processo ADD COLUMN IF NOT EXISTS autor_id int8 NULL;
ALTER TABLE public.protecao_processo ADD CONSTRAINT fk_protecaoProcesso_laboratorioOcp FOREIGN KEY (autor_id) REFERENCES public.empresa(id);