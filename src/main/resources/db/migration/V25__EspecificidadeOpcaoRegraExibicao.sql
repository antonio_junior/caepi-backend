CREATE TABLE IF NOT EXISTS public.especificidade_opcao_regra_exibicao (
      id bigserial NOT NULL,
      especificidade_opcao_id int8 NULL,
      especificidade_id int8 NULL,
      CONSTRAINT especificidade_opcao_regra_exibicao_pkey PRIMARY KEY (id),
      CONSTRAINT especificidade_opcao_regra_exibicao FOREIGN KEY (especificidade_opcao_id) REFERENCES public.especificidade_opcao(id),
      CONSTRAINT especificidade_regra_exibicao FOREIGN KEY (especificidade_id) REFERENCES public.especificidade(id)
);

CREATE TABLE IF NOT EXISTS public.regras_especificidades_opcoes (
      regra_id int8 NOT NULL,
      regras_especificidades_opcoes int8 NULL,
      CONSTRAINT fkrs40dsajg4wvoq55m953i1q0lewd FOREIGN KEY (regra_id) REFERENCES public.especificidade_opcao_regra_exibicao(id)
);