ALTER TABLE public.nivel_desempenho_regra_exibicao ADD COLUMN IF NOT EXISTS item_identificador int8 NULL;

ALTER TABLE public.nivel_desempenho_regra_exibicao ADD COLUMN temp_identificador bigint;

UPDATE public.nivel_desempenho_regra_exibicao
SET temp_identificador = NULLIF(regexp_replace(campo_identificador, '\D','','g'), '')::bigint;

ALTER TABLE public.nivel_desempenho_regra_exibicao DROP COLUMN campo_identificador;

ALTER TABLE public.nivel_desempenho_regra_exibicao RENAME COLUMN temp_identificador TO campo_identificador;

