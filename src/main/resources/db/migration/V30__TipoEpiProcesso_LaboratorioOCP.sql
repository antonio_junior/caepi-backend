CREATE TABLE public.tipo_epi_processo_laboratorios_ocp (
     tipo_epi_processo_id int8 NOT NULL,
     laboratorio_ocp_id int8 NOT NULL,
     CONSTRAINT tipo_epi_processo_laboratorios_ocp_pkey PRIMARY KEY (tipo_epi_processo_id, laboratorio_ocp_id),
     CONSTRAINT fk4796ff39tj1d8vkef5g444uc7 FOREIGN KEY (tipo_epi_processo_id) REFERENCES public.tipo_epi_processo(id),
     CONSTRAINT fkip8wkfuemsigh5454443drgih FOREIGN KEY (laboratorio_ocp_id) REFERENCES public.laboratorio_ocp(id)
);