ALTER TABLE arquivo_processo
    RENAME COLUMN responsavel_upload TO empresa_id;

ALTER TABLE arquivo_processo
    ALTER COLUMN empresa_id TYPE BIGINT USING empresa_id::bigint;

ALTER TABLE arquivo_processo
    ADD CONSTRAINT fk_arquivo_processo_empresa
    FOREIGN KEY (empresa_id)
    REFERENCES empresa(id);

ALTER TABLE especificidade
    ADD COLUMN assinado_digitalmente boolean NOT NULL DEFAULT false;