CREATE TABLE public.protecao_base (
                                      id bigserial NOT NULL,
                                      data_cadastro timestamp NULL,
                                      descricao text NULL,
                                      nome varchar(255) NULL,
                                      status varchar(255) NULL,
                                      tipo varchar(255) NULL,
                                      tipo_protecao_id int8 NULL,

                                      CONSTRAINT protecao_base_pkey PRIMARY KEY (id),
                                      CONSTRAINT tipo_protecao_sub_tipo_protecao FOREIGN KEY (tipo_protecao_id) REFERENCES public.protecao_base(id)
);

INSERT INTO public.protecao_base (id, data_cadastro, descricao, nome, status, tipo, tipo_protecao_id) VALUES
                                                                                                          (1,'2023-07-16 00:00:00',NULL,'Aerodispersoides',NULL,'TipoProtecao',NULL),
                                                                                                          (10,'2023-07-16 00:00:00',NULL,'Gases e vapores',NULL,'TipoProtecao',NULL),
                                                                                                          (17,'2023-07-16 00:00:00',NULL,'Umidade',NULL,'TipoProtecao',NULL),
                                                                                                          (11,'2023-07-16 00:00:00',NULL,'Agentes biológicos',NULL,'TipoProtecao',NULL),
                                                                                                          (14,'2023-07-16 00:00:00',NULL,'Agentes químicos',NULL,'TipoProtecao',NULL),
                                                                                                          (16,'2023-07-16 00:00:00',NULL,'Agentes térmicos',NULL,'TipoProtecao',NULL),
                                                                                                          (15,'2023-07-16 00:00:00',NULL,'Radiação ionizante',NULL,'TipoProtecao',NULL),
                                                                                                          (23,'2023-07-16 00:00:00',NULL,'Agentes químicos (agrotóxicos)',NULL,'TipoProtecao',NULL),
                                                                                                          (26,'2023-07-16 00:00:00',NULL,'Impacto de objeto sobre o crânio',NULL,'TipoProtecao',NULL),
                                                                                                          (9,'2023-07-16 00:00:00',NULL,'Choque elétrico',NULL,'TipoProtecao',NULL),
                                                                                                          (12,'2023-07-16 00:00:00',NULL,'Agentes mecânicos',NULL,'TipoProtecao',NULL),
                                                                                                          (27,'2023-07-16 00:00:00',NULL,'Impacto de partículas volantes',NULL,'TipoProtecao',NULL),
                                                                                                          (28,'2023-07-16 00:00:00',NULL,'Luminosidade intensa',NULL,'TipoProtecao',NULL),
                                                                                                          (29,'2023-07-16 00:00:00',NULL,'Radiação ultravioleta',NULL,'TipoProtecao',NULL),
                                                                                                          (30,'2023-07-16 00:00:00',NULL,'Radiação infravermelha',NULL,'TipoProtecao',NULL),
                                                                                                          (31,'2023-07-16 00:00:00',NULL,'Níveis de pressão sonora superiores ao estabelecido na NR-15, Anexos I e II',NULL,'TipoProtecao',NULL),
                                                                                                          (3,'2023-07-16 00:00:00',NULL,'Atmosferas com concentração de oxigênio maior que 12,5% ao nível do mar',NULL,'TipoProtecao',NULL),
                                                                                                          (4,'2023-07-16 00:00:00',NULL,'Atmosferas imediatamente perigosas à vida e à saúde (IPVS)',NULL,'TipoProtecao',NULL),
                                                                                                          (32,'2023-07-16 00:00:00',NULL,'Vibração',NULL,'TipoProtecao',NULL),
                                                                                                          (5,'2023-07-16 00:00:00',NULL,'de Proteção',NULL,'TipoProtecao',NULL),
                                                                                                          (6,'2023-07-16 00:00:00',NULL,'de Segurança',NULL,'TipoProtecao',NULL),
                                                                                                          (7,'2023-07-16 00:00:00',NULL,'Ocupacional',NULL,'TipoProtecao',NULL),
                                                                                                          (8,'2023-07-16 00:00:00',NULL,'Para trabalho ao potencial',NULL,'TipoProtecao',NULL),
                                                                                                          (24,'2023-07-16 00:00:00',NULL,'Queda com diferença de nível',NULL,'TipoProtecao',NULL),
                                                                                                          (100,'2023-07-16 00:00:00',NULL,'Choque elétrico (Classe I)',NULL,'SubTipoProtecao',6),
                                                                                                          (50,'2023-07-16 00:00:00',NULL,'Frio (temperatura acima de -5 ºC)',NULL,'SubTipoProtecao',16),
                                                                                                          (49,'2023-07-16 00:00:00',NULL,'Frio (temperatura igual ou abaixo de -5 ºC)',NULL,'SubTipoProtecao',16),
                                                                                                          (70,'2023-07-16 00:00:00',NULL,'Cortantes e perfurantes provocados por facas manuais e objetos cortantes similares',NULL,'SubTipoProtecao',12),
                                                                                                          (71,'2023-07-16 00:00:00',NULL,'Corte manual de cana-de-açúcar',NULL,'SubTipoProtecao',12),
                                                                                                          (72,'2023-07-16 00:00:00',NULL,'Cortes por impacto provocado por facas manuais',NULL,'SubTipoProtecao',12),
                                                                                                          (73,'2023-07-16 00:00:00',NULL,'Impacto de projéteis de armas de fogo',NULL,'SubTipoProtecao',12),
                                                                                                          (74,'2023-07-16 00:00:00',NULL,'Impacto de projéteis de armas de fogo e perfurocortantes',NULL,'SubTipoProtecao',12),
                                                                                                          (75,'2023-07-16 00:00:00',NULL,'Motosserra',NULL,'SubTipoProtecao',12),
                                                                                                          (101,'2023-07-16 00:00:00',NULL,'Choque elétrico (Classe II)',NULL,'SubTipoProtecao',6),
                                                                                                          (77,'2023-07-16 00:00:00',NULL,'Proveniente de operações com uso de água',NULL,'SubTipoProtecao',17),
                                                                                                          (76,'2023-07-16 00:00:00',NULL,'Proveniente de precipitação pluviométrica',NULL,'SubTipoProtecao',17),
                                                                                                          (78,'2023-07-16 00:00:00',NULL,'Poeiras e névoas',NULL,'SubTipoProtecao',1),
                                                                                                          (79,'2023-07-16 00:00:00',NULL,'Poeiras, névoas e fumos',NULL,'SubTipoProtecao',1),
                                                                                                          (80,'2023-07-16 00:00:00',NULL,'Poeiras, névoas, fumos e radionuclídeos',NULL,'SubTipoProtecao',1),
                                                                                                          (81,'2023-07-16 00:00:00',NULL,'Impactos de quedas de objetos sobre os artelhos',NULL,'SubTipoProtecao',5),
                                                                                                          (82,'2023-07-16 00:00:00',NULL,'Choque elétrico (Classe I)',NULL,'SubTipoProtecao',5),
                                                                                                          (83,'2023-07-16 00:00:00',NULL,'Choque elétrico (Classe II)',NULL,'NULL',5),
                                                                                                          (84,'2023-07-16 00:00:00',NULL,'Umidade proveniente de operações com uso de água (Classe II)',NULL,'SubTipoProtecao',5),
                                                                                                          (102,'2023-07-16 00:00:00',NULL,'Choque elétrico',NULL,'SubTipoProtecao',8),
                                                                                                          (85,'2023-07-16 00:00:00',NULL,'Agentes químicos',NULL,'SubTipoProtecao',5),
                                                                                                          (103,'2023-07-16 00:00:00',NULL,'Queda de um nível a outro em trabalhos em altura',NULL,'SubTipoProtecao',24),
                                                                                                          (43,'2023-07-16 00:00:00',NULL,'Arco elétrico',NULL,'SubTipoProtecao',16),
                                                                                                          (41,'2023-07-16 00:00:00',NULL,'Calor e chamas',NULL,'SubTipoProtecao',16),
                                                                                                          (47,'2023-07-16 00:00:00',NULL,'Combate a incêndio de estruturas',NULL,'SubTipoProtecao',16),
                                                                                                          (48,'2023-07-16 00:00:00',NULL,'Combate a incêndio florestal',NULL,'SubTipoProtecao',16),
                                                                                                          (46,'2023-07-16 00:00:00',NULL,'Fogo repentino',NULL,'SubTipoProtecao',16),
                                                                                                          (89,'2023-07-16 00:00:00',NULL,'Agentes térmicos - combate a incêndio',NULL,'SubTipoProtecao',6),
                                                                                                          (42,'2023-07-16 00:00:00',NULL,'Soldagem ou processos similares',NULL,'SubTipoProtecao',16),
                                                                                                          (44,'2023-07-16 00:00:00',NULL,'Combate a incêndio',NULL,'SubTipoProtecao',16),
                                                                                                          (90,'2023-07-16 00:00:00',NULL,'Agentes térmicos - salpicos de metal fundido',NULL,'SubTipoProtecao',6),
                                                                                                          (91,'2023-07-16 00:00:00',NULL,'Agentes mecânicos - motosserra',NULL,'SubTipoProtecao',6),
                                                                                                          (92,'2023-07-16 00:00:00',NULL,'Umidade proveniente de operações com uso de água (Classe II)',NULL,'SubTipoProtecao',6),
                                                                                                          (93,'2023-07-16 00:00:00',NULL,'Agentes químicos',NULL,'SubTipoProtecao',6),
                                                                                                          (94,'2023-07-16 00:00:00',NULL,'Agentes abrasivos e escoriantes',NULL,'SubTipoProtecao',7),
                                                                                                          (95,'2023-07-16 00:00:00',NULL,'Choque elétrico (Classe I)',NULL,'SubTipoProtecao',7),
                                                                                                          (96,'2023-07-16 00:00:00',NULL,'Choque elétrico (Classe II)',NULL,'SubTipoProtecao',7),
                                                                                                          (97,'2023-07-16 00:00:00',NULL,'Umidade proveniente de operações com uso de água (Classe II)',NULL,'SubTipoProtecao',7),
                                                                                                          (98,'2023-07-16 00:00:00',NULL,'Agentes químicos',NULL,'SubTipoProtecao',7),
                                                                                                          (99,'2023-07-16 00:00:00',NULL,'Impactos de quedas de objetos sobre os artelhos',NULL,'SubTipoProtecao',6),
                                                                                                          (104,'2023-07-16 00:00:00',NULL,'Posicionamento ou restrição de movimentação em trabalhos em altura',NULL,'SubTipoProtecao',24),
                                                                                                          (105,'2023-07-16 00:00:00',NULL,'Queda de um nível a outro e no posicionamento ou restrição de movimentação em trabalhos em altura',NULL,'SubTipoProtecao',24),
                                                                                                          (52,'2023-07-16 00:00:00',NULL,'Frio',NULL,'SubTipoProtecao',16);


CREATE TABLE public.tipo_epi_protecao_base (
                                               id bigserial NOT NULL,
                                               tipo_epi_id int8 NULL,
                                               protecao_base_id int8 NULL,
                                               indicacao_de_uso text NULL,
                                               CONSTRAINT tipo_epi_protecao_base_pkey PRIMARY KEY (id),
                                               CONSTRAINT fkbnyfvjh14gvaol33dfcpu69np FOREIGN KEY (protecao_base_id) REFERENCES public.protecao_base(id),
                                               CONSTRAINT fkqh0u24w92q4dfdfruvw4u85i8 FOREIGN KEY (tipo_epi_id) REFERENCES public.tipo_epi(id)
);

INSERT INTO public.tipo_epi_protecao_base (id, tipo_epi_id, protecao_base_id,indicacao_de_uso) VALUES
                                                                                                   (124,1,72,NULL),
                                                                                                   (125,2,49,NULL),
                                                                                                   (126,2,50,NULL),
                                                                                                   (127,2,43,NULL),
                                                                                                   (128,2,41,NULL),
                                                                                                   (129,2,46,NULL),
                                                                                                   (130,2,47,NULL),
                                                                                                   (131,2,42,NULL),
                                                                                                   (132,2,48,NULL);


CREATE TABLE public.protecao_base_norma_tecnica (
                                                    protecao_base_id int8 NOT NULL,
                                                    norma_tecnica_id int8 NOT NULL,
                                                    CONSTRAINT fk3e8383830sdlkjd0t3ffkf8qq FOREIGN KEY (norma_tecnica_id) REFERENCES public.norma_tecnica(id),
                                                    CONSTRAINT fkfe06dy3s803939kdkudm62aqg FOREIGN KEY (protecao_base_id) REFERENCES public.protecao_base(id)
);



DROP TABLE public.tipo_epi_sub_tipo_protecao_norma_tecnica;
DROP TABLE public.tipo_protecao_norma_tecnica;
DROP TABLE public.sub_tipo_protecao_norma_tecnica;
DROP TABLE public.tipo_epi_sub_tipo_protecao;
DROP TABLE public.sub_tipo_protecao;
DROP TABLE public.tipo_protecao;