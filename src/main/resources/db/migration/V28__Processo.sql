CREATE TABLE public.processo (
    id bigserial NOT NULL,
    numero_processo varchar(255) NULL,
    descricao text NULL,
    etapa int4 NULL,
    data_cadastro timestamp NULL,
    status varchar(255) NULL,
    CONSTRAINT processo_pkey PRIMARY KEY (id)
);