ALTER TABLE public.tipo_epi_processo ADD COLUMN IF NOT EXISTS titulo varchar(255) NULL;
ALTER TABLE public.tipo_epi_processo ADD COLUMN IF NOT EXISTS descricao text NULL;
ALTER TABLE public.tipo_epi_processo ADD COLUMN IF NOT EXISTS texto_protecao text NULL;
ALTER TABLE public.tipo_epi_processo ADD COLUMN IF NOT EXISTS indicacao_de_uso text NULL;