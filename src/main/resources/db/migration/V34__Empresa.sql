-- Script para criar a tabela empresa
CREATE TABLE empresa (
    id SERIAL PRIMARY KEY,
    razao_social VARCHAR(255),
    nome_fantasia VARCHAR(255),
    cnpj VARCHAR(255),
    endereco TEXT,
    cep VARCHAR(255),
    estado_id INT,
    municipio_id INT,
    email VARCHAR(255),
    telefone VARCHAR(255),
    data_cadastro DATE,
    data_revogacao DATE,
    status VARCHAR(50),
    tipo varchar(255) NULL,
    tipo_empresa varchar(255) NULL
);

-- Script para criar a tabela empresa_tipo_epi
CREATE TABLE empresa_tipo_epi (
    id SERIAL PRIMARY KEY,
    empresa_id INT,
    tipo_epi_id INT,
    FOREIGN KEY (empresa_id) REFERENCES empresa(id),
    FOREIGN KEY (tipo_epi_id) REFERENCES tipo_epi(id)
);

CREATE TABLE tipo_epi_processo_empresa (
    tipo_epi_processo_id BIGINT NOT NULL,
    empresa_id BIGINT NOT NULL,
    PRIMARY KEY (tipo_epi_processo_id, empresa_id),
    CONSTRAINT fk_tipo_epi_processo
        FOREIGN KEY (tipo_epi_processo_id)
        REFERENCES tipo_epi_processo (id),
    CONSTRAINT fk_empresa
        FOREIGN KEY (empresa_id)
        REFERENCES empresa (id)
);

DROP TABLE public.tipo_epi_processo_laboratorios_ocp;
DROP TABLE public.laboratorio_ocp_tipo_epi;
DROP TABLE public.laboratorio_ocp;

-- script para ajustar a sequence da tabela protecao_base
SELECT setval('protecao_base_id_seq', 105);