CREATE TABLE public.sub_tipo_protecao (
    id bigserial NOT NULL,
    data_cadastro timestamp NULL,
    descricao varchar(255) NULL,
    nome varchar(255) NULL,
    status varchar(255) NULL,
    tipo_protecao_id int8 NULL,
    CONSTRAINT sub_tipo_protecao_pkey PRIMARY KEY (id),
    CONSTRAINT fk47599rhw632h0gnkuum7fwitp FOREIGN KEY (tipo_protecao_id) REFERENCES public.tipo_protecao(id)
);

INSERT INTO public.sub_tipo_protecao (data_cadastro,descricao,nome,status,tipo_protecao_id) VALUES
                                                                                                  ('2023-07-16 00:00:00',NULL,'Aerodispersoides',NULL,1),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Aerodispersoides + Gases e vapores',NULL,2),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Respirador de circuito aberto de demanda com pressão positiva.',NULL,3),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Respirador de fluxo contínuo',NULL,4),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Respirador de fluxo contínuo

_________________



Respirador de fluxo contínuo




Respirador de demanda com pressão positiva',NULL,4),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Respirador de fluxo contínuo para operações de jateamento',NULL,4),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Impactos de quedas de
objetos sobre os artelhos',NULL,5),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes provenientes de energia elétrica',NULL,5),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes químicos',NULL,5),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Impactos de quedas de
objetos sobre os artelhos',NULL,6);
INSERT INTO public.sub_tipo_protecao (data_cadastro,descricao,nome,status,tipo_protecao_id) VALUES
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes provenientes de energia elétrica',NULL,6),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes térmicos - Combate a incêndio',NULL,6),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes químicos',NULL,6),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes térmicos - Salpicos de metal fundido',NULL,6),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes mecânicos - Motosserra',NULL,6),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes abrasivos e escoriantes',NULL,7),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes provenientes de energia elétrica',NULL,7),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes químicos',NULL,7),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes provenientes da energia elétrica',NULL,8),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Choques elétricos',NULL,9);
INSERT INTO public.sub_tipo_protecao (data_cadastro,descricao,nome,status,tipo_protecao_id) VALUES
                                                                                                  ('2023-07-16 00:00:00',NULL,'Vestimenta Condutiva',NULL,9),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Gases e vapores',NULL,10),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Luva cirúrgica',NULL,11),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Luva para procedimentos não cirúrgicos',NULL,11),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Luva para uso industrial',NULL,11),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes mecânicos - Abrasivos e escoriantes',NULL,12),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Impacto de partículas volantes',NULL,12),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes físicos (diversos)',NULL,12),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Impactos de partículas
volantes;
luminosidade intensa;
soldagem e processos similares.',NULL,12),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Níveis de pressão
sonora superiores ao estabelecido na NR-15, Anexos I e II',NULL,12);
INSERT INTO public.sub_tipo_protecao (data_cadastro,descricao,nome,status,tipo_protecao_id) VALUES
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes mecânicos - Cortes por impacto provocado por facas manuais',NULL,12),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes mecânicos - Motosserra',NULL,12),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes mecânicos - Atividade de corte manual de cana-de-açúcar',NULL,12),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes mecânicos - Abrasivos, escoriantes, cortantes e perfurantes',NULL,12),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes mecânicos -
Vibração',NULL,12),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Umidade proveniente de operações com uso de água',NULL,12),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes mecânicos - Cortantes e perfurantes provocados por facas manuais e objetos cortantes similares',NULL,12),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes térmicos - Arco elétrico',NULL,13),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes químicos',NULL,14),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes químicos -
Agrotóxicos',NULL,14);
INSERT INTO public.sub_tipo_protecao (data_cadastro,descricao,nome,status,tipo_protecao_id) VALUES
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes térmicos -
Calor e chamas',NULL,16),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes térmicos - Soldagem ou processos similares',NULL,16),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes térmicos - Arco elétrico',NULL,16),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes térmicos -Combate a incêndio',NULL,16),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes físicos (diversos)',NULL,16),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes térmicos -
Fogo repentino',NULL,16),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes térmicos - Combate a incêndio de estruturas',NULL,16),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes térmicos - Combate a incêndios florestais',NULL,16),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes térmicos (frio) -
Temperaturas abaixo de -5 ºC',NULL,17),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes térmicos (frio) -
Temperaturas acima de -5 ºC',NULL,17);
INSERT INTO public.sub_tipo_protecao (data_cadastro,descricao,nome,status,tipo_protecao_id) VALUES
                                                                                                  ('2023-07-16 00:00:00',NULL,'Agentes térmicos (frio)',NULL,17),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Umidade proveniente de operações com uso de água',NULL,18),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Umidade proveniente de precipitação pluviométrica',NULL,18),
                                                                                                  ('2023-07-16 00:00:00',NULL,'Umidade proveniente de operações com água',NULL,18),
                                                                                                  ('2023-07-14 21:00:00',NULL,'Agentes radioativos - Radiação ionizante (raio X)',NULL,15);
