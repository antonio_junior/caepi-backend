CREATE TABLE public.tipo_protecao (
    id bigserial NOT NULL,
    data_cadastro timestamp NULL,
    descricao varchar(255) NULL,
    nome varchar(255) NULL,
    status varchar(255) NULL,
    CONSTRAINT tipo_protecao_pkey PRIMARY KEY (id)
);

INSERT INTO public.tipo_protecao (data_cadastro,descricao,nome,status) VALUES
                                                                                 ('2023-07-16 00:00:00',NULL,'Aerodispersoides',NULL),
                                                                                 ('2023-07-16 00:00:00',NULL,'Aerodispersoides + Gases e vapores',NULL),
                                                                                 ('2023-07-16 00:00:00',NULL,'Atmosferas imediatamente perigosas à vida e à saúde (IPVS) e porcentagem de oxigênio menor ou igual a 12,5% ao nível do mar',NULL),
                                                                                 ('2023-07-16 00:00:00',NULL,'Atmosferas não imediatamente perigosas à vida e à saúde e com porcentagem de oxigênio superior a 12,5% ao nível do mar',NULL),
                                                                                 ('2023-07-16 00:00:00',NULL,'Calçado de Proteção',NULL),
                                                                                 ('2023-07-16 00:00:00',NULL,'Calçado de Segurança',NULL),
                                                                                 ('2023-07-16 00:00:00',NULL,'Calçado Ocupacional',NULL),
                                                                                 ('2023-07-16 00:00:00',NULL,'Calçado para trabalho ao potencial',NULL),
                                                                                 ('2023-07-16 00:00:00',NULL,'Choques elétricos',NULL),
                                                                                 ('2023-07-16 00:00:00',NULL,'Gases e vapores',NULL);
INSERT INTO public.tipo_protecao (data_cadastro,descricao,nome,status) VALUES
                                                                                 ('2023-07-16 00:00:00',NULL,'Riscos de origem biológica',NULL),
                                                                                 ('2023-07-16 00:00:00',NULL,'Riscos de origem mecânica',NULL),
                                                                                 ('2023-07-16 00:00:00',NULL,'Riscos de origem mecânica/térmica',NULL),
                                                                                 ('2023-07-16 00:00:00',NULL,'Riscos de origem química',NULL),
                                                                                 ('2023-07-16 00:00:00',NULL,'Riscos de origem radioativa',NULL),
                                                                                 ('2023-07-16 00:00:00',NULL,'Riscos de origem térmica (calor)',NULL),
                                                                                 ('2023-07-16 00:00:00',NULL,'Umidade',NULL),
                                                                                 ('2023-07-16 00:00:00',NULL,'Riscos de origem térmica (frio)',NULL);