CREATE TABLE public.nivel_desempenho (
    id bigserial NOT NULL,
    titulo VARCHAR(255) NOT NULL,
    tipo VARCHAR(255) NOT NULL,
    norma_tecnica_id int8 NULL,
    CONSTRAINT nivel_desempenho_pkey PRIMARY KEY (id)
);

CREATE TABLE public.nivel_desempenho_opcao (
     id bigserial NOT NULL,
     descricao VARCHAR(255) NOT NULL,
     nivel_desempenho_id int8 NULL,
     CONSTRAINT nivel_desempenho_opcao_pkey PRIMARY KEY (id)
);

ALTER TABLE public.nivel_desempenho ADD CONSTRAINT FK_nivel_desempenho_norma_tecnica FOREIGN KEY (norma_tecnica_id) REFERENCES public.norma_tecnica(id);
ALTER TABLE public.nivel_desempenho_opcao ADD CONSTRAINT FK_nivel_desempenho_opcao_nivel_desempenho FOREIGN KEY (nivel_desempenho_id) REFERENCES public.nivel_desempenho(id);