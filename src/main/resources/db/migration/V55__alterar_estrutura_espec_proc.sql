ALTER TABLE public.especificidade_processo RENAME COLUMN protecao_id TO protecao_processo_id;

ALTER TABLE public.especificidade_processo ADD CONSTRAINT fk_especificidade_protecao_proc FOREIGN KEY (protecao_processo_id) REFERENCES public.protecao_processo(id);