CREATE TABLE public.tipo_epi_processo (
    id bigserial NOT NULL,
    processo_id int8 NOT NULL,
    tipo_epi_id int8 NOT NULL,
    principal boolean NULL,
    CONSTRAINT tipo_epi_processo_pkey PRIMARY KEY (id),
    CONSTRAINT FK_tipo_epi_processo_processo FOREIGN KEY (processo_id) REFERENCES public.processo(id),
    CONSTRAINT FK_tipo_epi_processo_tipo_epi FOREIGN KEY (tipo_epi_id) REFERENCES public.tipo_epi(id)
);

