ALTER TABLE public.tipo_epi_processo ADD COLUMN IF NOT EXISTS em_revisao_especificidades_gerais bool NULL;
ALTER TABLE public.protecao_processo ADD COLUMN IF NOT EXISTS em_revisao bool NULL;

CREATE TABLE public.mensagem (
  id SERIAL PRIMARY KEY,
  texto TEXT NOT NULL,
  data_envio TIMESTAMP NOT NULL,
  data_leitura TIMESTAMP NULL,
  tipo_remetente VARCHAR(255) NOT NULL,
  id_remetente BIGINT NOT NULL,
  tipo_destinatario VARCHAR(255) NOT NULL,
  id_destinatario BIGINT NOT NULL,
  entidade_origem VARCHAR(255) NULL,
  id_entidade_origem BIGINT NULL
);