CREATE TABLE public.laboratorio_ocp (
      id bigserial NOT NULL,
      razao_social varchar(255) NULL,
      nome_fantasia varchar(255) NULL,
      cnpj varchar(255) NULL,
      data_cadastro timestamp NULL,
      data_revogacao timestamp NULL,
      endereco text NULL,
      cep varchar(255) NULL,
      tipo varchar(255) NULL,
      status varchar(255) NULL,
      CONSTRAINT tipo_protecao_pkey PRIMARY KEY (id)
);