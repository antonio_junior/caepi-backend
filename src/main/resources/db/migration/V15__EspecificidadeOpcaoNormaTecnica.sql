CREATE TABLE public.especificidade_opcao_norma_tecnica (
    especificidade_opcao_id int8 NOT NULL,
    norma_tecnica_id int8 NOT NULL,
    CONSTRAINT fk3083md8301idfxa0sdfsdf8qq FOREIGN KEY (norma_tecnica_id) REFERENCES public.norma_tecnica(id),
    CONSTRAINT fkfe06dy3p4sdfsdfsdfdy62aqg FOREIGN KEY (especificidade_opcao_id) REFERENCES public.especificidade_opcao(id)
);