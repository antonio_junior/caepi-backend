CREATE EXTENSION IF NOT EXISTS unaccent;

ALTER TABLE public.norma_tecnica ADD COLUMN IF NOT EXISTS nome_normalizado VARCHAR(255) NULL;

UPDATE norma_tecnica
SET nome_normalizado = UPPER(
  REGEXP_REPLACE(
    unaccent(nome),  -- Aplica a função unaccent antes de remover os caracteres não alfanuméricos
    '[^a-zA-Z0-9]',  -- Expressão regular para manter apenas números e letras
    '',              -- Substituir por uma string vazia
    'g'              -- 'g' indica substituição global, para todas as ocorrências
  )
);
