CREATE TABLE public.tipo_epi_conjugaveis (
                                             tipo_epi_id int8 NOT NULL,
                                             tipo_epi_conjugavel_id int8 NOT NULL,
                                             CONSTRAINT tipo_epi_conjugaveis_pkey PRIMARY KEY (tipo_epi_id, tipo_epi_conjugavel_id),
                                             CONSTRAINT fk4796ff39tj1d8vkef5g9m1uc7 FOREIGN KEY (tipo_epi_id) REFERENCES public.tipo_epi(id),
                                             CONSTRAINT fkip8wkfuemsighx7kwp7vdrgih FOREIGN KEY (tipo_epi_conjugavel_id) REFERENCES public.tipo_epi(id)
);
