ALTER TABLE public.especificidade_processo ADD COLUMN IF NOT EXISTS especificidade_pai int8 NULL;
ALTER TABLE public.especificidade_processo ADD COLUMN IF NOT EXISTS opcoes_resposta_pai text[] NULL;