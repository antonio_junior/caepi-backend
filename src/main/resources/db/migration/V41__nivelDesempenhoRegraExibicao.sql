CREATE TABLE nivel_desempenho_regra_exibicao (
  id bigserial NOT NULL,
  ordem int4 NOT NULL,
  nivel_desempenho_id int8 NOT NULL,
  campo_identificador varchar(255) NOT NULL,
  operador varchar(255) NOT NULL,
  valor_referencia varchar(255) NOT NULL,
  operador_logico varchar(255) NULL,
  CONSTRAINT nivel_desempenho_regra_exibicao_pkey PRIMARY KEY (id),
  CONSTRAINT FK_desempenho_regra_exibicao_nivel_desempenho FOREIGN KEY (nivel_desempenho_id) REFERENCES public.nivel_desempenho(id)
);