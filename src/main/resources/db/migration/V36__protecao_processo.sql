CREATE TABLE public.protecao_processo (
  id bigserial NOT NULL,
  nome varchar(255) NULL,
  descricao text NULL,
  selecionado bool NULL,
  tipo_epi_processo_id int8 NULL,
  id_origem int8 NOT NULL,

  CONSTRAINT protecao_processo_pkey PRIMARY KEY (id),
  CONSTRAINT FK_tipo_epi_processo_protecao_processo FOREIGN KEY (tipo_epi_processo_id) REFERENCES public.tipo_epi_processo(id)
);


CREATE TABLE public.sub_protecao_processo (
  id bigserial NOT NULL,
  nome varchar(255) NULL,
  descricao text NULL,
  selecionado bool NULL,
  protecao_pai_id int8 NULL,
  id_origem int8 NOT NULL,

  CONSTRAINT sub_protecao_processo_pkey PRIMARY KEY (id),
  CONSTRAINT FK_protecao_protecao_pai FOREIGN KEY (protecao_pai_id) REFERENCES public.protecao_processo(id)
);