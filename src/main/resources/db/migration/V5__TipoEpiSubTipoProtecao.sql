CREATE TABLE public.tipo_epi_sub_tipo_protecao (
                                               id bigserial NOT NULL,
                                               tipo_epi_id int8 NULL,
                                               sub_tipo_protecao_id int8 NULL,
                                               CONSTRAINT tipo_epi_sub_tipo_protecao_pkey PRIMARY KEY (id),
                                               CONSTRAINT fkbnyfvjh14gvaol2n1wcpu69np FOREIGN KEY (sub_tipo_protecao_id) REFERENCES public.sub_tipo_protecao(id),
                                               CONSTRAINT fkqh0u24w92q3b30fruvw4u85i8 FOREIGN KEY (tipo_epi_id) REFERENCES public.tipo_epi(id)
);

INSERT INTO public.tipo_epi_sub_tipo_protecao (tipo_epi_id,sub_tipo_protecao_id) VALUES
                                                                             (1,34),
                                                                             (1,31),
                                                                             (2,32),
                                                                             (2,26),
                                                                             (2,31),
                                                                             (2,39),
                                                                             (2,40),
                                                                             (2,42),
                                                                             (2,43),
                                                                             (2,44);
INSERT INTO public.tipo_epi_sub_tipo_protecao (tipo_epi_id,sub_tipo_protecao_id) VALUES
                                                                             (2,47),
                                                                             (2,48),
                                                                             (2,49),
                                                                             (2,50),
                                                                             (2,51),
                                                                             (2,53),
                                                                             (2,54),
                                                                             (3,10),
                                                                             (3,19),
                                                                             (3,19);
INSERT INTO public.tipo_epi_sub_tipo_protecao (tipo_epi_id,sub_tipo_protecao_id) VALUES
                                                                             (3,39),
                                                                             (3,10),
                                                                             (3,19),
                                                                             (3,19),
                                                                             (3,45),
                                                                             (3,39),
                                                                             (3,14),
                                                                             (3,32),
                                                                             (3,16),
                                                                             (3,19);
INSERT INTO public.tipo_epi_sub_tipo_protecao (tipo_epi_id,sub_tipo_protecao_id) VALUES
                                                                             (3,19),
                                                                             (3,39),
                                                                             (3,19),
                                                                             (4,42),
                                                                             (4,43),
                                                                             (4,44),
                                                                             (4,45),
                                                                             (4,50),
                                                                             (4,51),
                                                                             (4,39);
INSERT INTO public.tipo_epi_sub_tipo_protecao (tipo_epi_id,sub_tipo_protecao_id) VALUES
                                                                             (4,40),
                                                                             (4,26),
                                                                             (4,53),
                                                                             (5,39),
                                                                             (6,26),
                                                                             (7,33),
                                                                             (7,34),
                                                                             (7,35),
                                                                             (7,53),
                                                                             (7,37);
INSERT INTO public.tipo_epi_sub_tipo_protecao (tipo_epi_id,sub_tipo_protecao_id) VALUES
                                                                             (7,32),
                                                                             (7,42),
                                                                             (7,43),
                                                                             (7,45),
                                                                             (7,52),
                                                                             (7,23),
                                                                             (7,24),
                                                                             (7,25),
                                                                             (7,39),
                                                                             (7,41);
INSERT INTO public.tipo_epi_sub_tipo_protecao (tipo_epi_id,sub_tipo_protecao_id) VALUES
                                                                             (8,42),
                                                                             (8,43),
                                                                             (8,44),
                                                                             (8,47),
                                                                             (8,48),
                                                                             (8,49),
                                                                             (8,50),
                                                                             (8,39),
                                                                             (8,40),
                                                                             (8,53);
INSERT INTO public.tipo_epi_sub_tipo_protecao (tipo_epi_id,sub_tipo_protecao_id) VALUES
                                                                             (8,54),
                                                                             (9,20),
                                                                             (9,39),
                                                                             (9,34),
                                                                             (9,31),
                                                                             (9,53),
                                                                             (9,43),
                                                                             (9,42),
                                                                             (10,29),
                                                                             (11,27);
INSERT INTO public.tipo_epi_sub_tipo_protecao (tipo_epi_id,sub_tipo_protecao_id) VALUES
                                                                             (11,46),
                                                                             (12,27),
                                                                             (13,32),
                                                                             (13,26),
                                                                             (13,31),
                                                                             (13,43),
                                                                             (13,42),
                                                                             (13,39),
                                                                             (13,40),
                                                                             (13,53);
INSERT INTO public.tipo_epi_sub_tipo_protecao (tipo_epi_id,sub_tipo_protecao_id) VALUES
                                                                             (14,30),
                                                                             (15,27),
                                                                             (15,46),
                                                                             (15,44),
                                                                             (16,4),
                                                                             (16,5),
                                                                             (16,6),
                                                                             (17,3),
                                                                             (18,1),
                                                                             (18,22);
INSERT INTO public.tipo_epi_sub_tipo_protecao (tipo_epi_id,sub_tipo_protecao_id) VALUES
                                                                             (18,2),
                                                                             (19,39),
                                                                             (19,40),
                                                                             (19,55),
                                                                             (19,54),
                                                                             (19,21),
                                                                             (20,42),
                                                                             (20,43),
                                                                             (20,44),
                                                                             (20,47);
INSERT INTO public.tipo_epi_sub_tipo_protecao (tipo_epi_id,sub_tipo_protecao_id) VALUES
                                                                             (20,48),
                                                                             (20,49),
                                                                             (20,50),
                                                                             (20,51),
                                                                             (20,26),
                                                                             (20,31),
                                                                             (20,32),
                                                                             (20,39),
                                                                             (20,40),
                                                                             (20,41);
INSERT INTO public.tipo_epi_sub_tipo_protecao (tipo_epi_id,sub_tipo_protecao_id) VALUES
                                                                             (20,54),
                                                                             (20,53);


