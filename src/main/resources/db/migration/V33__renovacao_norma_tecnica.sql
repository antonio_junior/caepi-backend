TRUNCATE TABLE public.protecao_base_norma_tecnica;

TRUNCATE TABLE public.nivel_desempenho CASCADE;

TRUNCATE TABLE public.norma_tecnica RESTART IDENTITY CASCADE;

INSERT INTO public.norma_tecnica (data_cadastro,data_revogacao,descricao,nome,status) VALUES
('2024-04-08 00:00:00',NULL,'Dedeira - Requisitos e métodos de ensaio','ABNT NBR 13599:2019',NULL),
('2024-04-08 00:00:00',NULL,'Equipamentos de proteção respiratória - Peças semifacial e um quarto facial','ABNT NBR 13694:2022',NULL),
('2024-04-08 00:00:00',NULL,'Equipamentos de proteção respiratória - Peças faciais inteiras','ABNT NBR 13695:2023',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção respiratória - Filtros químicos e combinados','ABNT NBR 13696:2010',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção respiratória - Filtros para partículas','ABNT NBR 13697:2010',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção respiratória - Peça semifacial filtrante para partículas','ABNT NBR 13698:2022',NULL),
('2024-04-08 00:00:00',NULL,'Equipamentos de proteção respiratória - Respirador de linha de ar comprimido para uso com peça facial inteira ou semifacial','ABNT NBR 14372:1999',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção respiratória - Máscara autônoma de ar comprimido com circuito aberto','ABNT NBR 13716:1996',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção individual contra queda de altura - Trava-queda deslizante incluindo a linha flexível de ancoragem','ABNT NBR 14626:2020',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção individual contra queda de altura - Trava-queda guiado em linha rígida','ABNT NBR 14627:2010',NULL);

INSERT INTO public.norma_tecnica (data_cadastro,data_revogacao,descricao,nome,status) VALUES
('2024-04-08 00:00:00',NULL,'Equipamento de proteção individual contra queda de altura - Trava-queda retrátil','ABNT NBR 14628:2020',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção individual contra queda de altura - Absorvedor de energia','ABNT NBR 14629:2020',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção respiratório - Respirador de linha de ar comprimido com capuz, para uso em operações de jateamento - Especificação','ABNT NBR 14750:2001 ',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção respiratória - Respirador de adução de ar - Respirador de linha de ar comprimido com capuz','ABNT NBR 14749:2001 ',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção individual contra queda de altura - Talabarte de segurança para retenção de queda','ABNT NBR 15834:2020',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção individual contra queda de altura - Cinturão de segurança tipo abdominal e talabarte de segurança para posicionamento e restrição','ABNT NBR 15835:2020',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção individual contra queda de altura - Cinturão de segurança tipo paraquedista','ABNT NBR 15836:2020',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção individual contra queda de altura - Conectores','ABNT NBR 15837:2020',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção individual - Protetores auditivos - Medição de atenuação de ruído com métodos de orelha real','ABNT NBR 16076:2020',NULL);

INSERT INTO public.norma_tecnica (data_cadastro,data_revogacao,descricao,nome,status) VALUES
('2024-04-08 00:00:00',NULL,'Trabalhos em linha viva - Vestimenta condutiva para uso em tensão nominal até 800 kV c.a. e ± 600 kV d.c. (IEC 60895:2002, MOD)','ABNT NBR 16135:2012',NULL),
('2024-04-08 00:00:00',NULL,'Cremes protetores de segurança contra agentes químicos - Requisitos e métodos de ensaio','ABNT NBR 16276:2018',NULL),
('2024-04-08 00:00:00',NULL,'Vestimentas de proteção contra calor e chama provenientes do fogo repentino - Requisitos','ABNT NBR 16623:2021',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção individual - Protetores auditivos - Métodos para a medição da perda por inserção de protetores auditivos em ruído contínuo ou impulsivo utilizando procedimentos com dispositivo de ensaio de microfone de campo na orelha humana ou dispositivo de ensaio acústico','ABNT NBR 17072:2022',NULL),
('2024-04-08 00:00:00',NULL,'Capacete de segurança para uso ocupacional - Especificação e métodos de ensaio','ABNT NBR 8221:2019',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção individual - Calçado isolante elétrico para trabalhos em instalações elétricas de baixa tensão até 500 V em ambiente seco - Requisitos e métodos de ensaios','ABNT NBR 16603:2017',NULL),
('2024-04-08 00:00:00',NULL,'Trabalhos em tensão - Vestimenta de proteção contra riscos térmicos de um arco elétrico Parte 1-1: Métodos de ensaio - Método 1: Determinação da resistência ao arco elétrico (ATPV ou EBT50) de materiais resistentes à chama para vestimenta','ABNT NBR IEC 61482-1-1:2017',NULL),
('2024-04-08 00:00:00',NULL,'Trabalho em tensão - Vestimenta de proteção contra riscos térmicos de um arco elétrico Parte 1-2: Métodos de ensaio - Método 2: Determinação de classe de proteção ao arco elétrico de material e vestuário utilizando um arco elétrico direcionado e restringido (box test)','ABNT NBR IEC 61482-1-2:2017',NULL),
('2024-04-08 00:00:00',NULL,'Trabalho sob tensão - Vestimenta de proteção contra perigos térmicos de um arco elétrico Parte 2: Requisitos','ABNT NBR IEC 61482-2:2023',NULL),
('2024-04-08 00:00:00',NULL,'Equipamentos de proteção individual - Métodos de ensaio para calçados','ABNT NBR ISO 20344:2015',NULL);

INSERT INTO public.norma_tecnica (data_cadastro,data_revogacao,descricao,nome,status) VALUES
('2024-04-08 00:00:00',NULL,'Equipamento de proteção individual - Calçado de segurança','ABNT NBR ISO 20345:2015',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção individual - Calçado de proteção','ABNT NBR ISO 20346:2015',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção individual - Calçado ocupacional','ABNT NBR ISO 20347:2015',NULL),
('2024-04-08 00:00:00',NULL,'Luvas cirúrgicas de borracha, estéreis ou a serem esterilizadas, de uso único - Especificação','ABNT NBR ISO 10282:2014',NULL),
('2024-04-08 00:00:00',NULL,'Luvas para exame médico de uso único Parte 1: Especificação para luvas produzidas de látex de borracha ou solução de borracha','ABNT NBR ISO 11193-1:2015',NULL),
('2024-04-08 00:00:00',NULL,'Luvas para exame médico de uso único Parte 2: Especifi cação para luvas produzidas de policloreto de vinila','ABNT NBR ISO 11193-2:2013',NULL),
('2024-04-08 00:00:00',NULL,'Vestimentas de proteção - Vestimentas para proteção contra calor e chama - Requisitos mínimos de desempenho','ABNT NBR ISO 11612:2017',NULL),
('2024-04-08 00:00:00',NULL,'Luvas de proteção contra produtos químicos perigosos e micro-organismos Parte 1: Terminologia e requisitos de desempenho para riscos químicos','ABNT NBR ISO 374-1:2018',NULL),
('2024-04-08 00:00:00',NULL,'Luvas de proteção contra produtos químicos perigosos e microrganismos Parte 5: Terminologia e requisitos de desempenho para riscos de microrganismos','ABNT NBR ISO 374-5:2018',NULL),
('2024-04-08 00:00:00',NULL,'Trabalho sob tensão elétrica - Luvas isolantes elétricas','ABNT NBR IEC 60903:2024',NULL);

INSERT INTO public.norma_tecnica (data_cadastro,data_revogacao,descricao,nome,status) VALUES
('2024-04-08 00:00:00',NULL,'Vestimenta de proteção - Requisitos de desempenho para vestimenta de proteção utilizada por trabalhadores na aplicação de agrotóxicos e durante o período de reentrada','ABNT NBR ISO 27065:2023',NULL),
('2024-04-08 00:00:00',NULL,'American National Standard For Occupational And Educational Personal Eye And Face Protection Devices','ANSI/ISEA Z87.1-2015',NULL),
('2024-04-08 00:00:00',NULL,'Standard Specification for Rubber Surgical Gloves','ASTM D3577-19',NULL),
('2024-04-08 00:00:00',NULL,'Standard Specification for Rubber Examination Gloves','ASTM D3578-19',NULL),
('2024-04-08 00:00:00',NULL,'Standard Specification for Poly(vinyl chloride) Gloves for Medical Application','ASTM D5250-19',NULL),
('2024-04-08 00:00:00',NULL,'Standard Specification for Nitrile Examination Standard Specification for Nitrile Examination Gloves for Medical Application Gloves for Medical Application','ASTM D6319-19',NULL),
('2024-04-08 00:00:00',NULL,'Standard Specification for Polychloroprene Examination Gloves for Medical Application','ASTM D6977-19',NULL),
('2024-04-08 00:00:00',NULL,'Standard Performance Specification for Flame Resistant and Electric Arc Rated Protective Clothing Worn by Workers Exposed to Flames and Electric Arcs','ASTM F1506-22 ',NULL),
('2024-04-08 00:00:00',NULL,'Standard Test Method for Determining the Arc Rating of Materials for Clothing','ASTM F1959/F1959M-22',NULL),
('2024-04-08 00:00:00',NULL,'Standard Test Method for Determining the Arc Rating and Standard Specification for Eye or Face Protective Products','ASTM F2178-17',NULL);

INSERT INTO public.norma_tecnica (data_cadastro,data_revogacao,descricao,nome,status) VALUES
('2024-04-08 00:00:00',NULL,'Standard Practice for Evaluating Response Characteristics of Safety Products in an Electric Arc Exposure','ASTM F2621/F2621M-22',NULL),
('2024-04-08 00:00:00',NULL,'Live working. Footwear for electrical protection Insulating footwear and overboots','BS EN 50321-1:2018',NULL),
('2024-04-08 00:00:00',NULL,'Respiratory protective devices. Self-contained open-circuit compressed air breathing apparatus with full face mask. Requirements, testing, marking','BS EN 137:2006',NULL),
('2024-04-08 00:00:00',NULL,'Respiratory protective devices. Full face masks. Requirements, testing, marking','BS EN 136:1998',NULL),
('2024-04-08 00:00:00',NULL,'Respiratory protective devices. Half masks and quarter masks. Requirements, testing, marking','BS EN 140:1999',NULL),
('2024-04-08 00:00:00',NULL,'Respiratory protective devices. Particle filters. Requirements, testing, marking','BS EN 143:2021',NULL),
('2024-04-08 00:00:00',NULL,'Specification and qualification of welding procedures for metallic materials. Welding procedure test Spot, seam and projection welding','BS EN 15614-2012:2021',NULL),
('2024-04-08 00:00:00',NULL,'Firefighters helmets. Helmets for wildland fire fighting','BS EN 16471:2014',NULL),
('2024-04-08 00:00:00',NULL,'Firefighters helmets. Helmets for wildland fire fighting','BS EN 166:2002',NULL),
('2024-04-08 00:00:00',NULL,'Personal protection. Equipment for eye and face protection during welding and allied processes','BS EN 175:1997',NULL);

INSERT INTO public.norma_tecnica (data_cadastro,data_revogacao,descricao,nome,status) VALUES
('2024-04-08 00:00:00',NULL,'Protective gloves for welders','BS EN 12477:2001+A1:2005',NULL),
('2024-04-08 00:00:00',NULL,'Footwear protecting against chemicals Requirements for limited contact with chemicals','BS EN 13832-2:2018',NULL),
('2024-04-08 00:00:00',NULL,'Footwear protecting against chemicals Requirements for prolonged contact with chemicals','BS EN 13832-3:2018',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing. Garments for protection against cool environments','BS EN 14058:2017+A1:2023',NULL),
('2024-04-08 00:00:00',NULL,'Footwear for firefighters','BS EN 15090:2012',NULL),
('2024-04-08 00:00:00',NULL,'Determination of material resistance to permeation by chemicals Permeation by potentially hazardous liquid chemicals under conditions of continuous contact','BS EN 16523-1:2015+A1:2018',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing. Ensembles and garments for protection against cold','BS EN 342:2017',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing. Protection against rain','BS EN 343:2019',NULL),
('2024-04-08 00:00:00',NULL,'Protective gloves for firefighters','BS EN 659:2003+A1:2008',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing. Clothing to protect against heat and flame. Minimum performance requirements','BS EN ISO 11612:2017',NULL);

INSERT INTO public.norma_tecnica (data_cadastro,data_revogacao,descricao,nome,status) VALUES
('2024-04-08 00:00:00',NULL,'Protective gloves General requirements and test methods','BS EM ISO 21420:2020',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing for firefighters. Requirements and test methods for fire hoods for firefighters','BS EN 13911:2017',NULL),
('2024-04-08 00:00:00',NULL,'The National Institute for Occupational Safety and Health','CERTIFICADO NIOSH',NULL),
('2024-04-08 00:00:00',NULL,'Respiratory protective devices. Compressed air line breathing devices with demand valve Devices with a full face mask. Requirements, testing and marking','BS EN 14593-1:2018',NULL),
('2024-04-08 00:00:00',NULL,'Respiratory protective devices. Continuous flow compressed air line breathing devices. Requirements, testing and marking','BS EN 14594:2018',NULL),
('2024-04-08 00:00:00',NULL,'Personal eye-equipment. High performance visors intended only for use with protective helmets','BS EN 14458:2018',NULL),
('2024-04-08 00:00:00',NULL,'Respiratory protective devices. Powered filtering devices incorporating a loose fitting respiratory interface. Requirements, testing, marking','BS EN 12941:2023',NULL),
('2024-04-08 00:00:00',NULL,'Respiratory protective devices. Powered filtering devices incorporating full face masks, half masks or quarter masks. Requirements, testing, marking','BS EN 12942:2023',NULL),
('2024-04-08 00:00:00',NULL,'Protective gloves against mechanical risks','BS EN 388:2016+A1:2018',NULL),
('2024-04-08 00:00:00',NULL,'Personal eye-protection - Automatic welding filters','BS EN 379:2003+A1:2009',NULL);

INSERT INTO public.norma_tecnica (data_cadastro,data_revogacao,descricao,nome,status) VALUES
('2024-04-08 00:00:00',NULL,'Protective gloves and other hand protective equipments against thermal risks (heat and/or fire)','BS EN 407:2020',NULL),
('2024-04-08 00:00:00',NULL,'Helmets for fire fighting in buildings and other structures','BS EN 443:2008',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing for firefighters. Performance requirements for protective clothing for firefighting activities','BS EN 469:2020',NULL),
('2024-04-08 00:00:00',NULL,'Protective gloves against cold','BS EN 511:2006 ',NULL),
('2024-04-08 00:00:00',NULL,'Coated fabrics for use in the manufacture of water penetration resistant clothing','BS EN 3546:1974',NULL),
('2024-04-08 00:00:00',NULL,'Live working. Footwear for electrical protection Insulating footwear and overboots','EN 50321-1:2018',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing for firefighters. Requirements and test methods for fire hoods for firefighters','EN 13911:2017',NULL),
('2024-04-08 00:00:00',NULL,'Protective gloves for firefighters','EN 659:2003+A1:2008',NULL),
('2024-04-08 00:00:00',NULL,'Mechanical vibration and shock. Hand-arm vibration. Measurement and evaluation of the vibration transmissibility of gloves at the palm of the hand','EN ISO 10819:2013+A2:2022',NULL),
('2024-04-08 00:00:00',NULL,'Protective gloves for welders','EN 12477:2001',NULL);

INSERT INTO public.norma_tecnica (data_cadastro,data_revogacao,descricao,nome,status) VALUES
('2024-04-08 00:00:00',NULL,'Respiratory protective devices. Powered filtering devices incorporating a loose fitting respiratory interface. Requirements, testing, marking','EN 12941:2023',NULL),
('2024-04-08 00:00:00',NULL,'Respiratory protective devices. Powered filtering devices incorporating full face masks, half masks or quarter masks. Requirements, testing, marking','EN 12942:2023',NULL),
('2024-04-08 00:00:00',NULL,'Respiratory protective devices. Full face masks. Requirements, testing, marking','EN 136:1998',NULL),
('2024-04-08 00:00:00',NULL,'Respiratory protective devices. Self-contained open-circuit compressed air breathing apparatus with full face mask. Requirements, testing, marking','EN 137:2006',NULL),
('2024-04-08 00:00:00',NULL,'Footwear protecting against chemicals Requirements for limited contact with chemicals','EN 13832-2:2018',NULL),
('2024-04-08 00:00:00',NULL,'Footwear protecting against chemicals Requirements for prolonged contact with chemicals','EN 13832-3:2018',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing for firefighters. Requirements and test methods for fire hoods for firefighters','EN 13911:2017',NULL),
('2024-04-08 00:00:00',NULL,'Respiratory protective devices. Half masks and quarter masks. Requirements, testing, marking','EN 140:1999',NULL),
('2024-04-08 00:00:00',NULL,'Respiratory protective devices. Particle filters. Requirements, testing, marking','EN 143:2021',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing. Garments for protection against cool environments','EN 14058:2017',NULL);

INSERT INTO public.norma_tecnica (data_cadastro,data_revogacao,descricao,nome,status) VALUES
('2024-04-08 00:00:00',NULL,'Personal eye-equipment. High performance visors intended only for use with protective helmets','EN 14458:2018',NULL),
('2024-04-08 00:00:00',NULL,'Respiratory protective devices. Compressed air line breathing devices with demand valve Devices with a full face mask. Requirements, testing and marking','EN 14593-1:2018',NULL),
('2024-04-08 00:00:00',NULL,'Respiratory protective devices. Continuous flow compressed air line breathing devices. Requirements, testing and marking','EN 14594:2018',NULL),
('2024-04-08 00:00:00',NULL,'Footwear for firefighters','EN 15090:2012',NULL),
('2024-04-08 00:00:00',NULL,'Specification and qualification of welding procedures for metallic materials. Welding procedure test Spot, seam and projection welding','EN 15614-2012:2021',NULL),
('2024-04-08 00:00:00',NULL,'Firefighters helmets. Helmets for wildland fire fighting','EN 16471:2014',NULL),
('2024-04-08 00:00:00',NULL,'Firefighters helmets. Helmets for wildland fire fighting','EN 166:2002',NULL),
('2024-04-08 00:00:00',NULL,'Personal protection. Equipment for eye and face protection during welding and allied processes','EN 175:1997',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing. Ensembles and garments for protection against cold','EN 342:2017',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing. Protection against rain','EN 343:2019',NULL);

INSERT INTO public.norma_tecnica (data_cadastro,data_revogacao,descricao,nome,status) VALUES
('2024-04-08 00:00:00',NULL,'Protective gloves against mechanical risks','EN 388:2016+A1:2018',NULL),
('2024-04-08 00:00:00',NULL,'Personal eye-protection - Automatic welding filters','EN 379:2003+A1:2009',NULL),
('2024-04-08 00:00:00',NULL,'Protective gloves and other hand protective equipments against thermal risks (heat and/or fire)','EN 407:2020',NULL),
('2024-04-08 00:00:00',NULL,'Helmets for fire fighting in buildings and other structures','EN 443:2008',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing for firefighters. Performance requirements for protective clothing for firefighting activities','EN 469:2020',NULL),
('2024-04-08 00:00:00',NULL,'Protective gloves against cold','EN 511:2006 ',NULL),
('2024-04-08 00:00:00',NULL,'Coated fabrics for use in the manufacture of water penetration resistant clothing','EN 3546:1974',NULL),
('2024-04-08 00:00:00',NULL,'Protective gloves against dangerous chemicals and micro-organisms Terminology and performance requirements for chemical risks','EN ISO 374-1:2016',NULL),
('2024-04-08 00:00:00',NULL,'Dispositivos de proteção contra radiação X para diagnóstico médico Parte 3: Vestimentas de proteção, óculos de proteção e blindagens de proteção para pacientes','IEC 61331-3:2020',NULL),
('2024-04-08 00:00:00',NULL,'Dispositivos de proteção contra radiação X para fins de diagnóstico médico Parte 1: Determinação das propriedades de atenuação de materiais','IEC 61331-1:2018',NULL);

INSERT INTO public.norma_tecnica (data_cadastro,data_revogacao,descricao,nome,status) VALUES
('2024-04-08 00:00:00',NULL,'Live working - Electrical insulating sleeves','IEC 60984:2014',NULL),
('2024-04-08 00:00:00',NULL,'Trabalho sob tensão elétrica - Luvas isolantes elétricas','IEC 60903:2024',NULL),
('2024-04-08 00:00:00',NULL,'Gloves and arm guards protecting against cuts and stabs by hand knives Part 1: Chain-mail gloves and arm guards','ISO 13999-1:1999',NULL),
('2024-04-08 00:00:00',NULL,'Gloves and arm guards protecting against cuts and stabs by hand knives Part 2: Gloves and arm guards made of material other than chain mail','ISO 13999-2:2003',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing - Aprons, trousers and vests protecting against cuts and stabs by hand knives','ISO 13998:2003',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing for protection against chemicals Classification, labelling and performance requirements','ISO 16602:2007+A1:2012',NULL),
('2024-04-08 00:00:00',NULL,'Safety footwear with resistance to chain saw cutting','ISO 17249:2013 ',NULL),
('2024-04-08 00:00:00',NULL,'Mechanical vibration and shock. Hand-arm vibration. Measurement and evaluation of the vibration transmissibility of gloves at the palm of the hand','ISO 10819:2013',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing for users of hand-held chainsaws - Part 2: Performance requirements and test methods for leg protectors','ISO 11393-2:2018',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing for users of hand-held chainsaws - Part 4: Performance requirements and test methods for protective gloves','ISO 11393-4:2018',NULL);

INSERT INTO public.norma_tecnica (data_cadastro,data_revogacao,descricao,nome,status) VALUES
('2024-04-08 00:00:00',NULL,'Protective clothing for users of hand-held chainsaws - Part 5: Performance requirements and test methods for protective gaiters','ISO 11393-5:2018',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing for users of hand-held chainsaws - Part 6: Performance requirements and test methods for upper body protectors','ISO 11393-6:2018',NULL),
('2024-04-08 00:00:00',NULL,'Protective clothing for use in welding and allied processes','ISO 11611:2015',NULL),
('2024-04-08 00:00:00',NULL,'PPE for firefighters Test methods and requirements for PPE used by firefighters who are at risk of exposure to high levels of heat and/or flame while fighting fires occurring in structures Part 3: Clothing','ISO 11999-3:2015',NULL),
('2024-04-08 00:00:00',NULL,'PPE for firefighters. Test methods and requirements for PPE used by firefighters who are at risk of exposure to high levels of heat and/or flame while fighting fires occurring in structures Helmets','ISO 11999-5:2015',NULL),
('2024-04-08 00:00:00',NULL,'PPE for firefighters. Test methods and requirements for PPE used by firefighters who are at risk of exposure to high levels of heat and/or flame while fighting fires occurring in structures Helmets','BS ISO 11999-5:2015',NULL),
('2024-04-08 00:00:00',NULL,'Wildland firefighting personal protective equipment. Requirements and test methods Helmets','ISO 16073-5:2019',NULL),
('2024-04-08 00:00:00',NULL,'Personal protective equipment Footwear protecting against risks in foundries and welding Part 1: Requirements and test methods for protection against risks in foundries','ISO 20349-1:2017',NULL),
('2024-04-08 00:00:00',NULL,'Personal protective equipment Footwear protecting against risks in foundries and welding Part 2: Requirements and test methods for protection against risks in welding and allied processes','ISO 20349-2:2017',NULL),
('2024-04-08 00:00:00',NULL,'Protective gloves General requirements and test methods','ISO 21420:2020',NULL);

INSERT INTO public.norma_tecnica (data_cadastro,data_revogacao,descricao,nome,status) VALUES
('2024-04-08 00:00:00',NULL,'Protective gloves against dangerous chemicals and micro-organisms Part 1: Terminology and performance requirements for chemical risks','ISO 374-1:2016',NULL),
('2024-04-08 00:00:00',NULL,'Protective gloves against dangerous chemicals and micro-organisms Part 2: Determination of resistance to penetration','ISO 374-2:2019',NULL),
('2024-04-08 00:00:00',NULL,'Protective gloves against dangerous chemicals and micro-organisms Part 4: Determination of resistance to degradation by chemicals','ISO 374-4:2019',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção respiratório - Respirador de linha de ar comprimido com capuz, para uso em operações de jateamento - Especificação','NBR 14750:2001',NULL),
('2024-04-08 00:00:00',NULL,'Equipamento de proteção respiratória - Respirador de adução de ar - Respirador de linha de ar comprimido com capuz','NBR 14749:2001',NULL),
('2024-04-08 00:00:00',NULL,'Equipamentos de proteção respiratória - Respirador de linha de ar comprimido para uso com peça facial inteira ou semifacial','NBR 14372:1999',NULL),
('2024-04-08 00:00:00',NULL,'Standard on Protective Clothing and Equipment for Wildland Fire Fighting and Urban Interface Fire Fighting','NFPA 1977-2022',NULL),
('2024-04-08 00:00:00',NULL,'Standard on Protective Ensembles for Structural Fire Fighting and Proximity Fire Fighting','NFPA 1971-2018',NULL),
('2024-04-08 00:00:00',NULL,'Ballistic Resistance of Personal Body Armor','NIJ Standard 0101.04',NULL),
('2024-04-08 00:00:00',NULL,'Personal eye-protection - Specifications.','UNE EN 166:2002',NULL);

INSERT INTO public.norma_tecnica (data_cadastro,data_revogacao,descricao,nome,status) VALUES
('2024-04-08 00:00:00',NULL,'Personal eye-protection - Automatic welding filters','UNE EN 379:2004+A1:2010',NULL);

UPDATE public.norma_tecnica
SET nome_normalizado = UPPER(
        REGEXP_REPLACE(
                unaccent(nome),  -- Aplica a função unaccent antes de remover os caracteres não alfanuméricos
                '[^a-zA-Z0-9]',  -- Expressão regular para manter apenas números e letras
                '',              -- Substituir por uma string vazia
                'g'              -- 'g' indica substituição global, para todas as ocorrências
            )
    );
