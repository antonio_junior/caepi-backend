create table arquivo_processo (
    id  bigserial not null,
    nome_arquivo varchar(255),
    caminho_arquivo varchar(255),
    codigo varchar(50),
    descricao varchar(255),
    data_cadastro timestamp,
    processo_id int8, primary key (id));

alter table arquivo_processo add constraint FKc224ynuh49aksw8h1lfi7d96c foreign key (processo_id) references processo;