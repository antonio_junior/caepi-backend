CREATE TABLE public.nivel_desempenho_item (
   id bigserial NOT NULL,
   rotulo VARCHAR(255) NOT NULL,
   nivel_desempenho_id int8 NULL,
   tipo varchar(255) NULL,
   ordem int4 NULL,
   CONSTRAINT nivel_desempenho_item_pkey PRIMARY KEY (id)
);

ALTER TABLE public.nivel_desempenho_item ADD CONSTRAINT FK_nivel_desempenho_item_nivel_desempenho FOREIGN KEY (nivel_desempenho_id) REFERENCES public.nivel_desempenho(id);

DROP TABLE public.nivel_desempenho_opcao;

CREATE TABLE public.nivel_desempenho_opcao (
   id bigserial NOT NULL,
   descricao VARCHAR(255) NOT NULL,
   nivel_desempenho_item_id int8 NULL,
   ordem int4 NULL,
   CONSTRAINT nivel_desempenho_opcao_pkey PRIMARY KEY (id)
);

ALTER TABLE public.nivel_desempenho_opcao ADD CONSTRAINT FK_nivel_desempenho_opcao_nivel_desempenho_item FOREIGN KEY (nivel_desempenho_item_id) REFERENCES public.nivel_desempenho_item(id);