DELETE FROM public.nivel_desempenho;

ALTER TABLE public.nivel_desempenho ADD COLUMN IF NOT EXISTS identificacao VARCHAR(255) NOT NULL;
ALTER TABLE public.nivel_desempenho ADD COLUMN IF NOT EXISTS formula TEXT NULL;
ALTER TABLE public.nivel_desempenho ADD COLUMN IF NOT EXISTS formula_resposta_pai TEXT NULL;
