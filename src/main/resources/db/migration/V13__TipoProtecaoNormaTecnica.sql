CREATE TABLE public.tipo_protecao_norma_tecnica (
     tipo_protecao_id int8 NOT NULL,
     norma_tecnica_id int8 NOT NULL,
     CONSTRAINT fk3e83838301idfx80t3ffkf8qq FOREIGN KEY (norma_tecnica_id) REFERENCES public.norma_tecnica(id),
     CONSTRAINT fkfe06dy3s85wnkd3o76oy62aqg FOREIGN KEY (tipo_protecao_id) REFERENCES public.tipo_protecao(id)
);