CREATE TABLE public.norma_tecnica_processo (
  id bigserial NOT NULL,
  nome varchar(255) NULL,
  nome_normalizado varchar(255) NULL,
  descricao varchar(255) NULL,
  protecao_processo_id int8 NULL,
  especificidade_opcao_processo_id int8 NULL,
  id_origem int8 NOT NULL,
  CONSTRAINT norma_tecnica_processo_pkey PRIMARY KEY (id),
  CONSTRAINT FK_norma_tecnica_processo_protecao FOREIGN KEY (protecao_processo_id) REFERENCES public.protecao_processo(id),
  CONSTRAINT FK_norma_tecnica_processo_especificade_opcao FOREIGN KEY (especificidade_opcao_processo_id) REFERENCES public.especificidade_opcao_processo(id)
);

CREATE TABLE public.nivel_desempenho_processo (
   id bigserial NOT NULL,
   id_origem int8 NOT NULL,
   norma_tecnica_processo_id int8 NOT NULL,
   identificacao varchar(255) NULL,
   titulo varchar(255) NULL,
   nivel int4 NOT NULL,
   ordem int4 NOT NULL,
   tipo varchar(255) NULL,
   informacao TEXT NULL,
   texto_ilimitado varchar(10) NULL,
   num_caracteres int4 NULL,
   casas_decimais int4 NULL,
   ilimitado varchar(10) NULL,
   vlrmin varchar(255) NULL,
   vlrmax varchar(255) NULL,
   resposta TEXT NULL,
   nivel_desempenho_processo_pai_id int8 NULL,
   CONSTRAINT nivel_desempenho_processo_pkey PRIMARY KEY (id),
   CONSTRAINT FK_nivel_desempenho_proc_norma_tecnica FOREIGN KEY (norma_tecnica_processo_id) REFERENCES public.norma_tecnica_processo(id),
   CONSTRAINT FK_nivel_desempenho_processo_pai_id FOREIGN KEY (nivel_desempenho_processo_pai_id) REFERENCES public.nivel_desempenho_processo(id)
);

CREATE TABLE public.nivel_desempenho_item_processo (
   id bigserial NOT NULL,
   id_origem int8 NOT NULL,
   nivel_desempenho_processo_id int8 NOT NULL,
   rotulo varchar(255) NOT NULL,
   tipo varchar(10) NOT NULL,
   ordem int4 NOT NULL,
   opcoes TEXT NULL,
   informacao TEXT NULL,
   texto_ilimitado varchar(10) NULL,
   num_caracteres int4 NULL,
   casas_decimais int4 NULL,
   ilimitado varchar(10) NULL,
   vlrmin varchar(255) NULL,
   vlrmax varchar(255) NULL,
   CONSTRAINT nivel_desempenho_item_processo_pkey PRIMARY KEY (id),
   CONSTRAINT FK_nivel_desempenho_item_nivel_desempenho_proc FOREIGN KEY (nivel_desempenho_processo_id) REFERENCES public.nivel_desempenho_processo(id)
);

CREATE TABLE public.nivel_desempenho_regra_exibicao_processo (
    id bigserial NOT NULL,
    id_origem int8 NOT NULL,
    ordem int8 NOT NULL,
    nivel_desempenho_processo_id int8 NOT NULL,
    campo_identificador int8 NULL,
    item_identificador int8 NULL,
    operador varchar(255) NULL,
    valor_referencia varchar(255) NULL,
    operador_logico varchar(255) NULL,
    CONSTRAINT nivel_desempenho_regra_exibicao_processo_pkey PRIMARY KEY (id),
    CONSTRAINT FK_nivel_desempenho_regra_exibicao_processo_nivel_desempenho_proc FOREIGN KEY (nivel_desempenho_processo_id) REFERENCES public.nivel_desempenho_processo(id)
);