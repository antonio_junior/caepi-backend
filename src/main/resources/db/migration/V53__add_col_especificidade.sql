ALTER TABLE public.especificidade ADD COLUMN IF NOT EXISTS protecao_id int8 NULL;

UPDATE
    public.especificidade e
set
    protecao_id = (
        select
            testp.tipos_epis_sub_tipos_protecao
        from
            public.tipos_epis_sub_tipos_protecao testp
        where
                testp.especificidade_id = e.id
    limit 1);