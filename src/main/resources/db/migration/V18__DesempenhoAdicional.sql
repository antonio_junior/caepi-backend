CREATE TABLE public.desempenho_adicional (
    id bigserial NOT NULL,
    titulo VARCHAR(255) NOT NULL,
    tipo VARCHAR(255) NOT NULL,
    ordem int4 NULL,
    norma_tecnica_id int8 NULL,
    CONSTRAINT desempenho_adicional_pkey PRIMARY KEY (id)
);

CREATE TABLE public.desempenho_adicional_opcao (
     id bigserial NOT NULL,
     descricao VARCHAR(255) NOT NULL,
     desempenho_adicional_id int8 NULL,
     CONSTRAINT desempenho_adicional_opcao_pkey PRIMARY KEY (id)
);

ALTER TABLE public.desempenho_adicional ADD CONSTRAINT FK_desempenho_adicional_norma_tecnica FOREIGN KEY (norma_tecnica_id) REFERENCES public.norma_tecnica(id);
ALTER TABLE public.desempenho_adicional_opcao ADD CONSTRAINT FK_desempenho_adicional_opcao_desempenho_adicional FOREIGN KEY (desempenho_adicional_id) REFERENCES public.desempenho_adicional(id);