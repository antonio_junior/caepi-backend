ALTER TABLE public.arquivo_processo ADD COLUMN IF NOT EXISTS etapa INTEGER;

ALTER TABLE public.arquivo_processo ADD COLUMN IF NOT EXISTS responsavel_upload VARCHAR(50);

ALTER TABLE public.arquivo_processo ADD COLUMN IF NOT EXISTS tipo VARCHAR(50);

ALTER TABLE public.arquivo_processo ADD COLUMN IF NOT EXISTS visibilidade VARCHAR(50);