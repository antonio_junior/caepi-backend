ALTER TABLE public.nivel_desempenho DROP COLUMN IF EXISTS tipo;
ALTER TABLE public.nivel_desempenho ADD COLUMN IF NOT EXISTS nivel int4 NULL;
ALTER TABLE public.nivel_desempenho ADD COLUMN IF NOT EXISTS nivel_desempenho_pai_id int8 NULL;
ALTER TABLE public.nivel_desempenho ADD CONSTRAINT FK_nivel_desempenho_nivel_desempenho_pai FOREIGN KEY (nivel_desempenho_pai_id) REFERENCES public.nivel_desempenho(id);
