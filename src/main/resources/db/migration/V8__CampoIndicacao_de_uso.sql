ALTER TABLE public.tipo_epi_sub_tipo_protecao ADD COLUMN IF NOT EXISTS indicacao_de_uso text NULL;
ALTER TABLE public.especificidade_opcao ADD COLUMN IF NOT EXISTS indicacao_de_uso text NULL;
ALTER TABLE public.tipo_epi ADD COLUMN IF NOT EXISTS indicacao_de_uso text NULL;