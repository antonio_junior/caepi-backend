CREATE TABLE public.especificidade (
    id bigserial NOT NULL,
    titulo VARCHAR(255) NOT NULL,
    tipo VARCHAR(255) NOT NULL,
    tipo_epi_id int8 NULL,
    CONSTRAINT especificidade_pkey PRIMARY KEY (id)
);

CREATE TABLE public.especificidade_opcao (
    id bigserial NOT NULL,
    descricao VARCHAR(255) NOT NULL,
    especificidade_id int8 NULL,
    CONSTRAINT especificidade_opcao_pkey PRIMARY KEY (id)
);

ALTER TABLE public.especificidade ADD CONSTRAINT FK_especificidade_tipoEpi FOREIGN KEY (tipo_epi_id) REFERENCES public.tipo_epi(id);
ALTER TABLE public.especificidade_opcao ADD CONSTRAINT FK_especificidadeOpcao_especificidade FOREIGN KEY (especificidade_id) REFERENCES public.especificidade(id);