package br.com.caepi;

import br.com.caepi.repository.NormaTecnicaRepository;
import br.com.caepi.repository.TipoEpiRepository;
import br.com.caepi.repository.SubTipoProtecaoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@SpringJUnitConfig
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class EntityRelationshipTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private TipoEpiRepository tipoEpiRepository;

    @Autowired
    private SubTipoProtecaoRepository subTipoProtecaoRepository;

    @Autowired
    private NormaTecnicaRepository normaTecnicaRepository;

    @BeforeEach
    public void setUp() {
        // Cria um novo TipoEpi
        //TipoEpi tipoEpiCapuzOuBalaclava= new TipoEpi("CAPUZ OU BALACLAVA", "Proteção do crânio e pescoço do usuário contra", new Date());
       // tipoEpiRepository.save(tipoEpiCapuzOuBalaclava);

        // Cria um novo TipoProtecao
        //TipoProtecao tipoProtecaoRiscosOrigemTermicaCalor = new TipoProtecao("Riscos de origem térmica (calor)", new Date());
        //tipoProtecaoRepository.save(tipoProtecaoRiscosOrigemTermicaCalor);

        // Cria novas NormaTecnica
        /*NormaTecnica normaTecnicaABNT = new NormaTecnica("ABNT NBR ISO 11612:2017");
        normaTecnicaRepository.save(normaTecnicaABNT);
        NormaTecnica normaTecnicaBS = new NormaTecnica("BS EN ISO 11612:2015");
        normaTecnicaRepository.save(normaTecnicaBS);
        NormaTecnica normaTecnicaSO = new NormaTecnica("SO 11612:2008");
        normaTecnicaRepository.save(normaTecnicaSO);
        NormaTecnica normaTecnicaISO = new NormaTecnica("ISO 11612:2010");
        normaTecnicaRepository.save(normaTecnicaISO);
        NormaTecnica normaTecnicaISOE = new NormaTecnica("ISO 11612:2015 (E)");
        normaTecnicaRepository.save(normaTecnicaISOE);*/
    }

   /* @Test
    public void testCriarTipoEpiETipoProtecao () {
        // Realizar a consulta no banco de dados
        List<Map<String, Object>> tipoEpi = jdbcTemplate.queryForList("SELECT * FROM tipo_epi");
        List<Map<String, Object>> tipoProtecao = jdbcTemplate.queryForList("SELECT * FROM tipo_protecao");
        List<Map<String, Object>> normaTecnica = jdbcTemplate.queryForList("SELECT * FROM norma_tecnica");

        // Verificar se a quantidade de produtos é a esperada
        assertEquals(1, tipoEpi.size());
        assertEquals(1, tipoProtecao.size());
        assertEquals(5, normaTecnica.size());
    }*/

    /*@Test
    public void testRelacionamentoEntreTipoEpiETipoProtecao () {
        // Cria um novo TipoEpi
        TipoEpi tipoEpiCapuzOuBalaclava= new TipoEpi("CAPUZ OU BALACLAVA", "Proteção do crânio e pescoço do usuário contra", new Date());
        tipoEpiRepository.save(tipoEpiCapuzOuBalaclava);

        // Cria novos TipoProtecao
        TipoProtecao tipoProtecaoRiscosOrigemTermicaCalor = new TipoProtecao("Riscos de origem térmica (calor)", new Date());
        tipoProtecaoRepository.save(tipoProtecaoRiscosOrigemTermicaCalor);
        TipoProtecao tipoProtecaoRiscosOrigemTermicaFrio = new TipoProtecao("Riscos de origem térmica (frio)", new Date());
        tipoProtecaoRepository.save(tipoProtecaoRiscosOrigemTermicaFrio);
        TipoProtecao tipoProtecaoUmidade = new TipoProtecao("Umidade", new Date());
        tipoProtecaoRepository.save(tipoProtecaoUmidade);


        //relacionar o tipo de epi com o tipo protecao
        TipoEpiTipoProtecao tipoEpiTipoProtecao = new TipoEpiTipoProtecao();
        tipoEpiTipoProtecao.setTipoEpi(tipoEpiCapuzOuBalaclava);
        tipoEpiTipoProtecao.setTipoProtecao(tipoProtecaoRiscosOrigemTermicaCalor);
        tipoEpiTipoProtecao.setTipoProtecao(tipoProtecaoRiscosOrigemTermicaFrio);
        tipoEpiTipoProtecao.setTipoProtecao(tipoProtecaoUmidade);
    }

    @Test
    public void testDadoUmTipoDeEpiRetornarTodosOsTiposProtecaoRelacionado () {
        // Cria um novo TipoEpi
        TipoEpi tipoEpiCapuzOuBalaclava= new TipoEpi("CAPUZ OU BALACLAVA", "Proteção do crânio e pescoço do usuário contra", new Date());
        tipoEpiRepository.save(tipoEpiCapuzOuBalaclava);

        // Cria novos TipoProtecao
        TipoProtecao tipoProtecaoRiscosOrigemTermicaCalor = new TipoProtecao("Riscos de origem térmica (calor)", new Date());
        tipoProtecaoRepository.save(tipoProtecaoRiscosOrigemTermicaCalor);
        TipoProtecao tipoProtecaoRiscosOrigemTermicaFrio = new TipoProtecao("Riscos de origem térmica (frio)", new Date());
        tipoProtecaoRepository.save(tipoProtecaoRiscosOrigemTermicaFrio);
        TipoProtecao tipoProtecaoUmidade = new TipoProtecao("Umidade", new Date());
        tipoProtecaoRepository.save(tipoProtecaoUmidade);


        //relacionar o tipo de epi com o tipo protecao
        TipoEpiTipoProtecao tipoEpiTipoProtecao = new TipoEpiTipoProtecao();
        tipoEpiTipoProtecao.setTipoEpi(tipoEpiCapuzOuBalaclava);
        tipoEpiTipoProtecao.setTipoProtecao(tipoProtecaoRiscosOrigemTermicaCalor);
        tipoEpiTipoProtecao.setTipoProtecao(tipoProtecaoRiscosOrigemTermicaFrio);
        tipoEpiTipoProtecao.setTipoProtecao(tipoProtecaoUmidade);

    }*/
}

