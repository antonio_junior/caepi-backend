-- TIPO_EPI
INSERT INTO tipo_epi (id, data_cadastro, descricao, titulo, certificado_de_conformidade, conjugado, ensaio_no_exterior) VALUES (1,'05-07-2023', 'CAPUZ OU BALACLAVA', 'Proteção do crânio e pescoço do usuário contra', false, false, false);
INSERT INTO tipo_epi (id, data_cadastro, descricao, titulo, certificado_de_conformidade, conjugado, ensaio_no_exterior) VALUES (2,'05-07-2023', 'ÓCULOS DE TELA', 'Proteção dos olhos do usuário contra', false, false, false);
INSERT INTO tipo_epi (id, data_cadastro, descricao, titulo, certificado_de_conformidade, conjugado, ensaio_no_exterior) VALUES (3,'05-07-2023', 'ÓCULOS', 'Proteção dos olhos do usuário contra', false, false, false);
INSERT INTO tipo_epi (id, data_cadastro, descricao, titulo, certificado_de_conformidade, conjugado, ensaio_no_exterior) VALUES (4,'05-07-2023', 'PROTETOR FACIAL', 'Proteção dos olhos e face do usuário contra', false, false, false);
INSERT INTO tipo_epi (id, data_cadastro, descricao, titulo, certificado_de_conformidade, conjugado, ensaio_no_exterior) VALUES (5,'05-07-2023', 'MÁSCARA DE SOLDA', 'Proteção dos olhos e face do usuário contra', false, false, false);

-- GRUPO_TIPO_PROTECAO
INSERT INTO grupo_tipo_protecao (id, data_cadastro, nome, status) values (1, '05-07-2023', 'Riscos de origem térmica (calor)', 'ATIVO');
INSERT INTO grupo_tipo_protecao (id, data_cadastro, nome, status) values (2, '05-07-2023', 'Riscos de origem térmica (frio)', 'ATIVO');
INSERT INTO grupo_tipo_protecao (id, data_cadastro, nome, status) values (3, '05-07-2023', 'Riscos de origem química', 'ATIVO');
INSERT INTO grupo_tipo_protecao (id, data_cadastro, nome, status) values (4, '05-07-2023', 'Riscos de origem mecânica', 'ATIVO');
INSERT INTO grupo_tipo_protecao (id, data_cadastro, nome, status) values (5, '05-07-2023', 'Umidade', 'ATIVO');
INSERT INTO grupo_tipo_protecao (id, data_cadastro, nome, status) values (6, '05-07-2023', 'Riscos de origem mecânica/térmica', 'ATIVO');

-- TIPO_PROTECAO
INSERT INTO tipo_protecao (id, data_cadastro, nome, status, grupo_tipo_protecao_id) values (1, '05-07-2023', 'Agentes térmicos - Calor e chamas', 'ATIVO', 1);
INSERT INTO tipo_protecao (id, data_cadastro, nome, status, grupo_tipo_protecao_id) values (2, '05-07-2023', 'Agentes térmicos - Combate a incêndio', 'ATIVO', 1);
INSERT INTO tipo_protecao (id, data_cadastro, nome, status, grupo_tipo_protecao_id) values (3, '05-07-2023', 'Agentes térmicos (frio) - Temperaturas abaixo de -5 ºC', 'ATIVO', 2);
INSERT INTO tipo_protecao (id, data_cadastro, nome, status, grupo_tipo_protecao_id) values (4, '05-07-2023', 'Agentes térmicos (frio) - Temperaturas acima de -5 ºC', 'ATIVO', 2);
INSERT INTO tipo_protecao (id, data_cadastro, nome, status, grupo_tipo_protecao_id) values (5, '05-07-2023', 'Agentes químicos', 'ATIVO', 3);
INSERT INTO tipo_protecao (id, data_cadastro, nome, status, grupo_tipo_protecao_id) values (6, '05-07-2023', 'Agentes químicos - Agrotóxicos', 'ATIVO', 3);
INSERT INTO tipo_protecao (id, data_cadastro, nome, status, grupo_tipo_protecao_id) values (7, '05-07-2023', 'Impacto de particulas volantes', 'ATIVO', 4);
INSERT INTO tipo_protecao (id, data_cadastro, nome, status, grupo_tipo_protecao_id) values (8, '05-07-2023', 'Agentes físicos (diversos)', 'ATIVO', 4);

-- TIPO_EPI_TIPO_PROTECAO
INSERT INTO tipo_epi_tipo_protecao (id, tipo_epi_id, tipo_protecao_id) values (1, 1, 1);
INSERT INTO tipo_epi_tipo_protecao (id, tipo_epi_id, tipo_protecao_id) values (2, 1, 2);
INSERT INTO tipo_epi_tipo_protecao (id, tipo_epi_id, tipo_protecao_id) values (3, 1, 3);
INSERT INTO tipo_epi_tipo_protecao (id, tipo_epi_id, tipo_protecao_id) values (4, 2, 7);
INSERT INTO tipo_epi_tipo_protecao (id, tipo_epi_id, tipo_protecao_id) values (5, 3, 7);
INSERT INTO tipo_epi_tipo_protecao (id, tipo_epi_id, tipo_protecao_id) values (6, 4, 7);
INSERT INTO tipo_epi_tipo_protecao (id, tipo_epi_id, tipo_protecao_id) values (7, 4, 8);

-- NORMA_TECNICA
INSERT INTO norma_tecnica (id, data_cadastro, nome, status) values (1, '05-07-2023', 'ABNT NBR ISO 11612:2017', 'ATIVO');
INSERT INTO norma_tecnica (id, data_cadastro, nome, status) values (2, '05-07-2023', 'BS EN ISO 11612:2015', 'ATIVO');
INSERT INTO norma_tecnica (id, data_cadastro, nome, status) values (3, '05-07-2023', 'ISO 11612:2008', 'ATIVO');
INSERT INTO norma_tecnica (id, data_cadastro, nome, status) values (4, '05-07-2023', 'ISO 11612:2010', 'ATIVO');
INSERT INTO norma_tecnica (id, data_cadastro, nome, status) values (5, '05-07-2023', 'ISO 11612:2015 (E)', 'ATIVO');

-- TIPO_EPI_TIPO_PROTECAO_NORMA_TECNICA
INSERT INTO tipo_epi_tipo_protecao_norma_tecnica (tipo_epi_tipo_protecao_id, norma_tecnica_id) values (1, 1);
INSERT INTO tipo_epi_tipo_protecao_norma_tecnica (tipo_epi_tipo_protecao_id, norma_tecnica_id) values (1, 2);
INSERT INTO tipo_epi_tipo_protecao_norma_tecnica (tipo_epi_tipo_protecao_id, norma_tecnica_id) values (1, 3);
INSERT INTO tipo_epi_tipo_protecao_norma_tecnica (tipo_epi_tipo_protecao_id, norma_tecnica_id) values (1, 4);
INSERT INTO tipo_epi_tipo_protecao_norma_tecnica (tipo_epi_tipo_protecao_id, norma_tecnica_id) values (1, 5);