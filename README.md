# Caepi-API	

### Sistema de Gerenciamento de Certificados de Aprovação de Equipamentos de Proteção Individual

### Documentação

[Documento de Visão](Docs/DocumentoDeVisao.md)

[User Stories](Docs/user_stories.md)

[Arquitetura do Sistema](Docs/arquitetura.md)

[Manual de Utilização](Docs/PDF/manual.pdf)

[Configuração do Container Docker](Docs/Configuracoes/ConfiguracaoContainerDocker.md)

[Montando Ambiente de Desenvolvimento](Docs/ambiente_desenvolvimento.md)