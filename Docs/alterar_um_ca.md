# Alterar um CA

Como um funcionário responsável pela gestão de CAs, quero alterar as informações de um CA mantendo o número anterior, para que eu possa atualizar dados sem alterar a identificação original do CA.

<u>**CENÁRIO 1**</u>:
Após uma pequena modificação em um EPI, é necessário atualizar a descrição no CA existente sem mudar seu número.

<u>**CENÁRIO 2**</u>:
Erros são encontrados nas informações de um CA após sua emissão, exigindo uma atualização rápida dos dados mantendo o mesmo número de CA.