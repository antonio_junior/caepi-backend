# Buscar número de CA por uma busca externa

Como um usuário do sistema, quero buscar o número de CA por uma busca externa, para que eu possa rapidamente verificar a validade e a autenticidade de um EPI.

**CENÁRIO 1**: Um técnico de segurança precisa confirmar se os EPIs comprados recentemente possuem CAs válidos antes de distribuí-los aos trabalhadores.


**CENÁRIO 2:** Um auditor realiza uma inspeção surpresa e quer verificar rapidamente a autenticidade dos CAs dos EPIs usados no local.