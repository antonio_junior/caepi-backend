# Comunicar a CGNOR para conferência de dados de um novo CA

Como um funcionário responsável pelo cadastro de CAs, quero comunicar a CGNOR para a conferência de dados de um novo CA, para que os dados possam ser verificados e validados, garantindo a precisão e a confiabilidade das informações no sistema.

<u>**CENÁRIO 1:**</u> Após o cadastro de um novo CA no sistema, o funcionário envia automaticamente os detalhes para a CGNOR para uma revisão formal.

<u>**CENÁRIO 2:**</u> Um CA recém-registrado apresenta discrepâncias nos dados, e o funcionário inicia uma comunicação com a CGNOR para esclarecer e corrigir as informações.
