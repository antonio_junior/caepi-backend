# Gerenciar EPIs

Como um usuário do sistema, quero gerenciar EPIs (visualizar, adicionar, remover, editar e gerar certificados de aprovação), para que eu possa manter o catálogo de EPIs atualizado e garantir que todas as informações sejam precisas e acessíveis.

<u>**CENÁRIO 1:**</u>
Um usuário detecta um erro na descrição de um EPI e precisa corrigi-lo para evitar mal-entendidos.

<u>**CENÁRIO 2:**</u>
Um novo modelo de EPI é lançado, e o usuário responsável precisa adicionar essas informações ao sistema, incluindo o novo certificado de aprovação.