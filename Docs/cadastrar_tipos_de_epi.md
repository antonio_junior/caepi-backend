# Cadastrar Tipos de EPI

Como um administrador do sistema, quero cadastrar diferentes tipos de EPIs no sistema, para que eu possa organizar e classificar os EPIs de forma eficiente, facilitando a busca e o gerenciamento.

<u>**CENÁRIO 1:**</u> Uma nova categoria de proteção é reconhecida oficialmente, e o administrador precisa adicionar esse novo tipo de EPI ao sistema.

<u>**CENÁRIO 2:**</u> Os usuários solicitam uma melhor categorização dos EPIs para facilitar a busca, levando o administrador a reorganizar os tipos de EPIs no sistema.