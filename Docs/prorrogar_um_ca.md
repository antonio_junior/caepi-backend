# Prorrogar um CA

Como um funcionário responsável pela gestão de CAs, quero prorrogar a validade de um CA, para que os EPIs possam continuar sendo utilizados legalmente sem interrupções.

<u>**CENÁRIO 1**</u>:
Devido a atrasos na produção de um novo modelo de EPI, o fabricante solicita uma prorrogação do CA para continuar vendendo o modelo atual.

<u>**CENÁRIO 2**</u>:
Uma mudança regulatória estende a validade dos CAs, e um funcionário precisa atualizar a data de validade no sistema para todos os CAs afetados.


