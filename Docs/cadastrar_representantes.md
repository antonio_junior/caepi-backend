# Cadastrar Representantes

Como um administrador do sistema, quero cadastrar representantes de empresas ou EPIs no sistema, para que eu possa gerenciar quem são os pontos de contato para questões relacionadas a EPIs específicos.

<u>**CENÁRIO 1:**</u>
Uma empresa nomeia um novo representante para lidar com questões relacionadas aos seus EPIs, e o administrador do sistema precisa atualizar essa informação.

<u>**CENÁRIO 2:**</u>
Um representante deixa sua posição, e um substituto é nomeado, exigindo que o administrador atualize os detalhes de contato no sistema.