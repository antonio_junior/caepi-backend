# Cadastrar Usuários 

Como um administrador do sistema, quero cadastrar usuários com suas informações pessoais, para que os usuários possam acessar o sistema e executar suas funções de acordo com seus papéis.

<u>**CENÁRIO 1**:</u>
Um novo funcionário é contratado e precisa de acesso ao sistema para gerenciar informações sobre EPIs.

<u>**CENÁRIO 2:**</u>
Um usuário existente muda de departamento e precisa de suas permissões atualizadas no sistema para refletir seu novo papel.