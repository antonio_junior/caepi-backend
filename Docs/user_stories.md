# User Stories

[Buscar número de CA por uma busca externa](buscar_numero_de_ca_por_uma_busca_externa.md)

[Conferir se o CA existe na base antiga ou atual](conferir_se_o_ca_existe_na_antiga_ou_atual.md)

[Comunicar a CGNOR para conferência de dados de um novo CA](comunicar_a_cgnor.md)

[Emitir um CA](emitir_um_ca.md)

[Renovar um CA](renovar_um_ca.md)

[Alterar um CA](alterar_um_ca.md)

[Prorrogar um CA](prorrogar_um_ca.md)

[Cadastrar empresas](cadastrar_empresas.md)

[Cadastrar usuários](cadastrar_usuarios.md)

[Cadastrar EPIs](cadastrar_epi.md)

[Cadastrar tipos de EPIs](cadastrar_tipos_de_epi.md)

[Cadastrar tipos de proteção](cadastrar_tipos_de_protecao.md)

[Cadastrar representantes](cadastrar_representantes.md)

[Gerenciamento de EPIs](gerenciar_epis.md)