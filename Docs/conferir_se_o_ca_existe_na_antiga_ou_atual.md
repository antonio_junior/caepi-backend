# Conferir se o CA existe na base antiga ou atual

Como um administrador do sistema, quero conferir se o CA existe na base antiga ou atual, para que eu possa gerenciar eficientemente os registros de CAs e identificar quais necessitam atualização ou renovação.

**CENÁRIO 1**: Durante a auditoria de conformidade, um administrador precisa identificar CAs que estão prestes a expirar e necessitam renovação.

**CENÁRIO 2**:  Um fabricante de EPI quer verificar se o CA de seu produto recém-modificado consta na base atualizada após submeter uma solicitação de atualização.