# Cadastrar Empresas

Como um administrador do sistema, quero cadastrar empresas no sistema com todas as informações necessárias, para que eu possa gerenciar as entidades relacionadas aos EPIs de forma organizada.

<u>**CENÁRIO 1:**</u>
Uma nova empresa fabricante de EPIs entra no mercado e precisa ser adicionada ao sistema para acompanhar seus produtos e CAs.

<u>**CENÁRIO 2**:</u>
Uma empresa muda seu nome ou endereço, e o administrador precisa atualizar essas informações no sistema para manter a precisão dos registros.