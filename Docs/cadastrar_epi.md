# Cadastrar EPI

Como um administrador do sistema, quero cadastrar EPIs no sistema com informações detalhadas, para que os usuários possam facilmente encontrar e identificar os EPIs necessários.

<u>**CENÁRIO 1**</u>:
Um EPI inovador é desenvolvido e precisa ser adicionado ao sistema com todas as suas especificações e informações de CA.

<u>**CENÁRIO 2**</u>:
Um EPI existente passa por modificações significativas, requerendo uma atualização em seu cadastro no sistema para refletir as novas características.