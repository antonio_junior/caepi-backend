# Documento de Visão

## Sistema CAEPI

### 1. Objetivo

  O propósito deste documento é coletar, analisar e definir as necessidades de alto-nível e características do sistema, focando nas potencialidades requeridas pelos afetados e usuários-alvo, e como estes requisitos foram abordados no sistema.

  A visão do sistema documenta o ambiente geral de processos desenvolvidos para o sistema,fornecendo a todos os envolvidos uma descrição compreensível deste e suas macro-funcionalidades.

  O Documento de Visão do Sistema documenta as necessidades e funcionalidades do sistema. 

### 2. Descrição do Produto

  O novo sistema automatizado para gerenciar Certificados de Aprovação (CA) de Equipamentos de Proteção Individual (EPIs) visa tornar o processo mais rápido e eficiente. Com a parametrização adequada, a recepção de documentos e informações no sistema será melhorada, permitindo a emissão, renovação e alteração automática dos CAs. Essa medida economizará tempo, possibilitando à Coordenação Geral de Normatização e Registros (CGNOR) focar em atividades essenciais.

  O sistema proporcionará simplificação e agilidade na emissão de CA, permitindo aos fabricantes escolher o momento adequado para cadastrar seus EPIs. Isso beneficia empregadores e trabalhadores, mantendo centralização de informações, gerando segurança.

  A implementação do sistema automático reduzirá custos para empresas e para todos. O acesso imediato a uma maior oferta de EPIs também é um benefício, especialmente em situações extraordinárias como a pandemia de COVID-19

  Para atingir esses objetivos, é essencial criar a estrutura do sistema conforme os parâmetros da CGNOR/SIT e disponibilizá-la para testes internos. Além disso, uma ferramenta de busca no sistema atual (CAEPI) garantirá a continuidade de uso de números de CA já emitidos durante transições futuras de sistemas.

#### 2.1 Requisitos Funcionais

##### 2.1.1 Buscar número de CA por uma busca externa

Descrição: O sistema deve integrar-se com bases de dados externas para consultar o número de CA (Certificado de Aprovação) de EPIs (Equipamentos de Proteção Individual). Essa funcionalidade deve permitir a busca por meio de filtros específicos, como nome do EPI, fabricante ou tipo de proteção.
Exemplo: Um usuário pode inserir o nome de um EPI no campo de busca e o sistema retorna o número de CA correspondente, caso exista.

##### 2.1.2 Conferir se o CA existe na base antiga ou atual

Descrição: O sistema deve verificar se o número de CA fornecido já está cadastrado em sua base de dados, distinguindo entre registros antigos e atuais. Isso permite identificar CAs que precisam de renovação ou atualização.
Exemplo: Ao tentar cadastrar um novo CA, o sistema automaticamente verifica se o número já existe e indica se pertence à base antiga ou à base atual.

##### 2.1.3 Comunicar a CGNOR para conferência de dados de um novo CA

Descrição: Deve haver um mecanismo automatizado ou manual para notificar a Coordenação-Geral de Normatização e Registro (CGNOR) sempre que um novo CA for cadastrado no sistema, para que os dados possam ser verificados e validados.
Exemplo: Após o cadastro de um novo CA, um email é enviado automaticamente à CGNOR com os detalhes para conferência.

##### 2.1.4 Emitir, Renovar, Alterar e Prorrogar um CA

Descrição: O sistema deve permitir a emissão, renovação, alteração e prorrogação de CAs, mantendo um histórico de todas as modificações. Cada processo deve garantir que as informações necessárias, como número do CA, data de validade e descrição, sejam atualizadas corretamente.
Exemplo: Para renovar um CA, o usuário acessa a função de renovação, insere o número do CA existente, e o sistema permite atualizar a data de validade e outras informações relevantes.

##### 2.1.5 Cadastrar empresas, usuários, EPIs, tipos de EPIs, tipos de proteção, representantes e referências

Descrição: O sistema deve oferecer interfaces para o cadastro de empresas, usuários, EPIs e outros elementos relevantes, solicitando as informações necessárias para cada tipo de cadastro e garantindo a integridade e a organização dos dados.
Exemplo: Para cadastrar uma empresa, o sistema apresenta um formulário com campos para nome, CNPJ, endereço, telefone e email, que deve ser preenchido e submetido pelo usuário.

##### 2.1.6 Gerenciamento de EPIs

Descrição: Os usuários devem ser capazes de visualizar, adicionar, remover, editar e gerar certificados de aprovação para EPIs cadastrados, facilitando o controle e a administração dos itens.
Exemplo: Um usuário com permissões adequadas pode acessar a lista de EPIs, selecionar um EPI específico e atualizar sua descrição ou gerar um novo certificado de aprovação.

##### 2.1.7 Revisar/alterar todas as requisições que serão mostradas aos administradores

Descrição: Deve haver um sistema de revisão que permita aos administradores visualizar, aprovar ou rejeitar as requisições feitas no sistema, garantindo um controle efetivo sobre as alterações e inclusões de dados.
Exemplo: Um administrador recebe notificações de novas requisições, acessa o painel de controle e revisa cada uma, decidindo se aprova ou rejeita com base nas políticas internas.

#### 3.1 Requisitos Não Funcionais

##### 3.1.1 Segurança

O sistema deve assegurar a segurança dos dados por meio de criptografia, autenticação e autorização, utilizando práticas de segurança atualizadas e conformes com as normativas aplicáveis.

##### 3.1.2 Usabilidade

A interface do usuário deve ser intuitiva e amigável, facilitando o uso do sistema sem a necessidade de treinamento extensivo, e deve seguir os padrões de design system do gov.br para manter a consistência.

##### 3.1.3 Responsividade

O design do sistema deve ser responsivo, garantindo uma boa experiência de usuário em diferentes dispositivos e tamanhos de tela, desde desktops a smartphones.

##### 3.1.4 Escalabilidade

A arquitetura do sistema deve ser capaz de suportar um aumento no volume de dados e usuários sem degradar o desempenho, permitindo a expansão conforme necessário.

### 4.   Proposta de Solução Tecnológica Escolhida

O projeto de desenvolvimento do sistema automatizado para gerenciamento de Certificados
de Aprovação (CA) requer a escolha de tecnologias adequadas para a implementação da
solução. Serão apresentadas as tecnologias que serão utilizadas no desenvolvimento do
sistema em cada camada, seguindo a arquitetura em camadas proposta anteriormente.

Para a camada de apresentação (frontend), será utilizada a tecnologia Angular 15, um
framework de desenvolvimento de aplicações web comumente utilizado em grandes projetos.
O Angular permite a criação de interfaces de usuário dinâmicas e responsivas, tornando a
experiência do usuário mais agradável e interativa. Além disso, o Angular é baseado em
TypeScript, uma linguagem de programação orientada a objetos que oferece recursos
adicionais em relação ao JavaScript, como a tipagem estática, tornando o código mais fácil de
manter e depurar.

Para a camada de negócios (backend), será utilizado o framework Spring Boot, que oferece
uma estrutura robusta e escalável para o desenvolvimento de aplicativos em Java. O Spring
Boot fornece recursos para a implementação de APIs RESTful, bem como integração com
bancos de dados relacionais. Além disso, utilizar o Spring Security para autenticação e
autorização dos usuários e o Spring Data JPA para acesso aos dados do banco de dados.

Para a camada de persistência de dados (banco de dados), será utilizado o Postgresql, um
sistema de gerenciamento de banco de dados relacional amplamente utilizado no mercado. O
Postgresql é conhecido por sua estabilidade, escalabilidade e segurança, tornando-o uma
escolha popular para aplicativos desse tamanho, sendo utilizado por outros sistemas
desenvolvidos para a SIT.

Além das tecnologias mencionadas acima, também será utilizado o Git, um sistema de
controle de versão distribuído, para gerenciar o código-fonte do projeto. O Git oferece uma
maneira fácil e segura de gerenciar o código-fonte e controlar as alterações em diferentes
versões do software.

A escolha das tecnologias para o desenvolvimento do sistema, baseou-se na eficiência,
escalabilidade e estabilidade das tecnologias utilizadas. A utilização de tecnologias bem
estabelecidas, como Angular, Spring Boot e Postgresql, juntamente com o controle de versão
Git, garantirá a criação de um sistema robusto e confiável para o SISCAEPI

### 5. Metodologia de Desenvolvimento de Software

  O sistema deverá seguir as regras descritas no documento Metodologia de Desenvolvimento de Software (MDS).  

  A Metodologia está disponível em https://gitlab.sit.trabalho.gov.br/sit/mds

### 6. Requisitos não funcionais

  Os requisitos não funcionais, tais como linguagem e outros parâmetros básicos para codificação estão descritos em documento anexo, intitulado Padrão de Codificação.  Entretanto, apesar da SIT utilizar como padrão as linguagens Java e C# .NET, em razão de maior facilidade de integração entre C# e o Single Sing On (SSO) da SIT, devido a já existência de bibliotecas utilizadas em outros projetos, recomenda-se o desenvolvimento na linguagem C# .NET.

  O Padrão de Codificação está disponível em https://gitlab.sit.trabalho.gov.br/sit/mds

### 7. Requisitos Equipe Operacional 

  Em documento anexo, intitulado Requisitos para Contratação de Equipe Operacional, estão os requisitos necessários referentes a equipe operacional para participação no projeto, tais como conhecimentos necessários, experiência, entre outros.

  Para este projeto especificamente não será necessária a contratação de arquiteto de software, podendo portanto ser contratados os profissionais Full Stack pleno ou senior de acordo com a preferência e disponibilidade.

  Os requisitos da equipe estão disponíveis em https://gitlab.sit.trabalho.gov.br/sit/mds
















