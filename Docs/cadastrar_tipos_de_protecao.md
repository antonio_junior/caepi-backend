# Cadastrar Tipos de Proteção

Como um administrador do sistema, quero cadastrar os tipos de proteção no sistema, para que eu possa especificar e classificar as proteções que cada EPI oferece, melhorando a precisão das informações.

<u>**CENÁRIO 1:**</u>
Novas normas de segurança introduzem tipos de proteção inéditos, que o administrador precisa registrar no sistema.

<u>**CENÁRIO 2:**</u>
Feedback de usuários indica confusão sobre as categorias de proteção existentes, motivando uma revisão e atualização das classificações no sistema.