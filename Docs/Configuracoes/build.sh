#!/bin/bash

#variáveis de ambiente
SISTEMA=sisacte
HOSTDIR=/srv/$SISTEMA #diretório do volume mapeado do container
ARTIFACTSDIR=/root/$SISTEMA/job#3782/Backend-SpringBoot #diretório dos artefatos da integração continua
APPLICATIONPROPERTIES=/root/$SISTEMA/application.properties
IMAGETAG=1.0.14  #tag da imagem docker

echo "SCRIPT DE BUILD PARA $SISTEMA"
echo
echo "-------------------------------"
echo "VARIÁVEIS DE AMBIENTE"
echo "-------------------------------"
echo "HOSTDIR=$HOSTDIR"
echo "ARTIFACTSDIR=$ARTIFACTSDIR"
echo "APPLICATIONPROPERTIES=$APPLICATIONPROPERTIES"
echo "IMAGETAG=$IMAGETAG"
echo "-------------------------------"
echo "Pressione <ENTER>"; read

#test vars
if [ -z "$ARTIFACTSDIR" ]; then
    echo "[ERROR] path not found => $ARTIFACTSDIR"
    exit 1
fi
if [ ! -f "$APPLICATIONPROPERTIES" ]; then
    echo "[ERROR] file not found => $APPLICATIONPROPERTIES"
    exit 1
fi

echo "criando diretórios no host..."
mkdir $HOSTDIR $HOSTDIR/app $HOSTDIR/app/lib $HOSTDIR/data $HOSTDIR/tmp

echo  "copiando app para diretórios no host..."
cp -a $ARTIFACTSDIR/target/dependency/BOOT-INF/lib $HOSTDIR/app
cp -a $ARTIFACTSDIR/target/dependency/META-INF $HOSTDIR/app
cp -a $ARTIFACTSDIR/target/dependency/BOOT-INF/classes/* $HOSTDIR/app
cp    $APPLICATIONPROPERTIES $HOSTDIR/app

echo "setando permissões..."
chown -R 100:101 $HOSTDIR  #100 e 101 => usuário e grupo criados pelo alpine no container
chmod -R o-rwx $HOSTDIR


echo "criando Dockerfile..."
cat << EOF > $HOSTDIR/Dockerfile
FROM openjdk:8-jdk-alpine
LABEL sistema="$SISTEMA"

# Para rodar como usuário non-root
RUN mkdir /$SISTEMA /$SISTEMA/app/ /$SISTEMA/data /$SISTEMA/tmp
RUN addgroup -S $SISTEMA && adduser -S -G $SISTEMA $SISTEMA
RUN chown -R $SISTEMA:$SISTEMA /$SISTEMA
USER $SISTEMA

VOLUME ["/$SISTEMA"]

ENTRYPOINT ["java","-cp","$SISTEMA/app:$SISTEMA/app/lib/*","br.com.sisacte.api.SisActeApplication"]

EXPOSE 8102
EOF

echo "removendo containers e images antigos..."
docker stop $SISTEMA
docker rm   $SISTEMA
docker image prune --filter="label=sistema=$SISTEMA"

echo "construindo imagem docker..."
docker build -t sit/$SISTEMA:$IMAGETAG $HOSTDIR

echo "running docker container..."
docker run --name $SISTEMA -d -p 8102:8102 -v $HOSTDIR:/$SISTEMA sit/$SISTEMA:$IMAGETAG

echo "listando logs..."
docker logs -f $SISTEMA
