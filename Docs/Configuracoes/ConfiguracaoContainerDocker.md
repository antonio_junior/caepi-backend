## Configuração e Instalação da aplicação utilizando container Docker

### Pré-requisitos
  - zip com artefatos de instalação
  - configuração arquivo `consts.js`
  - arquivo `application.properties` específico do ambiente
  - script de build para instalação

### Arquivos de instalação

  Após gerar a tag do SisActe no gitlab, o deploy_job irá gerar um pacote .zip com artefatos para download.  
  - Faça o download do pacote .zip de artefatos no servidor onde será instalada a aplicação.
  - descompacte os artefatos em uma pasta com nome sugestivo: 

  ``` bash
  cd /root
  mkdir sisacte
  cd siscate
  unzip /root/sisacte#3782-artifacts.zip
  ```

### Alteração arquivo consts.js (Ambiente FrontEnd)

  Para tratar as constantes/urls do FrontEnd que mudam conforme o ambiente, existe no pacote gerado o arquivo target/dependency/BOOT-INF/classes/resources/assets/config/consts.js, que por padrão é o arquivo de produção.  Porém, para os ambientes de publicação em desenvolvimento ou homologação, é necessário substituir este arquivo, sobrescrevendo-o com o conteúdo dos arquivos `consts.js_dev` ou `consts.js_hm` conforme o caso, que no repositório ficam na pasta `Frontend-AngularJs/config/`.

### Alteração arquivo application.properties (Criptografia de Dados)

  Devido a proteção de dados sensíveis do git-crypt, o arquivo `[application|application-hm|application-dev].properties` estará criptografado e com isso a aplicação não irá funcionar.  Portanto, substitua o arquivo .properties que estará em `/target/dependency/BOOT-INF/classes` pelo arquivo que o perfil "Master" do repositório SisActe possuir.  Lembrando de verificar qual arquivo deve ser publicado (produção, homologação ou desenvolvimento).

### Script de build para instalação

Foi gerado o script `build.sh` que executa os comandos relacionados à instalação da aplicação. 
É necessário configurar as seguintes variáveis dentro do script:
``` bash
SISTEMA=sisacte
HOSTDIR=/srv/$SISTEMA #diretório do volume mapeado do container
ARTIFACTSDIR=/root/$SISTEMA/job#3782/Backend-SpringBoot #diretório dos artefatos da integração continua
APPLICATIONPROPERTIES=/root/$SISTEMA/application.properties
IMAGETAG=1.0.14   #tag da imagem docker
```
O script realiza os seguintes passos:
- testa a existência do diretório de artefatos
- testa a existencia do arquivo .properties
- cria diretórios para a instalação
- seta permissões nos diretórios e arquivos da aplicação
- cria o Dockerfile da aplicação
- cria uma imagem da aplicação baseada no Dockerfile
- remove containers e imagens antigos
- roda um novo container
- lista os logs do container

Rode o script 
```
./build.sh
```

Se tudo correr bem, um container da aplicação estará rodando.
``` bash
# docker ps -a
CONTAINER ID      IMAGE                COMMAND                   CREATED     STATUS     PORTS                     NAMES
892c97173d99      sit/sisacte:1.0.14   "java -cp sisacte/ap…"    1 min ago   Up 1 min   0.0.0.0:8102->8102/tcp    sisacte
```

### Comandos úteis
``` bash
docker ps -a
docker logs --tail 1000 -f sisacte
```

