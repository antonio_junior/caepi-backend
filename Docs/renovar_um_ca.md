# Renovar um CA

Como um funcionário responsável pela renovação de CAs, quero renovar um CA mantendo o número anterior, atualizando a data de validade e a descrição, para que os EPIs permaneçam em conformidade sem necessidade de um novo número de CA.

<u>**CENÁRIO 1**</u>:  Um EPI com CA prestes a expirar passa por uma reavaliação e é aprovado para continuação do uso, necessitando da renovação do CA.

<u>**CENÁRIO 2**</u>: Um fabricante solicita a renovação do CA de um produto clássico para garantir a continuidade das vendas sem interrupções.





