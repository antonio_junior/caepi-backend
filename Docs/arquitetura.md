**1.**         **INTRODUÇÃO**

O Documento de Arquitetura de Software é um artefato que permite comunicar aos stakeholders as decisões arquiteturais de um projeto de software. A presente seção introduz esse documento, descrevendo sua finalidade, as definições, acrônimos e abreviações utilizadas no projeto, suas referências e a visão geral da arquitetura do sistema.

**1.1.**   **Finalidade**

Este documento tem como objetivo apresentar a arquitetura do Sistema de Gerenciamento de Certificados de Aprovação de Equipamentos de Proteção Individual - CAEPI. Por arquitetura, entende-se a estrutura ou o conjunto de estruturas do sistema, que compreendem:

· elementos de software;

· propriedades externamente visíveis destes elementos; 

· os relacionamentos entre os mesmos.

A arquitetura do sistema é formada por diversos componentes que se relacionam globalmente sob diferentes perspectivas. Sendo assim, este documento desenvolve várias visões arquiteturais, as quais representam características e interesses diferentes de diversos stakeholders sobre um mesmo objeto, ou seja, sobre o software desenvolvido.

**1.2.**   **Definições e Acrônimos**

| Definições |                                                              |
| ---------- | ------------------------------------------------------------ |
|            |                                                              |
| REST       | Padrão arquitetural usado em serviços Web.                   |
| SPA        | Padrão de aplicações que carregam apenas uma página e atualizam dinamicamente as informações exibidas à medida que o   usuário interage com a aplicação |
 

| Acrônimos |                                 |
| --------- | ------------------------------- |
|           |                                 |
| REST      | Representational State Transfer |
| SPA       | Single Page Application         |

 

**1.3.**   **Visão Geral**

O restante do documento está organizado como segue. A Seção 2 descreve a representação global da arquitetura da aplicação. A Seção 3 apresenta as principais metas e restrições arquiteturais. As Seções de 4 a 8 apresentam as diferentes visões arquiteturais, sejam elas: visão de caso de uso, visão lógica, visão de implementação, visão de dados e visão de implantação, respectivamente.



**2.**         **REPRESENTAÇÃO ARQUITETURAL**

O SisActe é composto por quatro camadas:

**2.1.**   **Camada de Apresentação**

Controla a interação entre usuário e aplicação. Contém componentes responsáveis pela lógica de apresentação da aplicação e tem as responsabilidades de capturar dados, apresentar os resultados e controlar a navegação.

No mesmo nível estão os serviços expostos pela aplicação para interagir com outras aplicações.

A parte de apresentação no que diz respeito aos componentes de interface e interação direta com o usuário serão efetuados na camada do cliente (navegador) com o Angular 15. O código javascript integrará com o servidor enviando e recebendo mensagens no formato json via serviços REST providos pelo Spring MVC.

**2.1.1.**  **Tecnologias Utilizadas**

·       Angular 15

·       govbr - webcomponents


**2.1.2.** **Principais Dependências**

| Dependência               | Versão  |
| --------------------------| --------| 
|                           |         |
| angular                   | 15      |             
| angular/animations        | ^15.0.4 | 
| angular/common            | ^15.0.4   | 
| angular/compiler          | ^15.0.4   | 
| angular/core              | ^15.0.4   | 
| angular/forms             | ^15.0.4   | 
| angular/platform-browser  | ^15.0.4   | 
| angular/platform-browser-dynamic  | ^15.0.4   | 
| angular/router            | ^15.0.4  | 
| govbr-ds/core             | ^3.2.0   | 
| govbr-ds/webcomponents    | ^1.6.0"  | 
| rxjs                      | ~7.8.0   | 
| tslib                     | ^2.4.1   |  
| zone.js                   |~0.12.0   | 

**2.2.**   **Camada de Aplicação**

Coordena a atividade da aplicação. Ela não contém regras de negócio nem mantém o estado dos objetos de negócio, mas pode manter o estado de uma tarefa em progresso.

**2.2.1.**  **Tecnologias Utilizadas**

·       Spring Boot 2.7.12

·       Spring Web

·       Spring Security

**2.2.2.** **Principais Dependências**

| Dependência                       | Versão  | Descrição                                                    |
| ----------------------------------| --------|------------------------------------------------------------- |
|                                   |         |                                                              |
| spring-boot-starter               | 2.7.12  | Core starter, including auto-configuration support, logging and YAML |
| spring-boot-devtools              | 2.7.12  | Spring Boot Developer Tools |
| spring-boot-starter-web           | 2.7.12  | Starter para a construção da web, incluindo RESTful, aplicativos usando Spring MVC. Usa Tomcat como o contêiner embutido padrão |
| spring-boot-starter-security      | 2.7.12  | Starter para usar Spring Security |
| spring-security-config            | 2.7.12   | Spring Security |
| spring-boot-starter-data-jpa      | 1.11.15 | Starter para usar Spring Data JPA com Hibernate |
| postgresql                        | 8.4-701 | PostgreSQL JDBC Driver Postgresql |
| hibernate-spatial                 | 5.0.12  | Integração e suporte para dados espaciais/GIS no Hibernate ORM |
| spring-boot-starter-data-rest     | 2.7.12  | Starter para expor repositórios Spring Data sobre REST usando Spring Data REST |
| flyway-core                       | 3.2.1   | Flyway Core | 
| hibernate-types-52                | 2.1.1   | Hibernate ORM 5.2, 5.3 e 5.4 Tipos extras. https://github.com/vladmihalcea/hibernate-types |

**2.3.**   **Camada de Domínio**

Contém informações sobre o domínio do negócio. Mantém os objetos de negócio.

**2.3.1.**  **Tecnologias Utilizadas**

·       Hibernate Validation

·       JPA

**2.4.**   **Camada de Infraestrutura**

Atua como uma biblioteca de suporte para as demais camadas, provendo comunicação entre elas, implementando persistência para os objetos de negócio, fornecendo bibliotecas para a camada de interface com o usuário etc.

 

**2.4.1.**  **Tecnologias Utilizadas**

·       Hibernate

·       Spring Data

**2.5.**   **Diagrama da solução.**

![img](./img/camadas.png)

 

**3.**         **METAS E RESTRIÇÕES DA ARQUITETURA**

om essa arquitetura em camadas, temos uma série de benefícios, incluindo separação de
preocupações, reutilização de código, escalabilidade, facilidade de teste e flexibilidade. Esses
benefícios ajudam a melhorar a qualidade do software, tornando-o mais fácil de desenvolver,
manter e evoluir. Abaixo temos uma visão mais detalhada dessa camada, mostrando os
componentes que são transmitidos entre as camadas, quais classes são chamadas até a
informação chegar na camada de persistência, posteriormente no banco de dados.


**4.**         **VISÃO DE CASOS DE USO**

O diagrama de casos de uso a seguir ilustra o relacionamento entre as principais funcionalidades e atores com impacto na arquitetura do sistema:

![img](./img/use-case-caepi.jpg)

**4.1.**   **Atores**

O diagrama de caso de uso mostra as principais funcionalidades do sistema e os atores que interagem com o sistema. No diagrama, existem três atores principais: o usuário do sistema, aempresa e o administrador do sistema.

A empresa pode realizar ações como cadastrar um novo CA, cadastrar um novo laudo de CA, visualizar seus CAs cadastrados, visualizar seus laudos de CAs e gerar relatórios de laudos, cadastrar um novo representante de empresa, visualizar a lista de empresas cadastradas e visualizar a lista de representantes de empresa cadastrados. O administrador do sistema pode gerenciar os usuários do sistema, gerenciar as empresas e revisar e alterar requisições. Já os usuários, poderão buscar os CAs em um sistema externo

Os casos de uso do sistema estão organizados de forma a mostrar uma das principais funcionalidades do sistema. Cada caso de uso representa uma ação que pode ser realizada pelo usuário do sistema, pela empresa ou pelo administrador do sistema


**4.2.**   **Realizações de Caso de Uso**

Segue um diagrama de sequência, mostrando o fluxo da informação, desde a requisição até a chegada no banco de dados.

**5.**         **VISÃO LÓGICA**

O usuário interage com o sistema através da *view*, realizando alguma operação no sistema, a *view* solicita a *controller* a ação realizada, A *controller* processa as informações por meio da comunicação com a *model*, que por sua vez se comunica com o banco de dados, que por conseguinte repassa o resultado da operação solicitada para a *view*.

**6.**         **VISÃO DE IMPLEMENTAÇÃO**

**![img](./img/diagram-sequence-caepi.jpg)**

O primeiro passo é o usuário ou administrador acessar a página web do sistema. A página web
solicita que o usuário ou administrador faça login, e neste caso, o login do usuário é feito
através do gov.br e o login do administrador é feito através do SSO da SIT.

Após a autenticação, a página inicial do sistema é exibida. Nesse momento, o usuário ou
administrador pode navegar até a opção "Emitir CA". A página web solicita ao servidor que
envie as informações necessárias para preencher o formulário de emissão do Certificado de
Aprovação, através de uma chamada de serviço POST.

Essa chamada é recebida pelo controlador (Controller) do sistema, que é responsável por
gerenciar as solicitações recebidas pelo servidor. O controlador recebe os dados enviados pela
página web e os encaminha para o serviço (Service) correspondente.

O serviço então é responsável por realizar as validações e processamentos necessários para
emitir o Certificado de Aprovação. Ele pode fazer chamadas a outras camadas de arquitetura
do sistema, como por exemplo, uma camada de validação de dados. O serviço retorna as
informações necessárias para a emissão do Certificado de Aprovação para o controlador.

O serviço, por sua vez, encaminha as informações para o repositório (Repository)
correspondente. O repositório é responsável por persistir as informações do Certificado de
Aprovação no banco de dados.

Por fim, o banco de dados armazena as informações do Certificado de Aprovação e gera um
número de protocolo. O repositório retorna uma mensagem de sucesso para o controlador.
O controlador envia a mensagem de sucesso para a página web, que recebe a mensagem e
exibe o número de protocolo para o usuário.

**7.**         **VISÃO DE DADOS**

**7.1.**   **Modelo Lógico**

Diagrama de esquema do banco de dados, feito na ferramenta MySQL Workbench. Veja o diagrama abaixo em tamanho original na seção de Anexos.

![](./img/data-source-caepi.png)

A modelagem do banco de dados com os relacionamentos foi feita com base nos requisitos do
sistema e nas entidades identificadas no domínio do problema. Os relacionamentos escolhidos
foram definidos de forma a garantir a integridade referencial dos dados e a facilitar a
manipulação e análise das informações.

A tabela "Certificado de aprovação" representa uma entidade que pode ter várias versões ao
longo do tempo. Por isso, foi criada a tabela "Versão certificado de aprovação" com um
relacionamento um para muitos entre as tabelas. Dessa forma, é possível armazenar
informações específicas de cada versão do certificado, sem precisar duplicar informações na
tabela "Certificado de aprovação".

A tabela "EPI" representa uma entidade que pode ser utilizada por vários usuários em uma
empresa. Por isso, foi criada a tabela "Usuário" com um relacionamento muitos para muitos
entre as tabelas "EPI" e "Usuário". Dessa forma, é possível representar que um usuário pode
utilizar vários EPIs e que um EPI pode ser utilizado por vários usuários.

A tabela "EPI" também tem um relacionamento um para muitos com a tabela "Tipos de EPI".
Isso foi feito para garantir a integridade dos dados, evitando que um mesmo tipo de EPI seja
registrado com informações diferentes em diferentes registros. Além disso, a tabela "Tipos de

proteção" representa uma entidade que pode estar relacionada a vários tipos de EPIs, por isso
foi criado um relacionamento muitos para muitos entre essas tabelas.

A tabela "Empresa" representa uma entidade que pode ter vários representantes. Por isso, foi
criado um relacionamento um para muitos entre as tabelas "Empresa" e "Representante".
Dessa forma, é possível armazenar informações específicas de cada representante de uma
empresa sem precisar duplicar informações na tabela "Empresa".

A tabela "Laudo" representa uma entidade que está relacionada a um usuário e a um EPI. Por
isso, foi criado um relacionamento muitos para muitos entre as tabelas "Laudo", "Usuário" e
"EPI". Dessa forma, é possível representar que um usuário pode ter vários laudos relacionados
a diferentes CAs, e que um CA pode ter vários laudos relacionados a diferentes usuários.

Por fim, a tabela "Referência" representa uma entidade que pode estar relacionada a várias
outras entidades do sistema. Por isso, foi criado um relacionamento muitos para muitos entre
as tabelas "Referência" e todas as outras tabelas do sistema. Dessa forma, é possível utilizar a
tabela "Referência" para registrar informações adicionais que possam ser relevantes para
diferentes entidades do sistema.

Entendemos que esse modelo poderá crescer, contendo mais atributos em cada tabela,
conforme o projeto for sendo construído e outras tabelas e relacionamentos podem surgir
também.


**8.**         **VISÃO DE IMPLANTACÃO**

Esta seção descreve a configuração da rede física (hardware) na qual o SisActe será implantado e executado.

**8.1.**   **Diagrama de Implantação**

A figura abaixo mostra o Diagrama de Implantação da solução, onde o host do cliente acessa o servidor Web através de um navegador Web, e o servidor de banco de dados é acessado na porta 5432.

![img](./img/diagram-implantation.png)