# Emitir um CA

Como um funcionário responsável pela emissão de CAs, quero emitir um CA com todas as informações necessárias, para que os usuários possam obter a documentação apropriada para seus EPIs de maneira eficiente.

<u>**CENÁRIO 1:**</u> Um novo EPI é aprovado para uso, e o funcionário responsável precisa emitir um CA oficial para documentar sua conformidade.

<u>**CENÁRIO 2:**</u>  Após a conclusão bem-sucedida dos testes de um EPI, o responsável pela emissão de CAs gera o documento necessário para permitir sua comercialização.
