O projeto está dividido em dois módulos CAEPI-API e CAEPI-Frontend. O módulo Backend-SpringBoot contém aplicação servidor API Rest, desenvolvido na lingagem JAVA com SpringBoot 2.7.12 e banco de dados PostgreSQL. O módulo Frontend-Angular15 contém a aplicação cliente, desenvolvido nas linguagens HTML5 e TypeScript.



<h3>Requisitos para instalação do CAEPI-API </h3>

1) Primeiro item a ser instalado e configurado é o Java JDK 8. Para isso, siga este [Tutorial](https://comoprogramarjava.com.br/instalacao/instalar-jdk8-no-windows-10/) para fazer o download, instalação e configuração das variáveis ambientes no Windows.


2) Certifique-se que o Apache Maven 3.3+ também esteja instalado. Se você ainda não tem o Maven instalado, você pode seguir as instruções em [maven.apache.org](https://maven.apache.org/).


3) Será necessário a instalação do PgAdmin 4. Clique no [aqui](https://www.pgadmin.org/download/pgadmin-4-windows/) para fazer o download e a instalação do PgAdmin.


4) Para que o PostgreSQL possa realizar as operações de georeferenciamento baixe e instale o postgis:
 https://postgis.net/install/

<h3>Criando o banco de dados</h3>

Com o PgAdmin 4 instalado, crie um novo banco de dados conforme a imagem abaixo:

![img](./img/novo-bd-caepi.PNG)

Em seguida instale as extensões postgis, postgis_topology e unnacent no banco db_caepi criado.
Se quiser pode rodar os comandos abaixo para instalar também:

create extension postgis;

create extension postgis_topology;

create extension unaccent;

![img](./img/bd-extension.png)

### Instalação e Execução do CAEPI-API

Para o ambiente de desenvolvimento vamos utilizar a IDE Intellij Community, caso não tenha instalado a IDE, faça o download em https://www.jetbrains.com/idea/download

1) Primeiro clone o projeto digitando no CMD ou Git Bash

```
git clone https://git.mte.gov.br/mte-oit/caepi-api
```

2) Abra o módulo CAEPI-API do projeto clonado no Intellij Community em:

``` 
File -> Open
```

O Intellij irá automáticamente resolver as dependências do pom.xm.


3) Inclua a propriedade spring.profiles.active=dev no final do arquivo abaixo para habilitar o profile de desenvolvimento: 

```
caepi-api/src/main/resources/applications.properties
```

4) Edite as configurações do banco de dados de desenvolvimento em:

```
caepi-api/src/main/resources/applications-dev.properties
```
conforme o exemplo abaixo:

```
spring.datasource.url=jdbc:postgresql://localhost:5432/db_caepi
spring.datasource.username=postgres
spring.datasource.password=
spring.datasource.driverClassName=org.postgresql.Driver
spring.jpa.properties.hibernate.dialect = org.hibernate.spatial.dialect.postgis.PostgisDialect
```

5) Executar Backend-SpringBoot:

Para executar a aplicação utilizando o Intellij Community basta clicar com o botão direito na classe Main do projeto:

```
Backend-SpringBoot/src/main/java/br/com/sisacte/api/CaepiApplication.java
```

A aplicação executará na seguinte url http://localhost:8080.


### Requisitos para instalação do CAEPI-Frontend

A aplicação cliente requer o Node.Js na versão 10.24.0^. Se você ainda não tem instalado, você pode seguir as instruções em [nodejs.org](https://nodejs.org/en/)



### Instalação do CAEPI-Frontend

1) Caso não tenha clonado ainda o projeto, clone digitando:

```
git clone https://git.mte.gov.br/mte-oit/caepi-frontend
```

2)Abra o arquivo *CAEPI-Frontend/src/environments/environments.ts* e altere a propriedade para o endereço url da aplicação servidor (Backend-SpringBoot)

```export const environment = {
	production: (window["env"] && window["env"].PRODUCTION) === 'true' || true,
	api: (window["env"] && window["env"].API) || 'http://localhost:8080',
};
```

3) Digite o comando dentro do diretório CAEPI-Frontend

```
npm install
```

4) Digite o comando

```
npm install -g gulp
```

5) Por fim execute a aplicação digitando

```
ng serve
```

Após rodar o comando acima, o Gulp executará as tarefas para criar o build da aplicação Frontend e o CAEPI será aberto no navegador com o host http://localhost:4200

